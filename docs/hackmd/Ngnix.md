

Nginx 是非同步框架的網頁伺服器，主要用於
* 反向代理 (reverse proxy) 能作為 API Gateway 進行後端分流
* 負載平衡 (load balancer) 和快取 (HTTP Cache) 可用於靜態網站架設


## Nginx Config

[Beginner’s Guide - 中文文檔](https://github.com/DocsHome/nginx-docs/tree/master)
[Beginner’s Guide
](http://nginx.org/en/docs/beginners_guide.html)

Nginx 的主要設定檔通常會放置在 /etc/nginx/nginx.conf
另外在 /etc/nginx/conf.d/*.conf 則會放置不同域名的 config file
然後在主設定檔中的 http context 加入一行
```
include /etc/nginx/conf.d/*.conf;
```
即可將不同域名的設定引入，達成方便管理與修改不同域名設定的特性。


---


:::info
nginx.conf is the main configuration file for Nginx. 
It contains the global configuration settings that apply to the entire Nginx server.
:::


* /etc/nginx/nginx.conf
```
events {
    worker_connections 1024; # 網路連接相關設定
}

http {
    include conf.d/*.conf;
}
```

:::info
servers.conf is usually an additional configuration file where you can define individual server blocks for various websites or applications hosted by Ng
:::

* /etc/nginx/conf.d/servers.conf
```
upstream api {
    server localhost:5000;
    server localhost:5001;
}

server {
    listen 80;
    listen [::]:80;
    server_name SERVER_IP;
    root /home/ryan;

    proxy_set_header Host $http_host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

    location / {
            proxy_pass http://api/;
    }
}
```

* ++upstream++
upstream block 定義了我們想要將 request proxy 過去的應用，上圖中的例子代表我們可以將請求 proxy 到分別監聽 5000 與 5001 port 的兩個應用
* ++server++
這個 block 則是定義了 proxy server 的相關設定，包括要監聽的 port (http 為 80 ，https 為 443)、規定哪些 domain 或 ip 的 request 會被 nginx server 處理（server_name）
* ++location++
這個 block 非常重要，不過幸好概念還蠻易懂的，它其實就像是 routing 的概念，設定不同的 path 要對應到怎麼樣的設定。上圖的範例 location 後面接的是 /，代表任何路徑都會被這個 block 給接收處理。
(location 後面也是可以吃 regex 的喔，例如：location ~* .(pl|cgi|perl|prl)$ {}）



### Lab

* docker-compose
```
version: '3'
services:
  nginx:
    image: nginx:latest
    container_name: my_nginx
    network_mode: host
    volumes:
      - ./default.conf:/etc/nginx/conf.d/default.conf:ro
    ports:
      - "80:80"
```

* default.conf
```
user  nginx;
worker_processes  auto;

error_log  /var/log/nginx/error.log notice;
pid        /var/run/nginx.pid;


events {
    worker_connections  1024;
}


http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile        on;
    #tcp_nopush     on;

    keepalive_timeout  65;

    #gzip  on;

    include /etc/nginx/conf.d/*.conf;
}
```


## Crossplane

[Github - crossplane](https://github.com/nginxinc/crossplane)




### Command Line Interface


:::success
usage: crossplane <command> [options]

various operations for nginx config files

optional arguments:
  -h, --help            show this help message and exit
  -V, --version         show program's version number and exit
:::


---


## Nginx Document


### Proxy Module

[Module ngx_http_proxy_module](https://nginx.org/en/docs/http/ngx_http_proxy_module.html#proxy_pass)
[Module ngx_http_proxy_module - 中文](https://github.com/DocsHome/nginx-docs/blob/master/%E6%A8%A1%E5%9D%97%E5%8F%82%E8%80%83/http/ngx_http_proxy_module.md)


:::info
The ngx_http_proxy_module module allows passing requests to another server
:::



69 個參數

* Example Configuration
```
location / {
    proxy_pass       http://localhost:8000;
    proxy_set_header Host      $host;
    proxy_set_header X-Real-IP $remote_addr;
}
```



### Controlling NGINX Processes at Runtime

[Link](https://docs.nginx.com/nginx/admin-guide/basic-functionality/runtime-control/)

啟動、停止和重新載入配置

Understand the NGINX processes that handle traffic, and how to control them at runtime.

#### Controlling NGINX
To reload your configuration, you can stop or restart NGINX, or send signals to the master process. 
A signal can be sent by running the nginx command (invoking the NGINX executable) with the -s argument.

:::success
nginx -s <SIGNAL>
:::
where <SIGNAL> can be one of the following:

* quit – Shut down gracefully (the SIGQUIT signal)
* reload – Reload the configuration file (the SIGHUP signal)
* reopen – Reopen log files (the SIGUSR1 signal)
* stop – Shut down immediately (or fast shutdown, the SIGTERM singal)


