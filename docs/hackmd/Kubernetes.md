###### tags: `Work`


# Kubernetes

[影片參考: Kubernetes Tutorial for Beginners](https://www.youtube.com/watch?v=X48VuDVv0do&ab_channel=TechWorldwithNana)
[文章參考: 基礎教學](https://cwhu.medium.com/kubernetes-basic-concept-tutorial-e033e3504ec0)

**K8s = Container + Orchestration**

Container Orchestration
支持應用程序的容器的部署和組織稱為Container Orchestration容器編排，這是通過容器編排工具完成的。包括Kubernetes、Docker Swarm 和 MESOS

![](https://i.imgur.com/J32S5nF.png)



---

Kubernetes 是一個協助我們自動化部署、擴張以及管理容器應用程式(containerized applications)的系統。相較於需要手動部署每個容器化應用程式(containers)到每台機器上，Kubernetes 可以幫我們做到以下幾件事情：

* 同時部署多個 containers 到一台機器上，甚至多台機器。
* 管理各個 container 的狀態。如果提供某個服務的 container 不小心 crash 了，Kubernetes 會偵測到並重啟這個 container，確保持續提供服務
* 將一台機器上所有的 containers 轉移到另外一台機器上。
* 提供機器高度擴張性。Kubernetes cluster 可以從一台機器，延展到多台機器共同運行。



## k8s 元件說明

![](https://i.imgur.com/2A7uDSv.png)

* ++Container++
Container中文稱作容器，指的是將應用程式沙箱化，且包含應用程式所需的關聯系統程式、必要的執行檔等內容，讓其無須再透過另外部署安裝，即可在各種容器平台中執行。

* ++Pod++
Pod則為K8s運作的最小單位，中文可稱作容器集，意即一個Pod內部可含一個或是多個Container（視運行需求會有一或多個容器同時運行）。而一個Pod會對應到一個應用程式，同一個Pod中的Containers則會共享相同的網路資源（如：IP地址、主機名稱等）。

![](https://i.imgur.com/ufjsOAu.png)


* ++Node++
Node則還分為2種：
    * ++1.Worker Node++
Worker Node為主要執行的運行節點，且一個Worker Node會對應一台機器。內部還可細分為kubelet、kube-proxy及Container Runtime 3個元件。
    * ++2.Control Plane++
舊簡稱Master Node，負責各個Worker Node的管理，可稱作是K8S的發號施令的中樞。其內部由kube-apiserver、etcd、kube-scheduler以及kube-controller-manager 4個元件組成。



## k8s 架構

K8S 屬分布式系統，主要元件有：
1. Master – 大總管，可做為主節點
2. Node – 主要工作的節點，上面運行了許多容器。可想作一台虛擬機。K8S 可操控高達 1,000 個 nodes 以上
3. masters 和 nodes組成叢集 (Clusters)


![](https://i.imgur.com/MEmkEhf.png)

```
Controller Plane 在舊版的 K8s稱為 Master Node
Node以前則叫做 Slave/Worker Node 或 更早版本的 Minion
```


### Master Node

Kubernetes 運作的指揮中心，可以簡化看成一個特化的 Node 負責管理所有其他 Node。
一個 Master Node（簡稱 Master）中有四個組件

(a) Etcd, 
(b) API Server, 
(c\) Controller Manager Server
(d) Scheduler

#### ++API Server++

1. 管理整個 Kubernetes 所需 API 的接口（Endpoint），例如從 Command Line 下 kubectl 指令就會把指令送到這裏
2. 負責 Node 之間的溝通橋樑，每個 Node 彼此不能直接溝通，必須要透過 apiserver 轉介
3. 負責 Kubernetes 中的請求的身份認證與授權

紅框就是request 流程  (ex: kubectl 指令)


![](https://i.imgur.com/mhc9IVa.png)


#### ++kube-scheduler++

:::success
Scheduler just decides on which Node new Pod should be sheduled
:::

整個 Kubernetes 的 Pods 調度員
Scheduler 會監視新建立但還沒有被指定要跑在哪個 Node 上的 Pod，並根據每個 Node 上面資源規定、硬體限制等條件去協調出一個最適合放置的 Node 讓該 Pod 跑


#### ++Etcd++

ETCD 以分散式鍵值(key-value)的形式存放着整個分散系統的最重要資料。

用來存放 Kubernetes Cluster 的資料作為備份，當 Master 因為某些原因而故障時，我們可以透過 etcd 幫我們還原 Kubernetes 的狀態

#### [kube-controller-manager](https://ithelp.ithome.com.tw/articles/10197442)

負責管理並運行 K8s controller 的組件，負責監視 Cluster 狀態的組件，是 K8s中所有resource-objects 的自動化控制中心

kube-controller-manager又可以細分為：
* ++Namespace Controller++
* ++Node-Controller++
* ++Replication-Controller++
* ++Deployment Controller++


### Worker Node

:::warning
Host Application as Containers
:::

Kubernetes 運作的最小硬體單位
一個 Worker Node（簡稱 Node）對應到一台機器，可以是實體機如你的筆電、或是虛擬機如 AWS 上的一台 EC2 或 GCP 上的一台 Computer Engine。

Node的工作 是負責**將Application以容器的形式運行在K8s cluster 集群中**。


每個 Node 中都有三個組件：kubelet、kube-proxy、Container Runtime。

* ++Node 包含了四個基本組件++
Kubelet, Proxy, Pod, Container

![](https://i.imgur.com/2n1otbM.png)

#### ++kubelet++

對應api-server的接口，可以看成每個Node上的"實際執行者"或"操作者"，負責接收來自api-server的訊息，並做出相對應的動作。
例如，負責Pod對應的容器的建立、啟動或停止等

#### ++kube-proxy++

kube-proxy 是一個 network proxy，負責維護Node上的網路規則(iptables)，這些規則允許從群集內部或外部的與Pod進行通訊。

#### ++Container Runtime++

Node上運行容器的執行程式，K8s預設是Docker，但也支援其他Runtime Engine。
例如 rkt、CRI-O、containerd, kata container等等..

---

### [Pod](https://kubernetes.io/docs/concepts/workloads/pods/)

```
Pods are the smallest deployable units of computing that you can create and manage in Kubernetes.
```

Kubernetes 運作的最小單位，一個 Pod 對應到一個應用服務（Application） ，舉例來說一個 Pod 可能會對應到一個 API Server。
* 每個 Pod 都有一個身分證，也就是屬於這個 Pod 的 ++yaml 檔++
* 一個 Pod 裡面可以有一個或是多個 Container，但一般情況一個 Pod 最好只有一個 Container
* 同一個 Pod 中的 Containers 共享相同資源及網路，彼此透過 local port number 溝通

#### Using pod

The following is an example of a Pod which consists of a container running the image nginx:1.14.2.

```yaml=
apiVersion: v1    <=== 指定使用 v1 版本的 api
kind: Pod         <=== 指定類型為 Pod
metadata:         <=== 將 Pod 命名為 ngnix
  name: nginx
spec:                     <=== 規格描述
  containers:             <=== 描述容器
  - name: nginx           <=== 將容器命名為 frontend
    image: nginx:1.14.2   <=== 使用 nginx:1.14.2 映像檔
    ports:
    - containerPort: 80    <=== 指定使用 80 port
```


++Pod的狀態++

| Value | Description| 
| -------- | -------- | 
| Pending  | Pod已被k8s接受，但尚未創建完成，這可能需要一段時間。  |
| Running | Pod已創建所有Container。至少有一個Container仍在運行，或者正在啟動或重新啟動。 |
|Succeeded | Pod中的所有Container都已成功終止，並且不會重新啟動。 |
| Failed	| Pod中的所有Container都已終止，Container不是退出非零狀態，就是被系統終止。 |
|Unknown |由於某種原因，無法獲得Pod的狀態，這通常是由於與Pod的主機通信時出錯。|


##### 創建 Pod
To create the Pod shown above, run the following command:

```
kubectl apply -f https://k8s.io/examples/pods/simple-pod.yaml
```

##### 刪除 Pod

```
kubectl delete pods <pod>
```


### Kubernetes Cluster
[Ref: ](https://medium.com/starbugs/kubernetes-%E6%95%99%E5%AD%B8-%E4%B8%80-%E6%A6%82%E5%BF%B5%E8%88%87%E6%9E%B6%E6%A7%8B-954caa9b1558)
Kubernetes 中多個 Node 與 Master 的集合。基本上可以想成在同一個環境裡所有 Node 集合在一起的單位。

* Kubernetes 集群由控制面板 Control Panel 與節點 Node 所組成。
* Control Plane(控制面板)又稱為是 Kubernetes Master。
![](https://i.imgur.com/WzgQYkJ.png)



Control Plane 控制面板由幾個元件 (Component) 所組成：
1. Kube API Server
控制面板中用來暴露 Kubernetes API 的元件，讓其他服務可以讀寫 K8S 的資源物件 (Resouce Object)。
2. Kube Schedular
調度器，需要調度軟體、硬體資源的時候就要靠調度器囉。例如：如果新建立的 pod 沒有 node 可以放的時候，調度器就會開啟一個新的 node，來放置剛剛需要建立的 pod。
3. Kube Controller Manager
是一個在背景持續執行的程序 (daemon)，用來調節系統狀態，透過 api-server 可以監視 Cluster 共享的狀態。
需要變更目前狀態的時候 Kube Controller 就會將目前的狀態變更到想要變更的狀態，例如：本來 2 個副本 (Replica) 擴展到 4 個副本。
包含了副本控制器 (Replication Controller)，端點控制器 (Endpoint Controller)、命名空間控制器(Namepsace Controller)與服務帳號控制器
4. Cloud Controller Manager
基於 Kube Controller Manager，各個雲平台提供者（Provider）的實作。而每個 Node 則包含：
kubelet — 用來跟 Master 溝通的元件。
kube-proxy — 網路代理，用來反應 K8S 各個 Node 上的網路服務



## k8s command

kubectl — 跟 K8S Cluster 溝通的工具

[常見的 kubectl 指令](https://fufu.gitbook.io/kk8s/kubectl-cmd-note)
[vedio 34:50](https://youtu.be/X48VuDVv0do?t=2090) Minikube and Kubectl


![](https://i.imgur.com/QzdAZgm.png)





|                         | minikube | kind                 | k3s                 |
| ----------------------- | -------- | -------------------- | ------------------- |
| runtime                 | VM       | Tcontainer           | native              |
| supported architectures | AMD64    | AMD64                | AMD64, ARMv7, ARM64 |
| memory requirements     | 2GB      | 8GB (Windows, MacOS) | 512 MB              |
| requires root?          | no       | no                   | yes                 |
| multi-cluster support	   |  yes   |   yes        | no (can be achieved using containers  |
| multi-node support      | no       | yes    |   yes |


---

Install Kubernetes Cluster on Ubuntu 20.04 using K3s [- LINK](https://computingforgeeks.com/install-kubernetes-on-ubuntu-using-k3s/)

### Setup the Master k3s Node


In this step, we shall install and prepare the master node. 
This involves installing the k3s service and starting it.

```
curl -sfL https://get.k3s.io | sh -s - --docker
```

確認是否可以成功執行 Kubectl， 如果遇到下面權限問題， 改權限即可。

```
sudo chmod 755 /etc/rancher/k3s/k3s.yaml
```

![](https://i.imgur.com/pyKkp8k.png)


### Basic Kubectl cmds

Create and debug Pods in k3s

:::info
CRUD commands
:::
Create deployment - kubectl create deployment [name]
Edit deployment - kubectl edit deployment [name]
Delete deployment - kubectl delete deployment [name]

:::info
Status of different K8s components
:::
kubectl get nodes | pod | services | replicaset | deployment

:::info
Debugging pods
:::
Log to console - kubectl logs [pod name]
Get Interactive Terminal - kubectl exec -it [pod name] --bon/bash

* example
```
kubectl exec -it nginx-deployment-5d59d67564-llnpq -- bin/bash
```

![](https://i.imgur.com/FNtYh3i.png)


#### Deployment

Deployment Controller 提供 Pods 和 ReplicaSets可定義的更新，k8s 下的Deployment有許多feature可以使用
如：create、update、roll back、pause、resume等，針對 Deployment 的替代方案可以使用kubectl rolling update這個指令

建立一個 Deployment

```
kubectl apply -f example/deploy_test.yaml
```
[Deployment.py](https://medium.com/learn-or-die/kubernetes-deployment-a54327e79a7e)
```yaml=
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    app: nginx
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.7.9
        ports:
        - containerPort: 80
```
![](https://i.imgur.com/i0Z9jVP.png)

++檢視 Deployments, 會顯示以下欄位++
* NAME 列出在這個叢集中 Deployments 的名稱
* UP-TO-DATE 顯示已被更新來達成期望狀態的 replicas 數量
* AVAILABLE 顯示可用的 replicas 數量
* AGE 顯示 replica 已運行的時間





---

### k8s yaml

[YAML Ain't Markup Language](https://www.cloudbees.com/blog/yaml-tutorial-everything-you-need-get-started) (YAML) is a serialization language that has steadily increased in popularity over the last few years

在K8s中所有物件(resorce-object)都是用YAML File來描述。
我們需要寫非常大量的YAML來創造物件，因此 K8s Engineering 也被稱為 "YAML Engineering"

在 K8s中的YAML檔，有四個欄位是必須的：apiVersion、kind、metadata、spec

![](https://i.imgur.com/5nP1HyM.png)


#### ++apiVersion++

apiVersion 其實就是API的版本，因為k8s提供給每個物件的API版本都不同，需要跟著物件來更改需要的 apiVersion。


#### ++kind++

kind 就是我們這個 YAML是要用來創造哪個物件。  像是Pod、Service、Deployment等等。 
它們都有各自對應的 apiVersion，像是：


|kind| apiVersion| 
| -------- | -------- |
|Pod  |  v1   |
|Service|  v1   |
|Deployment	 | apps/v1 |
|ReplicaSet  | apps/v1 |


#### ++metadata++

metadata是用來描述這個物件的資訊，像是它的名稱，lable等。


#### ++spec++

spec是這四個欄位中最重要的欄位
用來定義這個物件內有甚麼，也就是說，用來定義這個物件是要用來幹嘛的。
例如一個 Pod內有 Container，那 kind就是Pod，而Container的資訊則寫在spec欄位，代表這個 Pod的功能是host 一個Container


## CNCF

Cloud Native Computing Foundation
Linux基金會底下的非營利組織



## Other

K8s 本身只需要專注於 kubelet 以及 CRI (Container Runtime Interface) 標準的互動，第三方開發者則可以根據需求開發出各式各樣不同的產品，只要能夠滿足 CRI 標準，都可以作為K8s的Container Runtime Engine


### Container Runtime Interface (CRI)


[Kubernetes 為了能夠有效地銜接各式各樣不同的 Container Runtime 解決方案](https://ithelp.ithome.com.tw/articles/10218127)，勢必也需要推出相關的標準，就如同 Container OCI 一樣，符合標準的解決方案就能夠輕鬆的整合到 kubernetes 之中。
於是 Container Runtime Interface(CRI) 標準就被設計且開發來

* 表格列出了 kubernetes 與 CRI Runtime 各自的責任

|Kubernetes	|CRI Runtime|
| --- | --- |
|Kubernetes Resources/API |	Pod Life Cycle (Add/Delete)|	
|Storage (CSI)|	Image management|	
|Networking (CNI)|	Status	|
|Dispatcher	| Container Operations (attatch/exec)|


#### CRI 架構


CRI 本身是個==溝通介面==，這代表溝通的兩方(Docker & Kubernetes)都需要根據這個界面去實現與滿足。

最直觀的想法就是如果沒有辦法使得 docker 本身支援 CRI 的標準，那就**額外撰寫一個轉接器(dockershim)**，其運作在 kubelet 與 Docker，該應用程式上承 CRI 與 kubernetes 溝通，下承 Docker API 與 Docker Engine 溝通。

問題: 做法可行，但是其實效能大大則扣
圖中的上半部份，而圖中的下半部分則是後來的改變之處

![](https://i.imgur.com/WKum4Tp.png)

反正最後都是透過 containerd 進行操作，而本身也不太需要 docker 自己的功能，那是否就直接將 dockershim 溝通的角色從 docker engine 轉移到 containerd 即可。 因此後來又有所謂的 CRI-Containerd 的出現。


伴隨者 Containerd 本身的功能開發，提供了 Plugin 這種外掛功能的特色後，將 CRI-Containerd 的功能直接整合到該 Plugin 內就可以直接再次減少一個元件，直接讓 kubelet 與 containerd 直接溝通來創建相關的 container。
![](https://i.imgur.com/ou2vzXG.png)



### Window install Kubernetes



Docker 內建有包含 Kubernetes 單機叢集環境, 預設並沒有啟用

![](https://i.imgur.com/SLb278f.png)




* 確認安裝成功
```
kubectl version --short 
```
![](https://i.imgur.com/hgcl2TC.png)



# Open source

[本機版k8s](https://ithelp.ithome.com.tw/articles/10262063)
Kubeadm, Minikube, KIND


## kinds

全名是: Kubernetes in Docker
使用docker在本機運行kubernetes，kind 會將 docker的容器當成節點(node)來運行
相較於MiniKube 使用 VM 來建立節點，kind 使用docker，因此kind的啟動速度會比 MiniKube快上不少


![](https://i.imgur.com/tp95ogX.png)


[Ref: Kind部署環境與測試](https://ithelp.ithome.com.tw/articles/10266092)

```
curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.11.1/kind-linux-amd64
chmod +x ./kind
mv ./kind /usr/local/bin/kind
```

當安裝完後,可以透過下面指令測試是否有安裝成功
:::success
kind create cluster --> 這會產生一個名稱為 kind 的 cluster
kind create cluster --name kind-2  --> 這樣就可以指定 cluster 名稱為 kind-2 的 cluster。
:::

![](https://i.imgur.com/WlbO8C3.png)

沒有加上 --name的話，就會刪除預設context 名稱，也就是叫做 kind的cluster
可以像下面這樣加上 \-\-name
:::info
kind get clusters --> 查看 cluster
kind delete cluster --> 刪除 cluster
kind delete cluster --name kind-2
:::


### 建立環境


去官網找到 [kind-example-config](https://raw.githubusercontent.com/kubernetes-sigs/kind/main/site/content/docs/user/kind-example-config.yaml)

透過這種方式，即可用剛剛建立的設定yaml檔去建立cluster。
建立完成後，可以使用下面指令檢查是否有正確建立nodes。
```
kind create cluster --config kind-example-config.yaml
kind get nodes
```

成功的話會看到有一個 control-plane 的 nodes 以及三個 worker 的 node
![](https://i.imgur.com/xBUhR4J.png)

kind是用 docker的 pod當作節點，因此當你現在使用查詢docker 運作中的pod的節點，就可以看到剛剛產生出來的nodes

![](https://i.imgur.com/ZcRdKzF.png)



### 建立 pods

- 透過這個指令，我們可以告訴k8s建立出單個pod，名稱為nginx
使用Docker Hub上的nginx映像檔,且 flag 為 --restart=Never
- 如果要看這個pod的詳細內容，則可以使用describe來查看詳細內容
- 如果要刪除pod的話，則可以透過delete pod來刪除pod
```
kubectl run nginx --image=nginx --restart=Never
kubectl describe pod nginx
kubectl delete pod nginx
```

![](https://i.imgur.com/IuVKpoK.png)



#### 透過 yaml 建立範例 pod

上面我們使用 image 直接建立了一個 pod，不過用這種方式沒辦法詳細設定pod的內容
因此這邊會講解如何用寫好的 yaml 來建立 pod (之後的 service Deployment等等也會用這種方式建立)



## Kubeadm

跟MiniKube一樣是官方維護的專案，和MiniKube很類似，Kubeadm會使用VM來建立節點。
跟MiniKube不一樣在於，MiniKube會建立單節點cluster，Kubeadm會建立master-node和slave-node。

![](https://i.imgur.com/996vHnh.png)



### Install on Linux


```
sudo apt-get update && sudo apt-get install -y apt-transport-https curl
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
cat <<EOF | sudo tee /etc/apt/sources.list.d/kubernetes.list
deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF
sudo apt-get update
sudo apt-get install -y kubelet kubeadm kubectl
sudo apt-mark hold kubelet kubeadm kubectl
```





## minikube

MiniKube 為官方維護的工具之一，其特色為透過虛擬機器(VM virtual machine)來建立一個單節點的k8s叢集，由於其簡單安裝的特性，以及本身就有提供儀表板(dashboard)，所以使用上非常方便

![](https://i.imgur.com/RZ76k2G.png)



Mac 可以透過 homebrew 來完成安裝
```
brew install minikube
```

Linux
```
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
sudo install minikube-linux-amd64 /usr/local/bin/minikube

# install kubectl
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
chmod +x ./kubectl
sudo mv ./kubectl /usr/local/bin/kubectl
```

### Start

[Ref](https://medium.com/starbugs/kubernetes-教學-02-在-minikube-上部屬一個-python-flask-應用程式-e7a3b9448f2c)

開始使用 Minikube 與 kubectl
安裝完 minikube 之後，第一步先啟動 Minikube，如果你是第一次啟動，Minikube 會幫你建立新的虛擬機器 (VM)。

```
minikube start
```

![](https://i.imgur.com/wvfJ40D.png)


啟動好 minikube 的同時，kubectl 也準備就緒了。
來看看 minikube 的狀態。
```
minikube status
```

![](https://i.imgur.com/LwzBv4Z.png)

kubelet 與 api-server 已經成功跑起來。
* api-server: 是 Master 的元件
* kubelet: 是 Node 上負責跟 Master 溝通的元件

因為 Minikube 只有單一個 Node，所以同時具備 Master 與 Node 的元件 (意思是下面兩個框框都被包含在一個 minikube)


![](https://i.imgur.com/JdLjd7m.png)


使用 kubectl 查看目前 Cluster 中的狀態

```
kubectl get all
```

![](https://i.imgur.com/CcNjuH0.png)

裡面只有一個 kubernetes 的 service，因為我們的 Kubenetes Cluster 中還沒有安裝任何的東西。


---

#### Linux install issue

如下圖, 假設遇到 Exiting due to DRV_AS_ROOT: The “docker” driver should not be used with root privileges
![](https://i.imgur.com/OAqSJRi.png)

```
sudo minikube start --force --driver=docker
```
![](https://i.imgur.com/DYZSsOc.png)



### 官網簡單部署 

[Deploy applications](https://minikube.sigs.k8s.io/docs/start/)
Create a sample deployment and expose it on port 8080:
:::info
kubectl create deployment hello-minikube --image=k8s.gcr.io/echoserver:1.4
:::
![](https://i.imgur.com/7eN78F5.png)


公開image
對了!這時候還沒結束唷要訪問我們建立的image還需要將其公開才行
使用expose將image公開

:::info
kubectl expose deployment hello-minikube --type=NodePort --port=8080
:::

![](https://i.imgur.com/tFFJYMf.png)


取得公開網址 (一定要先expose 才能公開)
但是我們要如何訪問我們建立且公開的image呢?
那就要來取得網址拉，透過--urlflag取得網址
:::info
minikube service hello-minikube --url
:::
![](https://i.imgur.com/8CpisjV.png)

訪問輸出上的網址就可以看到我們部屬的實例了，內容大致如下


![](https://i.imgur.com/LvobGCb.png)



Delete the hello-minikube service

```
kubectl delete services hello-minikube
--> service "hello-minikube" delete
```

我們也可以透過 kubectl describe 去查看該Pod 的詳細資訊

:::info
kubectl describe pod hello-minikube-66d5c9996d-wpcn2
:::




### 容器化 Flask Web 應用程式

先建立一個 Python 的 Flask Web 應用程式，用來準備容器化．

[Code Ref: Python Web](https://seanyeh.medium.com/python-web-快速建置網站的flask套件-59318830bd63)

```python=
from flask import Flask

app = Flask(__name__)
@app.route("/index")
@app.route("/")
def home():
    return "This is Home Page"
@app.route("/hello")
def hello():
    return "This is Hello Page"
if __name__ == "__main__":
    app.run() 
```

#### Dockerfile

建立一個 Dockerfiile，意思是用 Python 3.7 作為基本的容器，並把專案中的程式碼放進去。
並在最後執行 python main.py

```
From python:3.8.10-slim                  

ENV PYTHONPATH "${PYTHONPATH}:/app"

EXPOSE 8888

WORKDIR /app

COPY requirements.txt ./

RUN pip install -r requirements.txt

COPY . . 

CMD python3 app/main.py -vv 
```

個人 Tree 架構

![](https://i.imgur.com/6FZeci2.png)


製作 docker image，並給他一個名稱flask_app
```
docker build . -t flask_app
```
可以成功 build，沒有問題
用 docker image ls 查看剛剛製作的 image
![](https://i.imgur.com/QYn4mhx.png)





## K3s

```
curl -sfL https://get.k3s.io | K3S_KUBECONFIG_MODE=644 sh -
```

[可能 Error](https://ithelp.ithome.com.tw/articles/10235147)
如果後面沒有設定 K3S_KUBECONFIG_MODE 在操作 k3s 會有錯誤訊息 WARN[2020-09-09T13:49:32.937048689+08:00] Unable to read /etc/rancher/k3s/k3s.yaml, please start server with --write-kubeconfig-mode to modify kube config permissions error: error loading config file "/etc/rancher/k3s/k3s.yaml": open /etc/rancher/k3s/k3s.yaml: permission denied


```
k3s kubectl get nodes
```

測試結果 (Server 端自己會是一個 Woker Node 的狀態)

![](https://i.imgur.com/HarLNKa.png)



---


**Certification**

[Linux Foundation](https://training.linuxfoundation.org/full-catalog/)

Linux 基金會是 Linux 創造者 Linus Torvalds 和主要維護者 Greg Kroah-Hartman 的家，并提供了一個中立的家，可以在未來幾年保護和加速 Linux 核心開發



## Google Kubernetes Engine （GKE）

GKE，全名是 Google Kubernetes Engine ，照字面上的意義就是在 Google 上的 K8s (Kubernetes)
Google Kubernetes Engine 於2015 年推出，最大的特色即是能快速擴張、自動擴充(Autoscale)，這項技術，除了能最快速支援新版 Kubernetes，Google 基於用容器運行 Gmail 和 YouTube 等服務的經驗，已有超過12年的歷史，並成功協助各產業的客戶決解擴展性問題，且加速部署速度和提高生產力。


![](https://i.imgur.com/wLyHxla.png)
