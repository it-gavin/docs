CISCO 教材連結:  https://learningspace.cisco.com/
CISCO 帳密: oldelette@gmail.com/Asdf850710@@@

Access token: 6b9yT1Mw19qXSKiXywc8
CISCO LAB: https://cll-ng.cisco.com/users/pblogin
2023052184331 bVfdGk5h
Instructor token: indma




[CCNA 考題](https://drive.google.com/file/d/1xBrpE_TMjFY9q67EvoWsiJRbYvGZ3oOC/view?usp=sharing)

---

2023.7.2 (下午) - [Vedio](https://www.youtube.com/watch?v=Pq1joFXkFiQ&ab_channel=gavin) - Explaining the TCP IP Transport Layer and Application Layer
FASTLAB-1 - [Vedio](https://www.youtube.com/watch?v=2zcS2a5dIWI&ab_channel=gavin)

2023.7.8 (早上) - [Vedio](https://www.youtube.com/watch?v=Qz-mll5lmTw&ab_channel=gavin) - Exploring the Function of Routing
2023.7.9 (下午) - [Vedio](https://www.youtube.com/watch?v=yctF5fi5mTY&ab_channel=gavin) - Exploring the Packet Delivery Process

2023.7.9 (早上) - [Vedio](https://www.youtube.com/watch?v=rVVC0wa3hZA&ab_channel=gavin) - 考試預約方法 + Introducing Basic IPv6
2023.7.9 (下午) - [Vedio](https://www.youtube.com/watch?v=CjvXbMhZRGs&ab_channel=gavin) - Configuring Static Routing

2023.7.15 (早上) - [Vedio](https://www.youtube.com/watch?v=_HDRbDUr_iY&ab_channel=gavin) - Configure VLANs and Trunks
2023.7.15 (下午) - [Vedio](https://www.youtube.com/watch?v=7PQk0jZaslc&ab_channel=gavin) - Routing Between VLANs

2023.7.22 (早上) - [Vedio](https://www.youtube.com/watch?v=na-TvtR_3hU&ab_channel=gavin) - Managing Cisco Devices
2023.7.22 (下午) - [Vedio](https://www.youtube.com/watch?v=lnlp1o2PA3c&ab_channel=gavin) - Examining the Security Threat Landscape

2023.7.23 (早上) - [Vedio](https://www.youtube.com/watch?v=WkY7uG0bAY0&feature=youtu.be&ab_channel=gavin) - Implementing Device Hardening
2023.7.23 (下午) - [Vedio](https://youtu.be/2i5VGRS2zns) - Introducing QoS

---

```
考試沒辦法回上一頁
```

## 網路元件

###  Topologies

[【CCNA 學習筆記】第一章：探索網路功能（Exploring the function of network）](https://ithelp.ithome.com.tw/articles/10232280?sc=rss.qu)

拓樸（Topology），類似網路元件的布置圖，方便我們辨識網路環境資料流走向與網路配置
:::success
Logical Topologies: 邏輯拓樸
是指資料流的方向。雖然網路線可能互相串連在一起，但是資料流不一定會照著實體網路設備連線方式傳輸，而是會按照預先設定好的方式，再加上最短路徑等演算法，計算最有效率的路徑。
由此計算出來的資料流方向，就是邏輯拓樸。
:::


#### Physical topology

* Physical topology is the physical layout of the devices and cabling
* The primary physical topology categories are bus, ring, star and mesh.

mesh: 每個點會至少會拉兩連線以上 (部線成本上升)
Bus 跟 Ring 已幾乎不在使用，最主流的是星狀拓樸，中間可能是一台交換器或路由器。交換器互相連來連去，當然就會變成網狀拓樸


![](https://hackmd.io/_uploads/BySSLfpun.png)


### Network Diagram

網路架構圖

這張圖的網路設備上有Te1/0/11,E1/1等符號與數字，代表網路介面，英文字母代表是哪一種型號的介面，如果開頭S指串列網路介面（Serial），Fa是指具有100Mbps傳輸速度的高速以太網路介面（Fast Ethernet）、Gi則是千兆乙太網路介面（Gigabit Ethernet）
後面的數字是指介面的位置，假設 Fa1/0/5，是指高速以太網路介面的第1埠（port），其介面卡的第0號插槽（slot）的5號模組（module）位置。

![](https://hackmd.io/_uploads/rkt3PzTO2.png)

### Network Applications

* ++Batch applications++
批次程式
例如使用FTP傳輸資料時，一旦開始傳輸，就可以放著不管了。即使傳輸到一半網路斷掉，只要網路恢復，就會繼續傳輸。頻寬大小也不是那麼重要。
不需要人為介入
頻寬很重要但是也非絕對
範例：FTP、TFTP，或者是Windows的更新都屬於這類
* ++Interactive applications++
互動型應用程式
相較於批次程式，互動型程式比較需要注重回應時間。雖然程式能夠快速回應很重要，例如點開連結能夠快速載入網頁元件，但是就算慢一點，也是能夠完成所有任務。
支援人機互動
可接受的回應時間，依據我們對於個別程式的重要性而有所不同
範例：網頁瀏覽器
* ++Real-time Applications++
實時應用程式
頻寬對於網路電話來說非常重要，如果有大量延遲，則會影響語音傳輸。可能會需要使用QoS（Quality of Service）服務，也就是提高特定資料封包的重要性，讓這些封包能夠優先傳輸。
支援人與人之間的互動
重視網路延遲時間（latency）
範例：Skype

## Host-to-host communication model

[【CCNA 學習筆記】第二章：主機對主機通訊模型介紹（Introduction the Host-to-host communication model）](https://ithelp.ithome.com.tw/articles/10310315)

Introduction the Host-to-host communication model 主機對主機通訊模型介紹
![](https://hackmd.io/_uploads/rkEMeQTOn.png)

The following are elements of host-to-host communication:
* Source
* Transmission media
* Destination


### Open System Interconnection Model

![](https://hackmd.io/_uploads/Bk_xwQ6_n.png)



## Operating Cisco IOS software

操作 Cisco網際網路作業系統
操作網路設備的方式之一, 可以在自己的電腦上,直接使用主控台連接線（console connection), 實體連線到設備的主控台連接埠（console port），再使用圖形化或命令列的方式，去操作網際網路作業系統，也就是殼層（shell）。
比較常見的方式會是使用命令列，雖然比較不直覺，但是習慣後速度也較快。
另一個方式就是透過網路，以SSH（Secure Shell）或Telent的協定去連線。

![](https://hackmd.io/_uploads/By6TiNaOh.png)


```
SSID Service Set Identifier
是服務集標識的縮寫，可簡單理解為互聯網使用者的無線網絡名稱
```

### Management 

有兩種管理模式
* Standalone Remote Management
* Centrailized Remote Management 

![](https://hackmd.io/_uploads/rkmaT4TOh.png)

![](https://hackmd.io/_uploads/Hy77xSTO3.jpg)


#### CISCO IOS Mode

* ++User EXEC Mode++

Cisco的系統分為三層，第一層是一般使用者模式（User Exec Mode），權限最小，只能使用一些查詢的語法，提示符（prompt）為>
第二層是特權模式（Priviledged Exec Mode），進去時可能會需要密碼，提示符為#
第三層是全域設定模式（Global Configuration Mode），在這一層設定的指令會影響整台機器。第三層再往下，就是針對個別的介面、連線等設定。

![](https://hackmd.io/_uploads/S1R7HB6O2.png)



#### Managing Cisco IOS Configuration

In addition to NVRAM and RAM, Cisco devices have a Read Only Memory (ROM) and flash memory. ROM is a form of permanent storage. 
This type of memory contains microcode for basic functions to start and maintain the router. ROM is nonvolatile, so it maintains the memory contents even when the power is turned off.

![](https://hackmd.io/_uploads/Bkq-HI6d2.png)



## LAN

區域網路簡介


![](https://hackmd.io/_uploads/B13-lPTuh.png)

區域網路有下列幾個元件
* 主機（hosts）：只要能夠接收或傳送資料的都可稱為主機，也可稱為終端。
* 互連技術（interconnections）
    * Network Interface Cards
    * network media 
* 網路設備
* 區域網路常用協定



### Switches

交換器在現代網路架構中具有非常重要的角色，在資訊量越來越大的世代，它可以減輕網路的負擔避免壅塞


![](https://hackmd.io/_uploads/rySuQv6d2.jpg)



### Others

CSMA 和 CSMA/CD 是兩種常見的網絡傳輸協議，它們都是用於在共享媒介上進行數據傳輸的方法，但在碰撞檢測和處理上有所不同。

```
Layer1 才會有碰撞
Layer2 才有全雙工
```

#### CSMA/CD

Carrier Sense Multiple Access with Collision Detection
載波偵測多重存取/碰撞偵測

CSMA/CD 運作過程如下
* 訊號採用廣播的方式傳送（所以才會發生碰撞）
* 當節點要發送訊號時，會先偵測通道是否有其他節點正在使用（carrier sense）
* 當通道沒有被其他節點使用時，就傳送封包
* 封包傳送之後立即檢查是否發生碰撞（carrier detection），若是發生碰撞則對通道發出高頻訊號高知其他節點已經發生碰撞
* 碰撞後隨機等待一段時間重新發送封包
* 嘗試 15 次都失敗的話則告知上層 Timeout

#### CSMA/CA
Carrier Sense Multiple Access 
載波偵測多重存取/碰撞避免

CSMA/CA 運作過程如下
* 訊號採用廣播的方式傳送（非常容易受到無線電波干擾）
* 當節點要發送訊號時偵測頻道是否空閒
* 若是空閒則等待 IFS, Interval Frame Space 時間後再次偵測頻道是否空閒
* 若是空閒則發送封包，反之重新進入等待頻道空閒（隨機等待時間）
* 發送 RTS 之後必須在限定時間內收到來至目的端的 CTS 訊號
* 當失敗 32 次之後通知上層 Timeout
* 此外 CSMA/CA 又可分為以下兩種類型
    * 分散式協調功能 (Distributed Coordination Function, DCF)：競爭式服務 (Contention Service)
    * 集中式協調功能 (Point Coordination Function, PCF)：無競爭式服務 (Contention-Free Service)




## TCP/IP link layer

探索 TCP/IP 連結層（Exploring the TCP/IP link layer)


### UTP

無遮蔽雙絞線 （unshielded twisted-pair, UTP)
乙太網路目前主流都是用無遮蔽雙絞線

![](https://hackmd.io/_uploads/HJdBvD6_3.png)



:::success
Straight-Through or Crossover UTP Cable ???
:::

平行線（straight-through）及跳線（crosssover）的差異。
在CCNA課程也會稍微講到，簡單來說，兩台交換器如果要對接，理論上應該使用跳線

通常只用到 1,2,3,6

![](https://hackmd.io/_uploads/SJN2DPpO3.png)


平行線就是指左側這張圖，
RJ-45接頭的插槽共有8個腳位（Pin），它會將左邊RJ-45接頭的腳位1，傳到右邊的腳位1，其餘以此類推


#### 平行線與跳線的使用時機

![](https://hackmd.io/_uploads/rkABOvTOh.png)


### Fiber

光纖又可分為單模光纖（single-mode fiber）及多模光纖（multimode fiber）
* single-mode fiber 使用雷射傳輸光訊號，傳輸距離較長
* multimode fiber 使用LED傳送光訊號，傳輸距離較短

![](https://hackmd.io/_uploads/HkVqdP6_2.png)


### Ethernet Frame

訊框（frame）是指在OSI第二層資料鏈結層傳遞的訊息，目前規格主要是乙太二(Ethernet II)
組成方式如下表



| Bytes | 8 | 6 | 6| 2| 46-1500	| 4 |
| -------- | -------- | -------- | -- |-- |-- | --|
| 內容    | 前置訊號(Preamble)  | 目標MAC位址(Destination MAC Address)    |來源MAC位址(Source MAC Address) | 類型(Type)| 訊息內容(Payload) | 訊框檢查序列(Frame Check Sequence)|

## Introducing LANs


### LAN Communication Types

![](https://hackmd.io/_uploads/BydiGLCOh.png)


* ++Unicast frames forwarding:++
The switch examines the destination MAC address and if it is unicast, performs one of the following action depending whether the MAC address is present in the MAC address table.
* ++Broadcast frames forwarding:++
The switch examines the destination MAC address and if it is boardcast or multicast

### MAC位址

MAC位址(Media Access Control Address)可直譯為媒體存取控制位址
習慣上還是直接稱為MAC位址，可以視為網卡的編號。MAC位址主要是在OSI模型第二層資料鏈結層使用，也就是乙太網路、Wi-Fi、藍芽.

:::danger
Mac address is composed of 12 hexadecimal digits, it's 48 bits long.
:::

![](https://hackmd.io/_uploads/HyppNLAO3.png)




#### Frame switching

Frame switching refers to the process of forwarding network data frames from one interface to another within a network switch

:::warning
The MAC address table is stored in the content-addressable memory(CAM), which enables very fast lookups.
MAC table referred to as a CAM table.
:::

![](https://hackmd.io/_uploads/ByS2ywR_h.png)


## Starting a Switch

在企業網路中，交換器通常都是座落在網路的中心位置，向外輻射狀連結著各類主機

![](https://hackmd.io/_uploads/HJ_JqDRO2.png)


### Physical installation and startup of a Catalyst switch requires completion of these steps:
1. Before performing physical installation, verify the following:
    * Switch power requirements
    * Switch operating environment requirements (operational temperature and humidity)
2. Use the appropriate installation procedures for rack mounting, wall mounting, or table or shelf mounting.
3. Before starting the switch, verify the network cables that provide connectivity to end devices to the local-area network (LAN).
4. Attach the power cable plug to the power supply socket of the switch. The switch will start. Some Catalyst switches do not have power buttons.
5. Observe the boot sequence:
    * When the switch is on, power-on self test (POST) begins. During POST, the switch LED indicators blink while a series of tests determines that the switch is functioning properly.
    * The Cisco IOS Software output text is displayed on the console.

![](https://hackmd.io/_uploads/SJavjK0u3.png)



## Introducing the TCP/IP Internet Layer, IPv4 Addressing, and Subnets

---



:::warning
Router 和 Switch 的差異 ??
:::
Router 及 Switch 的功能不一樣
Switch 的交換速度是很快的，Router路由器是相對較慢的
* Router 主要負責路由，也就是該封包要怎麼被轉送到另一個子網路
* Switch 是用於區域網路 (LAN) 的交換，在區域網路裡，設備之間是用MAC去溝通而不是IP


## Exploring the Function of Routing

2023.7.8 早

Cisco router 中有四個主要内存區域，熟悉他們很重要，即 Flash，RAM，ROM 和 NVRAM.

![](https://hackmd.io/_uploads/H1qN7Y8t3.png)


## Exploring the Packet Delivery Process

2023.7.8 午

### Layer 2 Addressing

In most cases, Layer 2 network devices, like bridges and switches, are not assigned a different MAC address to every Ethernet port on the switch for the purpose of transmitting or forwarding traffic. 
These Layer 2 devices pass traffic, or forward frames, transparently at Layer 2 to the end devices.

![](https://hackmd.io/_uploads/Bk_hFFUKn.png)




### Layer 3 Addressing

Some network operating systems (NOS) have their own Layer 3 address format. 
For example, the Novell IPX Protocol uses a network service address along with a host identifier. 
However, most operating systems today, Including Novell, can support TCP/IP, which uses a logical IP address at Layer 3 for host-to-host communication.

![](https://hackmd.io/_uploads/BJqaFYUFn.png)


## Protocol

### Layer2

OSI Layer 2 連結層
Layer 2 是掌管 IP 底下的連結，通常不外乎與實體傳輸密切相關，像是網路拓樸結構探測或者是與 IP 配發有關.

比較常見的 L2 協定有以下一些:
* DHCP
* LLDP
* CDP
* ARP
* STP
* VLAN

#### LLDP (Link Layer Discovery Protocol)

連結層發現協定
:::info
Link Layer Discovery Protocol (LLDP, IEEE 802.1AB)
The Link Layer Discovery Protocol (LLDP) is a vendor neutral layer 2 protocol that can be used by a station attached to a specific LAN segment to advertise its identity and capabilities and to also receive same from a physically adjacent layer 2 peer.
:::

常用的 Layer２ 發現協定通常是供應商專有的，
例如 Cisco 的 CDP、Foundry 的 FDP、Extreme 的 EDP 和 Nortel 的 NDP.
這使得 Layer２ 在異質環境中的發現變得困難.
為了解決這個問題，IETF 引入了一種標準的與供應商無關的配置交換協定，即 LLDP

使用 LLDP，將底盤識別、port ID、port description、系統名稱和描述、設備功能（如路由器、開關、集線器、IP/MAC 位址等）等設備資訊傳輸到相鄰設備.
此資訊 還存儲在本地管理資訊資料庫（MIB）中，可以使用簡單的網路管理協定 （SNMP） 查詢


:::warning
LLDP/CDP 協定都是設備探測協議，可以偵測到線的另一端接的是甚麼樣的設備、韌體版本、廠牌等其他資訊
:::


#### CDP (Cisco Discovery Protocol)

Cisco Discovery Protocol (CDP) 是 Cisco 獨家的 Protocol，只可在 Cisco 產品上執行.
透過 CDP，網管人員可以查看該設備的物理連接，得到相鄰設備的資訊，例如：型號、IOS 版本等等.
現在一般都會使用 CDP Version 2，而 Cisco 設備預設亦會是 Version 2


![](https://hackmd.io/_uploads/HyYkZYUYh.png)

用上圖網絡為例子，由於 CDP 預設是開啟的，因此只要輸入 show cdp neighbors 即可看到相鄰 Router 的資料.

以下用 R1 作示範:
![](https://hackmd.io/_uploads/rkQW-F8Yn.png)


* ++Device ID++
相鄰設備的 hostname.
* ++Local Intrfce++
即 Local Interface，是本設備連接相鄰設備的Interface.
* ++Holdtme++
CDP packet 預設每 60 秒傳送一次，如果過了預設 Hold Time 180 秒也收不到 CDP packet，則判斷對方已死掉.所以 Hold Time 會由 180 秒開始倒數，直至收到 CDP packet 再回到 180 秒，如果 Hold Time 數到 0 也收不到 CDP packet 的話，此條紀錄便會被移除.
* ++Capability++
相鄰設備是一件什麼東西呢？只要查看 Capability Code 便知道了.
* ++Platform++
相鄰設備的型號.
* ++Port ID++
正在連接相鄰設備的那一個 Interface

