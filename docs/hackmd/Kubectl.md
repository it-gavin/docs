###### tags: `Work`

# Kubectl

[從題目中學習k8s](https://ithelp.ithome.com.tw/articles/10234562)


## Tips

[Certification Tip!](https://www.udemy.com/course/certified-kubernetes-administrator-with-practice-tests/learn/lecture/14937836#overview)  - 需登入 udemy
[kubectl Usage Conventions](https://kubernetes.io/docs/reference/kubectl/conventions/)

### 查看 K8s 所有物件和它們的縮寫
:::info
kubectl api-resources
:::


### Pod & yaml
* 創建出一個基本YAML，再去修改
:::info
kubectl run nginx-kusc00101 --image=nginx --restart=Never --dry-run=client -o yaml> xxx.pod 
:::

--dry-run=client 參數用來預覽 而不會真正提交到 cluster 集群中

++--dry-run++: By default as soon as the command is run, the resource will be created. 
If you simply want to test your command , use the ++--dry-run=client++ option. 
This will not create the resource, instead, tell you whether the resource can be created and if your command is right.

++-o yaml++: This will output the resource definition in YAML format on screen.



### Deployment

* Create a deployment
:::info
kubectl create deployment --image=nginx nginx
:::

* Generate Deployment YAML file (-o yaml). Don't create it (--dry-run)
  Generate Deployment YAML file (-o yaml). Don't create it(--dry-run) with 4 Replicas (--replicas=4)
:::info
kubectl create deployment --image=nginx nginx --dry-run=client -o yaml
kubectl create deployment --image=nginx nginx --replicas=4 --dry-run=client -o yaml > nginx-deployment.yaml
:::


### Service

* 將Pod expose出去 (創建一個Service)
:::info
kubectl expose po \<pod-name\> --type=NodePort --name=\<svc-name\> --port=80
kubectl expose pod nginx --type=NodePort --port=80 --name=nginx-service --dry-run=client -o yaml
:::


## Core Concepts




### 1. Label

[Ref](https://ithelp.ithome.com.tw/articles/10236866)
![](https://i.imgur.com/9pnxx4N.png)

---

* Answer
```
## 先透過kubectl run命令建立Pod基本YAML
kubectl run nginx-kusc00101 --image=nginx --restart=Never --dry-run=client -o yaml> q1.pod
## 將YAML 修改 (新增下方圖片紅字內容)
## 創立物件
kubectl apply -f q1.pod
```
nodeSelector 是 Pod 到某 Node 上最簡單、直接的調度方式，直接利用 label選取即可

![](https://i.imgur.com/sI3n5R6.png)

![](https://i.imgur.com/rwzGjMY.png)



是個Pod Scheduling 的概念題，也就是如何將 Pod運行在適合的 Node上

:::success
label 是一個 [key:value] 的結構，以 key 對應的 value 來區隔它們屬性的異同
label 可以被附加到各種 resource-objects 上，例如 Node、Pod 或 Service等..
一個 label 可以被增加到任意數量的物件上；一個物件也可以添加任意數量的 label

常用的 label 包含 release(版本)、environment(環境)、tier(架構)、partition(分區)、track(品質管控)等。
:::


相關label指令
:::warning
查看Pod label
kubectl get po --show-labels
查看特定label的Pod
kubectl get po --selector \<key\>=\<value\>
kubectl get po --selector \<key1\>=\<value1\>,\<key2\>=\<value2\>
新增Pod label
kubectl label po \<pod-name\> \<key\>=\<value\>
刪除Pod label
kubectl label po \<pod-name\> \<key\>-
:::

想要删除一個 Label，只需在命令最後指定 Label 的 key 名稱與一個減號相連即可：

![](https://i.imgur.com/6RSRY2i.png)



### 2. Deployment 


#### Deployment部署

Deployment 可以達成以下幾件事情：
* 部署一個應用服務 (application)
* 協助 applications 升級到某個特定版本
* 服務升級過程中做到無停機服務遷移 (zero downtime deployment)
* 可以 Rollback 到先前版本




| Deployment相關指令 | 指令功能 |
| -------- | -------- | 
| kubectl get deployments |  取得目前Kubernetes中的deployments的資訊  | 
| kubectl get rs |  取得目前Kubernetes中的 Replication Set 的資訊  | 
| kubectl describe deploy \<deployment-name\>	  |  取得特定deployment的詳細資料  | 
| kubectl set image deploy/ \<deployment-name\> \<pod-name\>: \<image-path\>:\<version\>    | 將 deployment 管理的 pod 升級到特定image版本	   | 
| kubectl edit deploy \<deployment-name\>    |  編輯特定 deployment 物件	  | 
| kubectl rollout status deploy \<deployment-name\>	    | 查詢目前某deployment升級狀況	   | 
| kubectl rollout history deploy \<deployment-name\>	    | 查詢目前某deployment升級的歷史紀錄	   | 
| kubectl rollout undo deploy \<deployment-name\>  |  回滾 Pod到先前一個版本  | 
| kubectl rollout undo deploy \<deployment-name\> --to-revision=n |  回滾 Pod到某個特定版本	  | 


---

```
k describe deployments <deployments-name>
```

kubectl describe deployment 兩項重要資訊：
* OldReplicaSets
* NewReplicaSet

這兩項代表著 deployments 所管理得 replicaset 物件。
如果 rollout 進行中，則兩個欄位都有資訊。
如果 rollout 完成了，則 OldReplicaSets 會顯示 資訊。

![](https://i.imgur.com/zQNYl35.png)


#### Example - Deployment

![](https://i.imgur.com/a9IMZxk.png)

這題的重點是 pod scheduling
* ++PodAntiAffinity++
The idea here is that we create a "Inter-pod anti-affinity" which allows us to say a Pod should only be scheduled on a node where another Pod of a specific label (here the same label) is not already running

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: deploy-important
  namespace: project-tiger
  labels:
    id: very-important
spec:
  replicas: 3
  selector:
    matchLabels:
      id: very-important
  template:
    metadata:
      labels:
        id: very-important
    spec:
      containers:
      - name: container1
        image: nginx:1.17.6-alpine
      containers:
      - name: container2
        image: kubernetes/pause
      affinity:
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchExpressions:
              - key: id
                operator: In
                values:
                - very-important
            topologyKey: "kubernetes.io/hostname"
```



### 2.1 ReplicaSet 複製集

![](https://i.imgur.com/sb9Q0FU.png)
圖中可以看到一個 Deployment 掌管一或多個 ReplicaSet，而一個 ReplicaSet 掌管一或多個 Pod

:::info
Kubernetes 官方强烈建議避免直接使用 ReplicaSet，應該通過 Deployment 來建立 RS 和Pod
:::

ReplicaSet 是用來確保在 k8s 中，在資源允許的前提下，指定的 pod 的數量會跟使用者所期望的一致，也就是所謂的 desired status

* ++常用的 replicaset command++
```
kubectl create -f replicaset-definition.yml
kubectl get replicaset
kubectl delete replicaset myapp-replicaset (Also deltes all underlying PODs)
kubectl replace -f replocaset-definition.yml
kubectl scale -replicas=6 -f replocaset-definition.yml (擴展 replicas)
kubectl edit rs new-replica-set (編輯 rs)
```


[Ref- ReplicaSet 介紹]
![](https://i.imgur.com/DIm2ybF.png)


```
# my-replica-sets.yaml
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: my-replica-set
spec:
  replicas: 3
  selector:
    matchLabels:
      env: dev
    matchExpressions:
      - {key: env, operator: In, values: [dev]}
      - {key: env, operator: NotIn, values: [prod]}
  template:
    metadata:
      labels:
        app: hello-pod-v1
        env: dev
        version: v1
    spec:
      containers:
      - name: my-pod
        image: zxcvbnius/docker-demo
        ports:
        - containerPort: 3000
```

* ++spec.selector.matchLabels++
在 Replica Set的selector 裡面提供了 matchLabels，matchLabels的用法代表著等於(equivalent)，代表Pod的labels必須與matchLabels中指定的值相同，才算符合條件。

* ++spec.selector.matchExpressions++
matchExpressions 的用法較為彈性，每一筆條件主要由三個部分組成key, operator，value.
以 my-replica-sets.yaml 中敘述為例，我們指定Pod的條件為 1) env必須為dev 2) env不能為prod。
目前operator支援4種條件: In, NotIn, Exists, DoesNotExis


### 2.2 Replication Controller

:::success
Replication Controller 就是 Kubernetes 上用來管理 Pod 的數量以及狀態的 controller
* 每個 Replication Controller 都有屬於自己的 yaml 檔
* 在 Replication Controller 設定檔中可以指定同時有多少個相同的 Pods 運行在 Kubernetes Cluster上
* 當某一 Pod 發生 crash, failed，而終止運行時，Replication Controller會幫我們自動偵測，並且自動創建一個新的Pod，確保 Pod運行的數量與設定檔的指定的數量相同
:::
透過 Replica 設定我們就可以快速產生一樣內容的 Pod
舉例來說：今天設定了 replica: 2 就代表會產生兩個內容一樣的 Pod 出來。
![](https://i.imgur.com/k5LOx1S.png)


```
kubectl create -f q3.yaml
kubectl get replicationcontroller
```


![](https://i.imgur.com/GuGoOFi.png)

```
# q3.yaml
apiVersion: v1                                                      
kind: ReplicationController
metadata:
  name: myapp-rc
  labels:
    app: myapp
    type: front-end
spec:
  template:
    metadata:
      creationTimestamp: null
      labels:
        run: test
      name: test
    spec:
      containers:
      - image: nginx:1.11.0-alpine
        name: test
  replicas: 3
```

* ++spec.replicas & spec.selector++
在spec.replicas中，我們必須定義 Pod的數量，以及在spec.selector中指定我們要選擇的Pod的條件 (labels)
* ++spec.template++
在spec.template 中我們會去定義pod的資訊，包含Pod的labels以及Pod中要運行的container。


### 3. Service

[Ref 從題目中學習k8s](https://ithelp.ithome.com.tw/articles/10238233)
![](https://i.imgur.com/rxhUfNu.png)

---

* Answer
```
## 透過kubectl expose命令創建Service
sudo kubectl expose pod kubernetes-demo-pod --type=NodePort --port=80
```


[Ref - Pod 進階應用 : Service、Deployment、Ingress](https://hackmd.io/@tienyulin/kubernetes-service-deployment-ingress)

:::success
Service 是一個抽象化的物件，它定義了一群的 Pod 和存取這些 Pod 的規則
簡單來說就是要讓使用者和 Kubernetes Cluster 進行隔離，使用者只需要告訴 Service 我的請求而不需要知道誰會幫我處理也不需要知道處理的過程和細節，Service 收到請求後就會依照定義的規則將請求送到對應的 Pod
Pod 和 Pod 之間的也會透過 Service 來存取。
:::

![](https://i.imgur.com/XGdNEJC.png)


#### kubectl expose
該指令可以幫我們創建一個新的 Kubernetes Service 物件，來讓Kubernetes Cluster中運行的 Pod與外部互相溝通
```
kubectl expose pod <pod name> --type=<service type> --name=<service name> \
--port=<port> --target-port=<target port>
```


#### Service Nodeport

Service 的類型:
:::success
* ClusterIP : 預設類型,使用 Cluster 內部的 IP 來暴露 Service，因此只能在 Cluster 內部存取。
* NodePort : 使用 Node 的 IP 和 Port 來暴露 Service，在 Cluster 外可以透過 NodeIP: NodePort 來存取 Service。
而 Cluster 內部所需的 Cluster IP 會自動被建立。
* LoadBalancer : 使用雲端供應商提供的 LoadBalancer 來開放 Service，會自動建立相對應的 NodePort 和 Cluster IP。
* ExternalName : 將 Service 關聯到 ExternalName，也就是 Domain。
例如 : foo.sample.com。
:::


![](https://i.imgur.com/B5u9XNy.png)

* ++apiVersion++
Service使用的Kubernetes API 是 v1版本號
* ++spec.type++
可以指定 Service的型別，可以是 NodePort 或是 LoadBalancer 等等..
* ++spec.ports.port++
創建 Service 的 Cluster IP，是哪個port number去對應到targetPort
* ++spec.ports.nodePort++
可以指定 Node 物件是哪一個 port numbrt，去對應到targetPort，若是在Service的設定檔中沒有指定的話，Kubernetes會隨機幫我們選一個port number
* ++spec.ports.targetPort++
targetPort 是我們指定的 Pod 的 port number，由於我們會在Pod中運行一個 port number 3000 的 web container，所以我們指定hello-service的特定 port number都可以導到該web container
* ++spec.ports.protocol++
目前 Service 支援TCP與UDP兩種protocl，預設為TCP
* ++spec.selector++
selector 會幫忙選擇 pod (use label)

[\<NodePort example\>](https://ithelp.ithome.com.tw/articles/10194344)
```
apiVersion: v1
kind: Service
metadata:
  name: hello-service
spec:
  type: NodePort
  ports:
  - port: 3000
    nodePort: 30390
    protocol: TCP
    targetPort: 3000
  selector:
    app: my-deployment
```

![](https://i.imgur.com/e54szMJ.png)






相關 service 指令
:::warning
kubectl expose pod kubernetes-pod --type=NodePort
\#\# 查看建立的 Service
kubectl get services
\#\# 查看 Service 的詳細資訊
kubectl describe services 
:::





### 4. Namespace

[Ref](https://ithelp.ithome.com.tw/articles/10238974)
![](https://i.imgur.com/JQeAd3s.png)


---

* Answer
```
kubectl create ns website-frontend
kubectl run jenkins --image=jenkins --restart=Never --dry-run=client -o yaml > q4.yaml
kubectl apply -f q4.yaml -n website-frontend
```


:::success
藉由 namespace 可以作到 project 的 resources 的隔離，使不同的 project 可以有相同名字的 deployment, services.
可以當作在 cluster 又在加上一層 virtual cluster 的分割，這個好處是不需要多開實體的 cluster 裡面又包含了 master node 的相關服務
* 同一個 namespace 的資源名稱是唯一性
* 不同 namespaces 的資源名稱可以相同
:::

![](https://i.imgur.com/6nxaHQV.png)



K8s 預設存在的 namespace 有三個:
* ++default++
K8s集群建立時自動創建，一般對K8s物件的操作都在default namespace中完成
* ++kube-system++
K8s在集群建立時會為了某些目的創建一系列的Pod和Service，例如weave net CNI、DNS Service等，通常是一些維持集群運作的重要物件
為了預防這些物件不小心被使用者刪除或更改導致集群毀損，必須將這些物件獨立於其它物件，因此會將這些物件放置於kube-system這個namespace之下
* ++kube-public++
此namespace也是由K8s自動創建，當有些resources需要讓所有user都能access到時，會利用此namespace

相關namespace指令
:::warning
\#\# 查看\<namespace-name\>下的pod
kubectl get po --namespace=\<namespace-name\>
kubectl get po -n \<namespace-name\>

\#\# 創建namespace
kubectl create ns \<namespace-name\>

\#\# 查看所有namespace下的pod
kubectl get po -A (--all-namespaces)
    
\#\# 刪除單一 Namespaces 
kubectl delete namespaces \<namespace-name\>

\#\#切換 Namespaces 
kubectl config set-context $(kubectl config current-context) --namespace=dev
kubectl config view | grep namespace
:::

* 切換 namespace example
![](https://i.imgur.com/eKAdWlx.png)


```
# my-namespace.yaml
apiVersion: v1
kind: Namespace
metadata:
  name: dev
```


* 刪除 example
![](https://i.imgur.com/wFGPdv2.png)
![](https://i.imgur.com/4pB0jdD.png)





### 5. Imperative vs Declarative

[Ref-資源定義與使用](https://ithelp.ithome.com.tw/articles/10240455)

<資源對象管理方式>
在 kubectl 中有三種方式管理資源
* ++Imperative commands++
透過指令方式管理資源, 通常於開發或測試使用
ex: kubectl edit
* ++Imperative object configurations++
ex: 建立, 刪除, 替換
```
kubectl create -f FILE.yaml
kubectl delete -f FILE.yaml
kubectl replace -f FILE.yaml
```
* ++Declarative object configurations++
使用檔案描述，底層由 kubernetes 維護，也就是其他未定義欄位。因此會針對先前修改過的欄位比較，並進行刪或更新動作。
**相較於 create 是去創建，而 apply 則是維護**

![](https://i.imgur.com/N2CNscT.png)


![](https://i.imgur.com/ffuHIpJ.jpg)


## Scheduling

[Scheduling.pdf](https://github.com/oldelette/oldelette.github.io/blob/master/Udemy/Section3-Scheduling/Kubernetes%2B-CKA-%2B0200%2B-%2BScheduling.pdf)

### 0. Scheduling - nodeName & nodeSelectors

Scheduling就是決定Pod在建立時，會被安排到哪個Node上 (cluster 集群預設是 **Scheduler**來負責這件事)
* ++Manual Scheduling++
會需要手動做 Scheduling 的情況為集群中沒有Scheduler時
如果沒有Sheduler，準備建立的Pod之狀態會停留在Pending，等待系統將其綁定到某個節點上運行

:::success
kubectl get pods --namespace kube-system --> 用這個指令看是否缺少 sheduler
:::

#### nodeName

##### 方法一: Binding
使用Binding來幫它綁定到某個Node上

```
apiVersion: v1
kind: Binding
metadata:
  name: nginx # 會自動找這個名字的物件
target:
  apiVersion: v1
  kind: Node
  name: node01
```
這邊 metadata.name 所設定的物件名稱，系統會自動根據這個名稱去尋找，找到這個名稱的物件並綁定到target所指定的Node

##### 方法二: Edit pod yaml

在原本的 YAML中，使用 **nodeName** 來指定Pod到某個Node.
(要注意的是，Pod其實是無法直接更新的，一定要透過刪除再重建來更新)

![](https://i.imgur.com/wecbjPH.png)


#### nodeSelectors

透過設置 nodeSelector 來達成 Node Affinity 的需求

```
apiVersion: v1
kind: Pod
metadata:
  labels:
    run: test
  name: test
spec:
  containers:
  - image: nginx
    name: test
  nodeSelector:
    size: Large 
```


### 1. Taints & Tolerations

[taints-tolerations.pdf](https://github.com/oldelette/oldelette.github.io/blob/master/Udemy/Section3-Scheduling/Udemy%2BKubernetes%2Btaints-tolerations.pdf)

taint 跟 node affinity 雖然都是屬於 scheduling 的一部份，但要達成的目的其實完全相反：
:::info
node affinity (親和性調度)：設計如何讓 pod 被分派到某個 worker node
taint (汙點)：設計讓 pod 如何不要被分派到某個 worker node
:::

* ++Taints (污點)++: 是讓 Node 能排斥特定類型的 Pod
* ++Tolerations (容忍度)++: 是應用在Pod上的屬性，允許(但不硬性要求)Pod 調度到帶有與之匹配的Taints 的 Node 上.

---
![](https://i.imgur.com/Hia2ShD.png)

pod D 有標示 Tolerations: app=blue 僅代表可以接受的 node (非強制)
以這張圖為例子, pod D 不一定只能在有標示 Taints: app=blue 上的 node1 執行, 他也可在無任何限制下的 node3 上執行


```
kubectl describe nodes <node-name> | grep -i taints
```

![](https://i.imgur.com/Nk2x2Tg.png)

++Master node++: 基於安全性考量情況下，k8s 中 master node是不會部署pod在上面
(可以看到有一個 Taint setting 是說 Noschedule on node-role.kubernetes.io\/master)

---

每個 taint 都有以下 3 個屬性：
* Key
* Value
* Effect: NoSchedule & PreferNoSchedule & NoExecute

:::success
effect 欄位是指：當 Pod 因為此 Taint 而無法調度到該節點上的時候，該怎麼處理
:::

```
#增加 taint
kubectl taint nodes node1 key=value:tain-effect
#移除 taint (在 taint 的 Key:Effect 後面加上 -)
kubectl taint nodes controlplane node-role.kubernetes.io/control-plane:NoSchedule-
```

![](https://i.imgur.com/hDK3XH8.png)

![](https://i.imgur.com/j1bXG49.png)


```
apiVersion: v1
kind: Pod 
metadata:
  name: test
spec:
  containers:
  - image: nginx
    name: test
  tolerations:
  - key: "app"
    operator: "Equal"
    value: "blue"
    effect: "NoSchedule"
```

* ++operator 是操作符，一般會有這幾種++
:::warning
Exists：某個Label 存在  (無須指定value)
In：Label 的值在某個列表中
NotIn：Label 的值不在某個列表中
Gt：Label 的值大於某個值
Lt：Label 的值小於某個值
DoesNotExist：某個 Label 不存在
:::



#### Taints/Tolerations and Node Affinity

有時候想要的效果需要組合 Taints/Tolerations + Node Affinity 才能辦到

---

![](https://i.imgur.com/WtcLJjT.png)

---


### 2. Affinity & Anti-Affinity

Affinity/Anti-Affinity 指的是親和性調度與反親和性調度，也就是滿足特定條件後，(不)將Pod調度到特定群集上
Node Affinity 和 Node Selector不同的是，它會有更多細緻的操作，你可以把Node Selector看成是簡易版的 Node Affinity

:::info
nodeAffinity有兩種策略
* 1.preferredDuringSchedulingIgnoredDuringExecution: 軟策略（可以不滿足)
* 2.requiredDuringSchedulingIgnoredDuringExecution: 硬策略（一定要滿足）
* requiredDuringSchedulingRequiredDuringExecution (planned)
:::


上面策略可以分成4段來看
|  | DuringScheduling | DuringExecution |
| -------- | -------- | -------- |
|  Type1  | Required  | Ignored  |
|  Type2  | Preferred  |  Ignored |
|  Type3  | Required  |  Required |

* 左右兩邊內容相同
![](https://i.imgur.com/wuf4Bn4.png)


---

[Schedule a Pod using required node affinity](https://kubernetes.io/docs/tasks/configure-pod-container/assign-pods-nodes-using-node-affinity/)
做題目的時候，先利用 [k8s.io document](https://kubernetes.io/docs/home/) 去搜尋 example 來改
```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: blue
spec:
  replicas: 3
  selector:
    matchLabels:
      run: nginx
  template:
    metadata:
      labels:
        run: nginx
    spec:
      containers:
      - image: nginx
        imagePullPolicy: Always
        name: nginx
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchExpressions:
              - key: color
                operator: In
                values:
                - blue
```
requiredDuringSchedulingIgnoredDuringExecution 代表了硬需求
:::warning
注意到這裡寫的 nodeSelectorTerms，在這個區塊底下，如果沒有寫 matchExpressions 的話，那麼在它底下所列出的條件，只要滿足其中一項就可以了，但在這個例子有寫 matchExpressions，所以在 matchExpressions 底下所列的條件全都要滿足才會調度
:::

#### Affinity 比較表

![](https://i.imgur.com/sRbNa3n.png)


### 2.1 Pod Affinity

和nodeAffinity類似，podAffinity也有requiredDuringSchedulingIgnoredDuringExecution和preferredDuringSchedulingIgnoredDuringExecution兩種調度策略
唯一不同的是如果要使用互斥性，我們需要使用 **podAntiAffinity字段**


* ++範例說明:++
這個設定是要在同個名稱的 Node 底下，要部署 Label 中 app=helloworld 的Pod，而不要部署Label 中 app=node-affinity-pod 的 Pod
```
apiVersion: v1
kind: Pod
metadata:
  name: nodehelloworld.example.com
  labels:
    app: helloworld
spec:
  containers:
  - name: k8s-demo
    image: 105552010/k8s-demo
    ports:
    - name: nodejs-port
      containerPort: 3000
  affinity:
    podAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
      - labelSelector:
          matchExpressions:
          - key: app
            operator: In
            values:
            - helloworld
        topologyKey: kubernetes.io/hostname
    podAntiAffinity:
      preferredDuringSchedulingIgnoredDuringExecution:
      - weight: 1
        podAffinityTerm:
          labelSelector:
            matchExpressions:
            - key: app
              operator: In
              values:
              - node-affinity-pod
          topologyKey: kubernetes.io/hostname
```


---

### 3. Kubernetes Resources - Request/Limit

Kubernetes 需要考慮如何在優先度和公平性的前提下提供資源的利用率，為了實現資源被有效調度和分配時同時提高資源利用率
Kubernetes 提供了 Request/Limit 兩種限制類型讓我們對資源進行分配。

:::success
* ++request++
容器使用的最小資源要求，做為容器調度時資源分配的判斷依賴
只有當前節點上可分配的資源量 >= request 時才允許將容器調度到該節點上。
* ++limit++
容器能使用的最大值。
設置為 0 表示對使用的資源不做限制，可以無限使用
:::

下面的 Pod 含有兩個容器. 
1.每個容器都有 resource requests, 0.25 cpu 以及 64MiB 記憶體
2.每個容器都有 resource limits 0.5 cpu 以及 128 MiB 記憶體
總體來說, 這個 Pod resource limits 1 cpu 以及 256 MiB 記憶體
```
apiVersion: v1
kind: Pod
metadata:
  name: frontend
spec:
  containers:
  - name: app
    image: images.my-company.example/app:v4
    resources:
      requests:
        memory: "64Mi"
        cpu: "250m"
      limits:
        memory: "128Mi"
        cpu: "500m"
  - name: log-aggregator
    image: images.my-company.example/log-aggregator:v6
    resources:
      requests:
        memory: "64Mi"
        cpu: "250m"
      limits:
        memory: "128Mi"
        cpu: "500m"
```

#### Exceed limit

* throttle (節流)
* terminate (終止)

![](https://i.imgur.com/LF00LLU.png)



#### Increase pod resource


:::info
The status OOMKilled indicates that it is failing because the pod ran out of memory.
:::

$ k describe pods elephant
下面這張圖清楚表示 pod 會死掉的原因, 因為 elephant pod 需要 15Mi 的 memory, 但是 limit 是 10Mi, 所以導致 OOMKilled.
![](https://i.imgur.com/7H3gNUW.png)

要調整 Memory Limit 到 20Mi
```
(o) kubectl replace --force -f /tmp/kubectl-edit-996911536.yaml
(x) kubectl edit pod elephant --> 不能直接調整 running pod resource limit
```
![](https://i.imgur.com/95LbxBq.png)


### 4. Demonsets

DamonSet 會確保在所有(或是特定)節點上，一定運行著指定的一個Pod，並且每當有新的Node加入Cluster時，DaemonSet會為他們新增這指定的一個Pod，同時只要有Node被移除Cluster外，在這Node上的指定Pod也會被移除

![](https://i.imgur.com/CXSKKpi.png)

應用場景: 
* Monitoring (Prometheu, Datadog)
* Log Viewer (fluent-bi, fluentd, logstash)
* Storage (glusterd, ceph)

```
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: fluentd-elasticsearch
spec:
  selector:
    matchLabels:
      name: fluentd-elasticsearch
  template:
    metadata:
      labels:
        name: fluentd-elasticsearch
    spec:
      containers:
      - name: fluentd-elasticsearch
        image: quay.io/fluentd_elasticsearch/fluentd:v2.5.2
```


* ++k describe daemonsets --namespace=kube-system++
觀察有多少 pod scheduled by DaemonSet kube-proxy --> 1個

![](https://i.imgur.com/pBA7ap7.png)

* ++daemonset create++
```
kubectl create deployment elasticsearch --namespace=kube-system \
--image=registry.k8s.io/fluentd-elasticsearch:1.20 --dry-run=client -o yaml > q1.yaml
```
創建 daemonset yaml (因為 kubectl 沒有 kubectl create daemonset command 所以先用 kubectl create deployment 再去做修正)

#### Example

![](https://i.imgur.com/4G1XOlG.png)

[k8s.io - Create a DaemonSet](https://kubernetes.io/docs/concepts/workloads/controllers/daemonset/#create-a-daemonset)
```
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: ds-important
  namespace: project-tiger
  labels:
    id: ds-important
    uuid: 18426a0b-5f59-4e10-923f-c0e078e82462
spec:
  selector:
    matchLabels:
      id: ds-important
      uuid: 18426a0b-5f59-4e10-923f-c0e078e82462
  template:
    metadata:
      labels:
        id: ds-important
        uuid: 18426a0b-5f59-4e10-923f-c0e078e82462
    spec:
      tolerations:
      # these tolerations are to have the daemonset runnable on control plane nodes
      # remove them if your control plane nodes should not run pods
      - key: node-role.kubernetes.io/master
        effect: NoSchedule
      containers:
      - name: ds-important
        image: httpd:2.4-alpine
        resources:
          requests:
            cpu: 10m
            memory: 10Mi
```




---

:::info
監控類型使用 DaemonSet（每一個Node都一定要有的服務）
有特殊需求可以使用 StatefulSet
沒任何要求就使用 Deployment，因為可以 rollback
:::

### 5. StatefulSet

透過 Deployment 部署的 POD，他都是 stateless, statefulset 跟 Deployment 的差別就在於它對對每一個 POD 產生固定的識別資訊，不會因為 POD 重啟而有所變動，所掛載的硬碟也都可以持續使用.

使用場景:
* 需要持久性儲存，不因為 POD 重啟而需要重新設定硬碟給它

### 6. Static pod 


Static Pod 不需依靠 Controller Plane 的物件，所以可以透過 Static Pod創建屬於自己 Node 中的 controller plane物件
ex: Control Plane 中的 controller.yaml, etcd.yaml等等，其實都算是一種 Static Pod

Pod依創建方式可分為兩種
* Static Pod
* kube-apiserver


:::success
kubectl get po -A --> 只要Pod name後面跟著 \<Control Plane-Name\> 的，就都是Static Pod
:::


下面這張圖有4個 static pod
![](https://i.imgur.com/ewdSnzZ.png)



#### Static Pod Path

Static Pod 的創建方式是由 kubelet 定期掃描特定目錄下的 YAML file 來創建

* /var/lib/kubelet/config.yaml
```
下圖紅色圈起來的 /etc/kubernetes/manifests 路徑就是創建 Static Pod 的特定目錄
```
![](https://i.imgur.com/qZ0cpLe.png)



#### Create statis pod

![](https://i.imgur.com/frLT5gU.png)

注意 --command 一定要放在命令最後面
```
kubectl run static-busybox --image=busybox --dry-run=client -o yaml \ 
--command -- sleep 1000 > q1.yaml
mv q1.yaml /etc/kubernetes/manifests/
```

建立好後，把 ymal 放到 Static Pod 的特定目錄, 他就會自動建立好 statis pod

注意 restartPolicy: Never 會自動重啟
```
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: static-busybox
  name: static-busybox
spec:
  containers:
  - command:
    - sleep
    - "1000"
    image: busybox:1.28.4
    name: static-busybox
    resources: {}
  dnsPolicy: ClusterFirst
  restartPolicy: Never
status: {}
```




----


## Logging & Monitoring

[Logging-Monitoring.pdf](https://github.com/oldelette/oldelette.github.io/blob/master/Udemy/Section4-Loggin%20%26%20Monitoring/Kubernetes%2B-CKA-%2B0300%2B-%2BLogging-Monitoring.pdf)


### 1. K8s Monitor Cluster Components

K8s本身就具備一些基本的伺服器監控工具，例如：

* K8s Dashboard：插件工具，展示每個 K8s 集群上的資源利用情況，也是實現資源和環境管理與交互的主要工具。
* Pod liveness probe：Container健康狀態診斷工具。
* Kubelet：每個 Node 上都運行著 Kubelet，監控Container的運行情況。 Kubelet 也是 Control Plane 與各個 Node 通信的渠道。




### 2. Metric Server

[metrics-server Github](https://github.com/kubernetes-sigs/metrics-server)

直接將 Github 上的 open spurce pull 到本地端並創建其中物件
```
git clone https://github.com/kubernetes-sigs/metrics-server.git
or
kubectl apply -f https://github.com/kubernetes-sigs/metrics- \
server/releases/download/v0.3.6/components.yaml
```

該 YAML會於 kube-system namespace創建一個Deployment
![](https://i.imgur.com/dTo7Qx1.png)



```
kubectl top no <node-name>
```

![](https://i.imgur.com/UMx7Pol.png)

Node 中是透過kubelet 來管理 Node 的運作，kubelet需要透過 apiserver 接收Control Plane下達的命令在 Node中 運行 Pod.
而 kubelet 中其實還包含一個 subcomponent，稱為 cAdvisor 或是 Container Advisor 
cAdvisor 負責從 Pod 中擷取performance metrics，並將收集到的數據以metrics-api的形式，透過Summary API expose 給 Metric-Server

![](https://i.imgur.com/lWPWbWR.png)

* Kubernetes log

```
Kubernetes logs -f 
```





## Application Lifecycle Managment

[Application+Lifecycle+Management.pdf](https://github.com/oldelette/oldelette.github.io/blob/master/Udemy/Section5-Application%20Lifecycle%20Management/Kubernetes%2B-CKA-%2B0400%2B-%2BApplication%2BLifecycle%2BManagement.pdf)


### 1. Rolling Update & Rollbacks


Deployment 這個物件裡面會包含了 ReplicaSet，然後再透過 ReplicaSet 來掌管 Pod 運行數Kubernetes 中回滾機制 (Rolling Update / Back)，就是透過 ReplicasSet 來達到

* 每次 Rolling Update (更新)都會產生新的 ReplicaSet 來管控 Pod，可以把它想成產生一個新版本
* Rollbacks (回滾) 則是把現有版本切換到上一個或指定的版本

![](https://i.imgur.com/1AZqz5W.png)


:::success
kubectl rollout status \<deployment\>  --> 查看歷史 rollout 狀態
kubectl rollout history \<deployment\>  --> 查看歷史 rollout 紀錄
kubectl rollout undo \<deployment\>  --> roll back 版本
kubectl rollout pause \<deployment\>  --> 暫停資源
kubectl rollout resume \<deployment\>  --> 恢復暫停之資源
:::


#### Deployment Strategy

K8s 中有兩種版本升級的strategies，一種是recreate，一種為rolling update.
* ++recreate++
直接將所有 Pod一次升級，一次刪除所有舊版Pod，再一次創建所有新版Pod，缺點是更新過程會暫時中斷服務；
* ++rolling update++
將舊版 Pod 一個一個刪除，再一個一個創建新的，可以保證更新期間提供的服務不會中斷

![](https://i.imgur.com/ZIL9m62.png)



#### Rolling Update

有三種方式來進行滾動升級 (以升級 docker image 為例)
++--record 參數:++ 
這參數主要是告知 Kubernetes 紀錄此次下達的指令，能清楚不同的版本(revision) 間做了什麼操作
* set image
```
kubectl set image deployment <deployment> <container>=<image> --record
```
* replace
修改 xxx.yaml 內的 image 版本
```
kubectl replace -f <yaml> --record
```
* edit
```
kubectl edit deployment <deployment> --record
```

---

![](https://i.imgur.com/YaRxY5J.jpg)

### 2. Commands and Arguments


創建 Pod 時，可以為其下的 container 設置啟動時要執行的命令及 帶入參數.
如果要設置命令，就填寫在配置文件的 command 後面，如果要設置命令的帶入參數，就填寫在配置文件的 args 後面.
一但 Pod 創建完成，該命令及其帶入參數 就無法再進行更改了


#### Example 

```
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: test
  name: test
spec:
  containers:
  - image: nginx
    name: test
    resources: {}
    command: ["/bin/sh","-c"]   --> 運行的命令
    args: ["echo '222'>/222.txt;sleep 30"]  --> 命令的參數
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
```
* 登入進去能看到在/ 目錄生成了一個內容是222 之 222.txt file

![](https://i.imgur.com/J3LMGHL.png)




### 3. ConfigMap

ConfigMap 與 Pod 可以單獨存在於 k8s 叢集中，當 Pod 需要使用 ConfigMap 時才需要將 ConfigMap 掛載到 Pod 內使用

:::info
Decoupled (解耦)
* 便於管理
* 高彈性：易於掛載不同的 ConfigMap 到 Pod 內使用
* 一處編輯多處使用：同一個 ConfigMap 可掛載到多個 Pod 使用
:::


k8s 中的 ConfigMap/Secret 可以接受兩種來源:
* --from-literal
```
kubectl create configmap myconfig --from-literal=k1=v1 --from-literal=k2=v2
```
![](https://i.imgur.com/eVf7tI7.png)

* --from-file
```
kubectl create configmap myconfigfromkey --from-file=fromfilekey=.env
```
![](https://i.imgur.com/ReZgUp4.png)

---
* Configmap in Pods 有三種型式

![](https://i.imgur.com/ebjgXcy.png)


#### Configmap in Pods

將 ConfigMap 內的 key 直掛在 Pod 環境變數的例子
```
apiVersion: v1
kind: Pod 
metadata:
  creationTimestamp: null
  labels:
    run: test
  name: test
spec:
  containers:
  - image: nginx
    name: test
    command: ["/bin/sh", "-c", "env"]
    resources: {}
    env:
    - name: MY_CONFIG_KEY  <=== 指定一個新的環境變數名稱為 MY_CONFIG_KEY
      valueFrom:
        configMapKeyRef:  <=== MY_CONFIG_KEY 將會參考 myconfig 內的 k1 值
          name: myconfig  <=== myconfig 是上面宣告的 ConfigMap 物件
          key: k1
  dnsPolicy: ClusterFirst
  restartPolicy: Never
```



---

### 4. Secret


Secrets 是 Kubernetes 提供開發者存放敏感資訊的方式
像是密碼、OAuth tokens 及 ssh keys 等等.. 將這些資訊存在放 Secret 中比直接放在 Pod YAML 或 Image中更加安全和靈活

:::success
Remember that secrets encode data in **base64 format**.
Anyone with the base64 encoded secret can easily decode it.
:::

* ++kubectl apply -f test_secret.yaml++
    * echo -n "xx" | base64
    * echo -n "xx" | base64 --decode
```
# test_secret.yaml
apiVersion: v1
kind: Secret
metadata:
  name: dotfile-secret
data:
  DB_Host: bXlzcWw=
  DB_User: cm9vdA==
  DB_Password: cGFzd29yZA== 
```



#### Secret in Pods -1

下面兩種方式 valueFrom & envFrom

考試的時候, 可以去 [kubernetes.io](https://kubernetes.io/docs/tutorials/kubernetes-basics/) document 搜尋 example
secret --> Using Secrets as environment variables --> [Define container environment variables using Secret data](https://kubernetes.io/docs/tasks/inject-data-application/distribute-credentials-secure/#define-container-environment-variables-using-secret-data)



##### valueFrom

*  ++將Secret作為容器的環境變數++
直接在 Pod 中以 spec.env.valueFrom.secretKeyRef 欄位參考到 Secret 即可

```
sudo kubectl apply -f secret_test.yaml
sudo kubectl exec -it secret-env-pod sh
```

```
apiVersion: v1
kind: Pod 
metadata:
  name: secret-env-pod                                                     
spec:
  containers:
  - name: mycontainer
    image: redis
    env:
      - name: SECRET_USERNAME
        valueFrom:
          secretKeyRef:
            name: test-secret
            key: username
      - name: SECRET_PASSWORD
        valueFrom:
          secretKeyRef:
            name: test-secret
            key: password
  restartPolicy: Never
```

* 創建完成後，進入pod 並echo 即可看到 Secret 的設置結果
![](https://i.imgur.com/8dDcH3e.png)


##### envFrom

* dotfile-secret  為 kubectl secret name
```
apiVersion: v1
kind: Pod 
metadata:
  name: secret-env-pod                                                         
spec:
  containers:
  - name: secret-env-pod
    image: redis
    envFrom:
      - secretRef:
          name: dotfile-secret 
```
![](https://i.imgur.com/dIA1Amm.png)



#####  Secret in Pods - Volume

將 Secret 製作為一個檔案，Pod 以 Volume的形式將此檔案掛載(mount)到容器上

* ++先用 kubectl 創建一個基礎的 yaml++
```
sudo kubectl run nginx --image=nginx --restart=Never \ 
--dry-run=client -o yaml > secret_volume.yaml
```



#### Question

[Ref](https://ithelp.ithome.com.tw/articles/10239675)

![](https://i.imgur.com/fkVjs2S.png)

---

* Answer

1. 創建一個 Secret
2. 創建兩個 Pod (kubectl run 先建立兩個基礎 pod yaml)
3. 其中一個 Podmount 此 Secret，另一個 Pod 將 Secret 作為環境變數使用

```
1. sudo kubectl create secret generic super-secret \ 
--from-literal=credential=alice --from-literal=username=bob
2-1. kubectl run pod-secrets-via-file --image=redis --dry-run=client -o yaml > q5-1-pod.yaml
2-2. kubectl run pod-secrets-via-env --image=redis --dry-run=client -o yaml > q5-2-pod.yaml
3.kubectl apply -f q5-1-pod.yaml q5-2-pod.yaml
```

[q5-1-pod.yaml](https://github.com/oldelette/oldelette.github.io/blob/master/secret/q5.yaml)
[q5-2-pod.yaml](https://github.com/oldelette/oldelette.github.io/blob/master/secret/q5-2.yaml)

![](https://i.imgur.com/kDNbK8K.png)


### 5. Multi-container Pod

大部分的 pod 只運行一個 container, 但在某些情況下
我們必須要在一個 pod 運行兩個以上的 containers, 這種 multi-container 的模式又可以衍伸至三種 design patterns:

1. ++sidecar pattern:++ 最簡單的方式， 兩個container利用volume的方式share同一個檔案目錄
2. ++adapter pattern:++ 利用另外一個 container 做接口， 把要輸出的資料格式化， 例如同一套 log system 便可以處理不同 pods 的 log
3. ++ambassador pattern:++ 假若開發環境是在 local, 此種模式可以將 pod 要輸出的資料直接寫在 local 的資料庫等

![](https://i.imgur.com/lsMnCO9.png)

[邊車模式 (The Sidecar Pattern) - 介紹](https://tachingchen.com/tw/blog/desigining-distributed-systems-the-sidecar-pattern-concept/)


#### example

![](https://i.imgur.com/X6f8ZAU.png)

注意只需要寫一個 container block 即可
```
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: yellow
  name: yellow
spec:
  containers:
  - image: busybox
    name: lemon
    command: ["sleep","1000"]
  - image: redis
    name: gold
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
```



### 6. Init Container


Init Container 和 Pod Container 定義在同一個 Pod YAML 中，通常是用於幫助 Pod Container 運行的前置作業。
像是 Pod Container 需要將執行結果輸出到某一檔案，但該檔案初始並不存在，這時就可以利用Init Container 在 Pod Container 運行前先將檔案創建，以供使用。

:::success
* Init Container 是運行於 Pod Container之前的專用容器.
* Init Conatiner 可以應用於一些不包含 setup environment 的 image.
* Init Container 和 Pod Container 不會同時存在於同一 Pod 中，Pod Container 會等待Init Container 運行到完成狀態後才創建.
:::

Pod 中可以有多個 Pod Container，也可以有一個或多個 Init Container，它們在啟動 Pod Container 之前就已運行。

* 它們的生命週期如下圖
![](https://i.imgur.com/xi8TAa8.png)

[k8s.io -Init containers in use](https://kubernetes.io/docs/concepts/workloads/pods/init-containers/#init-containers-in-use)

```
apiVersion: v1
kind: Pod
metadata:
  name: myapp-pod
  labels:
    app: myapp
spec:
  containers:
  - name: myapp-container
    image: busybox:1.28
    command: ['sh', '-c', 'echo The app is running! && sleep 3600']
  initContainers:
  - name: init-myservice
    image: busybox:1.28
    command: ['sh', '-c', "until nslookup myservice.$(cat /var/run/secrets/kubernetes.io/serviceaccount/namespace).svc.cluster.local; do echo waiting for myservice; sleep 2; done"]
  - name: init-mydb
    image: busybox:1.28
    command: ['sh', '-c', "until nslookup mydb.$(cat /var/run/secrets/kubernetes.io/serviceaccount/namespace).svc.cluster.local; do echo waiting for mydb; sleep 2; done"]
```

---



## Cluster Maintenance

[Kubernetes-CKA-0500-Cluster+Maintenance-v1.2.pdf](https://github.com/oldelette/oldelette.github.io/blob/master/Udemy/Section6-Cluster%20Maintenance/Kubernetes-CKA-0500-Cluster%2BMaintenance-v1.2.pdf)


### 0. Kubernets Software Versions

* 檢查版本
```
kubectl version --client
```
Kubernetes的版本v1.25.2可以分為三個部分：1、25、2，我們分別稱為Major、Minor、Patch。

* Major：主要版本
* Minor：次要，代表特點和功能上的更新
* Patch：補丁，代表修復Bug

![](https://i.imgur.com/S7vTbad.png)



Kubernetes的各個元件，如：kube-apiserver、Controller-manager等，彼此的版本都有相依性，也就是版本不能相差太多



Kubernetes的更新週期是三個月，也就是每三個月會推出新版本

![](https://i.imgur.com/nF8pJbC.png)

---


[kodekloud - Practiec: Cluster Upgrade 升版練習](https://kodekloud.com/topic/practice-test-cluster-upgrade-process-2/)
\<Master Node\>
* 一定要記得 Upgrade kubelet and kubect
* Restart the kubelet (sudo systemctl daemon-reload, sudo systemctl restart kubelet)

\<Worker Node\>
* 一定要記得 ssh 進去 node 裡面做

---


### 1. Cluster Upgrade


[Upgrade Strategy](https://ithelp.ithome.com.tw/articles/10301864)

Cluster Upgrade主要有三種策略，三者的共通點是**必須先更新 Master Node**：
1. ++Strategy-1:++ 一次同時更新全部的 Worker Node。
2. ++Strategy-2:++ 一次更新一個 Worker Node，更新成功後再更新下一個Worker Node，直到全部Worker Node更新完成。
3. ++Strategy-3:++ 一次加入一個新版本的 Worker Node，加入後再剔除一個舊版本的 Worker Node，直到全部 Worker Node更新完成。

3和2的主要差別是加入新的Node來取代舊的Node，3是EKS目前的更新方式。

---

:::success
分成 Upgrading control plane nodes 跟 Upgrade worker nodes 兩部分
:::



兩種 Node 的升級步驟都可以參考 [k8s document](https://kubernetes.io/docs/tasks/administer-cluster/kubeadm/kubeadm-upgrade/)

### 1.1 Upgrading control plane nodes




**++step0:++ Check the current version**

![](https://i.imgur.com/3PX8baW.png)

```
apt update
apt-cache madison kubeadm
# find the latest 1.26 version in the list
# it should look like 1.26.x-00, where x is the latest patch
```


**++step1:++**
檢查可以更新到哪個版本，輸入下面的指令 kubeadm 會告訴你目前可以升級到什麼版本

```
kubeadm upgrade plan
```
目前版本: v1.25.0
遠端版本 (remote version): v1.26.1


![](https://i.imgur.com/3rtBcKq.png)

**++step2:++ upgrage kubeadm version**

replace x in 1.26.0-00 with the latest patch version
```
kubeadm version
apt-get update && apt-get install -y kubeadm=1.26.0-00 && apt-mark hold kubeadm
```
![](https://i.imgur.com/BHg4ATH.png)

```
sudo kubeadm upgrade apply v1.26.0
```

成功把 kubeadm 從 v1.25.0 升級到 v1.26.0
![](https://i.imgur.com/ZHLwXLz.png)


**++step3:++ Upgrade kubelet and kubectl**

```
sudo apt-mark unhold kubelet kubectl && apt-get update && \
apt-get install -y kubelet=1.26.x-00 kubectl=1.26.x-00 && apt-mark hold kubelet kubectl
```

![](https://i.imgur.com/zBVNnBA.png)

**++step4:++ Restart the kubelet:**
重啟kubelet，使其更新生效

```
sudo systemctl daemon-reload
sudo systemctl restart kubelet
```
**++step5:++ Check the upgrade version**

檢查 control plane nodes 版本是否有確實升級

![](https://i.imgur.com/X1QjFOg.png)



**++step6:++ Uncordon the node** 

升級完後, 把 control plane nodes 調回 schedulable

```
# replace <node-to-uncordon> with the name of your node
kubectl uncordon <node-to-uncordon>
```
![](https://i.imgur.com/0E5fLFC.png)

---

### 1.2 Upgrade worker nodes

The upgrade procedure on worker nodes should be executed one node at a time or few nodes at a time, without compromising the minimum required capacity for running your workloads.

**++step0:++ ssh worker node**

```
ssh <woker-node-ip>
```

![](https://i.imgur.com/CrKsiCN.png)


**++step1:++ Upgrade kubeadm**

```
apt-mark unhold kubeadm && apt-get update && \
apt-get install -y kubeadm=1.26.0-00 && apt-mark hold kubeadm
```


**++step2:++kubeadm upgrade**

![](https://i.imgur.com/0lrKS2Y.png)


**++step3:++Upgrade kubelet and kubectl**

```
apt-mark unhold kubelet kubectl && apt-get update && \
apt-get install -y kubelet=1.26.0-00 kubectl=1.26.0-00 && apt-mark hold kubelet kubectl
```
**++step4:++ Restart the kubelet:**
重啟kubelet，使其更新生效

```
sudo systemctl daemon-reload
sudo systemctl restart kubelet
```

**++step5:++ Check the upgrade version**

檢查 worker nodes 版本是否有確實升級

![](https://i.imgur.com/X1QjFOg.png)



**++step6:++ Uncordon the node** 

升級完後, 把 worker nodes 調回 schedulable



![](https://i.imgur.com/8tZnUwS.png)



---

### 2. Node Maintenance

[Node Controller](https://kubernetes.io/docs/concepts/architecture/nodes/#node-controller) 是 Kubernetes 中用來管理 Node 的一個物件


kubectl drain 代表將該 Node 狀態變更為維護模式，如此在該 Node 上面的 Pod，就會轉移到其他 Node 上.
若不是透過 Replication Controller 或是 DaemonSet 等創建好的 Pod，則該指令會失敗，除非加上 --force

* ++從 Kubernetes Cluster 移除 node++ 
```
kubectl drain {node_name}
kubectl drain {node_name} --force

kubectl cordon {node_name}   --> 標記 Node 為不可 Schedule
kubectl uncordon {node_name} --> 恢復 Node
```

如果在 Node 上，有daemonset 類型的pod，不可以被刪除，需要加上下面的參數

* ++--ignore-daemonsets++
```
kubectl drain node01 --ignore-daemonsets
```

![](https://i.imgur.com/JSYb37l.png)

完成後可以看到 node01 的 Status 為 **SchedulingDisabled**
![](https://i.imgur.com/y2Yvtu1.png)

如果想要讓他恢復可被調度，使用 **uncordon** 後可以看到 Status 變回 Ready
![](https://i.imgur.com/Uch5ONU.png)


#### cordon vs drain vs delete

k8s 命令對 node 調度 cordon，drain，delete [區別](https://blog.csdn.net/erhaiou2008/article/details/104986006)

* ++cordon++
影響最小，只會將 node 調整為 **SchedulingDisabled**
之後再創建 pod，不會被調度到該節點
舊有的 pod 不會受到影響，仍正常對外提供服務
    ```
    # 恢復調度
    kubectl uncordon node_name
    ```
* ++drain++
驅逐 node 上的 pod，其他節點重新創建.
將節點調整為 **SchedulingDisabled**
    ```
    # 恢復調度
    kubectl uncordon node_name
    ```
* ++delete++
驅逐 node 上的 pod，其他節點重新創建
然後，從 master節點刪除該 node，master 對其不可見，失去對她的控制，master 不可對他恢復

    ```
    # 恢復調度 需進入 node 節點，重啟 kubelet
    systemctl restart kubelet
    ```





### 3. Backup & Restore Methods

[Ref: ETCD與集群的備份與還原](https://ithelp.ithome.com.tw/articles/10301931)
[Kodekloud: Practice Test - Backup and Restore Methods](https://www.udemy.com/course/certified-kubernetes-administrator-with-practice-tests/learn/lecture/14298862#overview)


Kubernetes Cluster Object 的備份方式有兩種


#### 1. ++儲存 YAML 檔++

備份所有 namespace 中的 deploy, service 狀態
    ```
    sudo kubectl get all -A -o yaml > all-deploy-service.yaml
    ```

#### 2. ++將ETCD Cluster 使用etcdctl備份成快照檔++

若發生問題需要還原時，再將快照檔還原
:::success
\<ETCD Cluster\>
Etcd 是 Kubernetes Cluster 中的一個十分重要的元件，用於保存 Cluster 所有的網路配置和對象的狀態訊息
:::



etcd 的備份有兩種方式：
* ++Built-in snapshot++
etcd 支持內建 snapshot，因此備份 etcd集群很容易。
可以使用etcdctl snapshot save 命令從集群內物件中獲取，也可以從當前未被 etcd process 使用的 etcd 資料目錄中複製 member/snap/db 文件。
* ++Volume snapshot++
如果 etcd 在支持備份的 Volume（例如Amazon Elastic Block Store）上運行，可以通過獲取存 Volume 的 snapshot來備份。


### 3.1 etcdctl command

* **++查看 etcd 的細節++**
```
k describe pod etcd-controlplane -n kube-system
cat /etc/kubernetes/manifests/etcd.yaml
```

因為 Kubernetes 集群使用 https，因此需要指定 --cert-file、--key-file和--ca-file三個參數，參數檔案都位於 /etc/kubernetes/pki/etcd目錄下

:::info
找到 advertise-client-urls, cert-file, key-file 檔案的路徑，作為之後參數使用
{$masterNodeIp} 為自己的 Master Node 的 IP
- --advertise-client-urls=https://{$masterNodeIp}:2379
- --cert-file=/etc/kubernetes/pki/etcd/server.crt
- --key-file=/etc/kubernetes/pki/etcd/server.key
- --trusted-ca-file=/etc/kubernetes/pki/etcd/ca.crt
:::


1. ++listen-client-urls:++ 
指定 etcd server 绑定的本地地址 以接收傳入連線。
要監聽所有的 port，要使用 0.0.0.0 的IP地址。
2. ++advertise-client-urls:++ 
建議用 Client 端的 url ，該值用在 etcd 代理 或 etcd 與node溝通, 不可以使用 localhost 這種，因為這種地址無法從遠端機器進行訪問
3. ++trusted-ca-file:++
ETCD CA Certificate file located
4. ++cert-file:++
ETCD server certificate located
5. ++endpoints:++
不加 --endpoints 參數時，默認訪問的是 https://127.0.0.1:2379

* **++讓 etcdctl command 生效++**
必須先設置環境變 ETCDCTL_API=3 來使用 etcd v3, 若是覺得每次都要先宣告環境變數很麻煩，也可以使用 export 來設定環境變數，那之後的 etcdctl 指令都不需要先設置環境變數告知使用 v3了
```
ETCDCTL_API=3 etcdctl snapshot
export ETCDCTL_API=3
```
![](https://i.imgur.com/To7DiXB.png)


### 3.2 ETCD Store 
使用 etcdctl 保存到 /opt/snapshot-pre-boot.db

```
etcdctl --endpoints 127.0.0.1:2379 --cert=/etc/kubernetes/pki/etcd/server.crt \
--key=/etc/kubernetes/pki/etcd/server.key --cacert=/etc/kubernetes/pki/etcd/ca.crt \
snapshot save /opt/snapshot-pre-boot.db
--> Snapshot saved at /opt/snapshot-pre-boot.db
```


![](https://i.imgur.com/erRNauv.png)

#### 檢查備份檔案

```
etcdctl snapshot status /opt/snapshot-pre-boot.db -w table --endpoints 127.0.0.1:2379 \
--cert=/etc/kubernetes/pki/etcd/server.crt --key=/etc/kubernetes/pki/etcd/server.key \
--cacert=/etc/kubernetes/pki/etcd/ca.crt
```
![](https://i.imgur.com/TJYvRyD.png)


### 3.3 ETCD Restore snapshot

使用保存的 /opt/snapshot-pre-boot.db snapshot 去還原
* ++--data-dir:++ 代表要將 ETCD 還原的位置，這邊選擇 /var/lib/etcd-from-backup
```
etcdctl snapshot restore --data-dir /var/lib/etcd-from-backup /opt/snapshot-pre-boot.db
```

![](https://i.imgur.com/dTW8ntM.png)


修改 /etc/kubernetes/manifests/etcd.yaml 文件內容
把 name: etcd-data 所指定的位置替換成 volume <--data-dir> 的 path
```
# 原本 /etc/kubernetes/manifests/etcd.yaml 文件內容
volumes:
  - hostPath:
      path: /var/lib/etcd
      type: DirectoryOrCreate
    name: etcd-data

# 改為
volumes:
  - hostPath:
      path: /var/lib/etcd-from-backup
      type: DirectoryOrCreate
    name: etcd-data
```



## Security


[Security.pdf](https://github.com/oldelette/oldelette.github.io/blob/master/Udemy/Section7-Security/Kubernetes%2B-CKA-%2B0600%2B-%2BSecurity.pdf)



### 1. Kubernetes API Server Authentication


kube-apiserver 是 kubernetes 的網關性質的元件，是 kubernetes cluster 資源操作的唯一入口
因此像是認證與授權等一些過程很明顯是要基於這個 kube-apiserver 元件

Kubernetes control plane 中包含了 etctd，kube-api-server，kube-scheduler，kube-controller-manager 等元件.
而這些元件之間的相互使用都是通過網路進行的。在進行網路溝通時，溝通雙方需要驗證對方的身份，以避免惡意第三方偽造身份竊取訊息 或者 對系統進行攻擊。
為了相互驗證對方的身份，溝通中的雙方 任何一方都需要做下面兩件事情：

* 1.向對方提供标明自己身份的一個證書
* 2.驗證對方提供的身份證書是否合法，是否偽造的？


三步驟
1. Authentication: 認證解决的問題是辨認用户的身份
2. Authorization: 授權是明確定義 user 具有哪些權限
3. Admission Control: 准入控制是作用於 kubernetes 中的資源對象

![](https://i.imgur.com/8U2SDMN.png)


Kubernetes 中有幾種驗證方式：
* Certificate / Token / OpenID / Web Hook
---
### 1.1 Server 端證書

:::success
kubeadm 安裝的 cluster 中都是用 3套 CA證書來管理 和 簽發其他證書
1. CA 给 ETCD 使用
2. kubernates 内部元件使用
3. 配置 Aggregation Layer 使用的
:::

* kubeadm 創建的 cluster 默認證書存放路徑為 /etc/kubernetes/pki

![](https://i.imgur.com/ATLfHwx.png)


#### ++etcd 證書++

etcd 證書位於 /etc/kubernetes/pki/etcd 目錄下

* etcd 對外提供服務的 server 證書及私鑰
```
server.crt  server.key
```
* etcd node 之間相互進行認證的 peer 證書、私鑰以及驗證 peer 的 CA
```
ca.crt  ca.key peer.crt  peer.key
```
* etcd 驗證訪問其服務的客户端的 CA
```
healthcheck-client.crt  healthcheck-client.key 
```

#### ++kube-apiserver 證書++

apiserver 證書位於 /etc/kubernetes/pki 目錄下

* 訪問 etcd 的客户端證書及私鑰，這個證書是由etcd 的 CA 證書簽發，因此也需要在 apiserver 中配置 etcd 的 CA證書
```
apiserver-etcd-client.key   apiserver-etcd-client.crt   
```

* 根 CA (用來簽發 k8s 中其他證書的 CA證書 及 私鑰)
```
ca.crt  ca.key
```

* apiServer 的對外提供服務的 server 證書及私鑰
```
apiserver.crt   apiserver.key 
```

### 1.2 Client 端證書

Kubernetes 的元件會通過 TLS 雙向確認來驗證客户端的身份。
所有由 cluster CA 簽發的客户端證書都認為是通過認證的（Authentication)，可以正常建立HTTPS 連接，進一步 Kubernetes 會從客户端證書中讀取用户名，用户组訊息，结合 RBAC 等鑑權策略來判斷客户端是 否有對應的操作權限 (Authorization)在客户端證書中

#### ++kubelet client 證書++

![](https://i.imgur.com/EDOVWBD.png)

上圖 kubelet-client-2023-02-24-09-31-47.pem 表示的是 客戶端的證書, 可以查看一下證書內容.
證書之所以带日期是因为 kubelet 的證書快過期時會自動更新，因此带上時間方便區分新舊證書

:::info
0. ++Serial Number++: 0 (0x0) CA機構给該證書的唯一序列號碼，根證書為0 (ca.crt)
1. ++Issuer++: 證書頒發者的相關訊息
2. ++Validity++：證書生效日期 和 失效日期
3. ++Subject++: 證書持有者的相關訊息
O 表示 Organization， CN 表示 Common Name
Common Name 表示用户名，Organization 表示用户组
4. ++Subject Public Key++: server 公開的密鑰
:::

![](https://i.imgur.com/IuyccRl.png)


#### ++kube-apiserver client 證書++

kube-apiserver 使用 kubelet 接口時（執行 exec/logs 命令），kubelet 也會要求檢驗kube-apiserver 的客户端證書
該證書保存路徑為 /etc/kubernetes/pki/apiserver-kubelet-client.crt


---
Test 小測驗:

1. Identify the certificate file used for the ++kube-api server++
--> /etc/kubernetes/pki/apiserver.crt
2. Identify the Certificate file used to authenticate kube-apiserver as ++a client to ETCD Server++
--> /etc/kubernetes/pki/apiserver-etcd-client.crt
3. Identify the key used to authenticate kubeapi-server to the kubelet server
--> /etc/kubernetes/pki/apiserver-kubelet-client.crt


* ++/etc/kubernetes/manifests/kube-apiserver.yaml++

![](https://i.imgur.com/HhqHNNR.png)

4. What is the Common Name (CN) configured on the Kube API Server Certificate?
--> kube-apiserver
```
# use the feature from kubeadm to get the expiration
kubeadm certs check-expiration

# command that would renew the apiserver server certificate
kubeadm certs renew apiserver

# 證書查看 command
openssl x509 -in file-path.crt -text -noout
```
![](https://i.imgur.com/mrutoM1.png)



### 2. Authorization

通過了 Authentication (身份認證)後，那僅能代表當前的使用者允許與 Kubernetes API Server 溝通，至於該使用者是否有權限(Permission)請求什麼資源，就是定義在 Authorization 

Authorization Mode 有以下幾種模式：
* Node
* ABAC (Attribute-based access control)
* RBAC (Role-Base Access Control) -**Default in k8s**
* Webhook


---

* ++Check the kube-apiserver settings:++
查看現有 cluster 的 Authorization 方法
以下圖為例, 紅色底線標記的 Node,RBAC 就是目前 cluster 的 Authorization 方法
```
cat /etc/kubernetes/manifests/kube-apiserver.yaml
```
![](https://i.imgur.com/nC9N5G1.png)




#### 2.1 Node

這是為了授權在每一個 node 上的 kubelet 所發出的 API request 所設計出來的，讓 kubelet 的 API request 可進行特定的權限控制

![](https://i.imgur.com/ejswpYT.png)



#### 2.2 ABAC
++Attribute-based access control++

此種方式就是在 master node 上保留一份 policy 文件，指定不同的使用者對於 resource 的存取權限，不彈性也不容易擴充，修改了 policy 文件之後還需要重新啟動 master node

![](https://i.imgur.com/2v4K66G.png)


#### 2.3 RBAC

++Role-Base Access Control++

RBAC API 中定義了 resource target，用來描述使用者以及 resource 之間的權限關係：
* Role：定義在特定 namespace 下的 resource 的存取權限
* RoleBinding： 設定哪些使用者(or service account)與 role 綁定而擁有存取權限
* ClusterRole：定義在整個 k8s cluster 下的 resource 的存取權限
* ClusterRoleBinding：設定哪些使用者(or service account) 與 role 綁定而擁有存取權限


![](https://i.imgur.com/Jw38R2Y.png)


![](https://i.imgur.com/aB1ufdX.png)



#### 2.4 Webhook


這個模式是管理者在外部提供 HTTPS 授權服務，並設定 API server 透過與外部服務互動的方式進行授權

![](https://i.imgur.com/snqwQKD.png)




### 3. KubeConfig

[ **Context** ](https://www.akiicat.com/2019/04/24/Kubernetes/setup-kubernetes-configuration/)

kubeconfig 是用來記錄 Cluster、User、預設 Namespace 以及 身份驗證機制的資料.
當我們使用 kubectl 這個工具在執行各種指令時，它就會透過 kubeconfig 來得知要到哪個集群，使用哪個使用者等資訊來完成這個命令。
在使用 kubeconfig 時，不需要像之前一樣建立 Object，而是可以直接建立檔案，然後讓kubectl 等工具來讀取這邊的設定


:::success
要存取某個 Kubernetes 的 cluster，必須先設定好 Kubernetes 的 context，context 裡面會描述要如何存取到你的 Kubernetes 的 cluster
在 Kubernetes 裡面，切換不同的 cluster 是以 context 為單位
一個 context 裡面必需要三個元件，分別是 User、Server、Certification.
這三個東西說起來也很直觀，有個使用者 (User) 必須要有憑證 (Certification) 才能連到某個 Cluster (Server)。
:::

* 底下是一個 Context 所包含的內容
![](https://i.imgur.com/2TXQw1X.png)


#### Config yaml
![](https://i.imgur.com/A1B0YdH.png)



設定檔會放置在 ~/.kube/config (mac or linux)，下面內容表示 kubectl 已連接到 kubernetes-admin
* 列出目前 kubectl 的設定內容
```
kubectl config view --> 取得設定檔
```

![](https://i.imgur.com/6NIM1dS.png)

可以分成區塊看:
* ++Clusters++
我們可能會管理到很多集群，像是Production、Stagging、Development之類有著不同功能的集群，在這裡就可以記錄這些集群的資訊。
* ++Users++
我們在各個集群中可能都會擔任不同的使用者，像是我們自己建立的集群，可能自己就是 Admin。
而公司的集群，我們可能就沒有那麼大的權限。這邊就是把各個使用者列出來。
* ++Contexts++
Context 的意思就是上下文，這邊紀錄了會在哪些 Cluster 使用了哪些 User。

---

#### 常用 config 指令


```
kubectl config view

# 如果加上 --kubeconfig，則可以指定要使用哪個 kubeconfig file
kubectl config view --kubeconfig=my-custom-config
```


管理多個 k8s cluster
```
# 改變 current-context 成 [NAME]
kubectl config use-context [NAME]

# 查詢有哪些 cluster 可以切換
kubectl config get-contexts

# 目前正在管理的 cluster
kubectl config current-context

# 取得 cluster 狀態
kubectl cluster-info

# 改變當前預設的 namespace
kubectl config set-context --current --namespace=NAMESPACE
```
* ++kubectl cluster-info++
![](https://i.imgur.com/Hp0ktrj.png)



#### example

![](https://i.imgur.com/tg2aQAe.png)

```
# To use that context, run the command
kubectl config --kubeconfig=/root/my-kube-config use-context research

# To know the current context, run the command
kubectl config --kubeconfig=/root/my-kube-config current-context
```

### 4. Security Context

Pod 的安全策略，雖然 Pod 是受到 kubernetes 經過檢查確認合法才得以部署的，但是由於這些服務都會直接面向 User，若這些容器內本身的權限過高且遭受到攻擊，就會衍生出其他的安全性問題.
SecurityContext 就是用來解決這類問題的，它定義了 Pod 或 容器的特權和訪問控制設置。

:::warning
Capabilities are only supported at the container level and not at the POD level
:::

SecurityContext 包括：
* ++Discretionary Access Control++
訪問目標（如檔案）的權限基於User ID（UID）和 Group ID（GID）。
* ++Security Enhanced Linux (SELinux)++
為目標分配安全標籤
* ++Running as privileged or unprivileged++
以特權或非特權運行
* ++Linux Capabilities++
為某些process提供特權，但不是root的所有特權
* ++AppArmor++
使用程式配置文件來限制個別程式的功能。
* ++Seccomp++
過濾及篩選process的system call
* ++AllowPrivilegeEscalation++
控制process是否可以比其parent process獲得更多的特權。
* ++readOnlyRootFilesystem++
將容器的root file system mount 為 Read-Only

[k8s-io Set the security context for a Pod](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/#set-the-security-context-for-a-pod)
[k8s-io Set capabilities for a Container ](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/#set-capabilities-for-a-container)
```
apiVersion: v1
kind: Pod
metadata:
  name: security-context-demo
spec:
  securityContext:
    runAsUser: 1000
    runAsGroup: 3000
    fsGroup: 2000
```

* ++runAsUser: 1000++
對於 Pod 中的任何容器，所有 process 都已 User ID=1000運行
* ++runAsGroup: 3000++
Pod 的任何容器內的所有 process 指定 primary group ID 為3000。
指定 runAsGroup 時， User ID 1000 和 Group ID 3000 也將擁有所有創建的檔案。
(若省略此參數，K8s default的primary group ID是root(0))
* ++fsGroup: 2000++
容器的所有 process 也是 supplementary Group ID 2000 的一部分。
Volume /data/demo 及該 Vloume 中所有檔案的擁有者均為 Group ID 2000。

#### Example

Create a new Pod called super-user-pod with image busybox:1.28. Allow the pod to be able to set system_time
The container should sleep for 4800 seconds

```
apiVersion: v1
kind: Pod
metadata: 
    name: super-user-pod
spec: 
    containers:
    - image: busybox:1.28
      name: super-user-pod
      ## 加上securityContext參數
      securityContext:
        capabilities:
          add: ["SYS_TIME"] ## 允許設定SYS_TIME
      command: ["sleep"] ## container sleep 4800
      args: ["4800"]
    restartPolicy: Never
```


### 5. Role


Role 即代表了 namespace 下的資源的權限，ClusterRole 則是 Cluster 層級資源的權限
[Role example](https://kubernetes.io/docs/reference/access-authn-authz/rbac/#role-example)
```
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  namespace: default
  name: pod-reader
rules:
- apiGroups: [""] # "" indicates the core API group
  resources: ["pods"]
  verbs: ["get", "watch", "list"]
```
rules包含三個子欄位
* ++apiGroups++
要使用的 API Group name。若此欄空白，則預設為 core API
* ++resources++
要對甚麼 resource 進行設定, 比如 pods、deployments、services、secrets 等等..
* ++verb++
允許對resources進行的操作, 比如 get、list、watch、create、delete、update 等等..

```
rules可以一次設定好幾個，以 YAML 陣列區隔即可
```

![](https://i.imgur.com/H3UpDu8.png)


### 5.1 Binding

就是把定義好規則權限的 Role 绑定到指定的 Subject 上，進而賦予 Subject 相對應的一系列權限

kubernetes 中 Binding 分為兩種：
1. ++RoleBinding++：绑定 Role/ClusterRole 到 Subject，生效在具體的 Namespace 範圍資源
2. ++ClusterRoleBinding++ 绑定 ClusterRole 到 Subject，生效在 Cluster 範圍資源

:::success
Kubernetes 中權限控制的範圍（Namespace/Cluster）是由 Binding 的類型决定的，而不是根據 Role 和 ClusterRole 决定的.
:::


#### Role + RoleBinding 的组合

這種方式是比較直觀的，Kubernetes 中的 Role 應該理解為 Namespace Role，它是定義在 Namespace 下面的，因此如果要實現 Namespace 範圍内的 Subject 與 Role 的绑定，無非就是通過配置一個 RoleBinding 把兩者聯繫起來.

![](https://i.imgur.com/pJLWUTc.png)

* 配置範例
```
kind: RoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: pods-reader-binding
  namespace: ns-a
subjects:
- kind: User
  name: mcleezs
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: Role
  name: pod-reader
  apiGroup: rbac.authorization.k8s.io
```


#### ClusterRole + RoleBinding 的组合

ClusterRole，它的本質是一個定義在全局的角色，可以作用在 Namespace 下，可以被多個 Namespace 重複使用，也可以作用在 Cluster 下。

我們現在需要實現 Namespace 範圍内绑定 Subject & Role，就是將 ClusterRole 的權限範圍控制在 Namespace 層面，因此採用 RoleBinding 將 Subject 與 ClusterRole 即可實現

![](https://i.imgur.com/pAfEvBN.png)


* 配置範例
```
kind: RoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: pods-reader-binding
  namespace: ns-a
subjects:
- kind: User
  name: mcleezs
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: ClusterRole
  name: pod-reader
  apiGroup: rbac.authorization.k8s.io
```

---


#### Example

kube-proxy 這個 role 可以看到 configmaps 這個 object by the name [kube-proxy]

![](https://i.imgur.com/YpH1ef5.png)

想確認你可以對特定物件做甚麼操作，可以透過以下指令
```
kubectl auth can-i list pod -A 
kubectl auth can-i delete pod
# 若你是管理者，你想檢查 dev-user 可否對特定物件做甚麼操作
kubectl auth can-i  list pod --as dev-user 
```
![](https://i.imgur.com/I8kANen.png)


#### Question 1

接續上面 example 內容, 因為 dev-user 無法 list / delete pods, 所以這題要求創建必要的 role 根 role binding 使得 dev-user 能夠 list / delete pods
![](https://i.imgur.com/TbPHWvh.png)

```
k create role developer --verb=list,create,delete --resource=pods
k describe role developer
k create rolebinding dev-user-binding --role=developer --user=dev-user
k describe rolebindings dev-user-binding
```

![](https://i.imgur.com/dNLmr6D.png)


#### Question 2

需要增加角色權限 (原本為 Forbidden)

![](https://i.imgur.com/hbA34WB.png)


我們用 describe 去看，發現 developer role 可以看得 Resource Names 是 blue-app 而非題目想要我們看的資源 dark-blue-app, 所以這就是問題所在

![](https://i.imgur.com/AZpdJkL.png)

```
k edit role developer -n blue
k --as dev-user get pods dark-blue-app -n blue
```
edit 進去把 rules 底下的 resourceNames 改成 dark-blue-app 即可
![](https://i.imgur.com/IyV9sEG.png)

#### Question 3

Add a new rule in the existing role developer to grant the dev-user permissions to create deployments in the blue namespace.

dev-user 沒有權限在 namespace=blue 底下 create deployment, 所以這題要求改變成能夠 create deployment.

```
k create deployment nginx --image=nginx -n blue --as dev-user
--> error: failed to create deployment: deployments.apps is forbidden: User "dev-user" 
cannot create resource "deployments" in API group "apps" in the namespace "blue"
k edit role developer -n blue
```

增加紅圈處的部分去給 deployment.apps 權限

![](https://i.imgur.com/0dYCmdO.png)


### 5.2 CertificateSigningRequest

[k8s-io 官網範例](https://kubernetes.io/docs/reference/access-authn-authz/certificate-signing-requests/#create-certificatesigningrequest)
```
cat <<EOF | kubectl apply -f -
apiVersion: certificates.k8s.io/v1
kind: CertificateSigningRequest
metadata:
  name: myuser
spec:
  request: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURSBSRVFVRVNULS0tLS0KTUlJQ1ZqQ0NBVDRDQVFBd0VURVBNQTBHQTFVRUF3d0dZVzVuWld4aE1JSUJJakFOQmdrcWhraUc5dzBCQVFFRgpBQU9DQVE4QU1JSUJDZ0tDQVFFQTByczhJTHRHdTYxakx2dHhWTTJSVlRWMDNHWlJTWWw0dWluVWo4RElaWjBOCnR2MUZtRVFSd3VoaUZsOFEzcWl0Qm0wMUFSMkNJVXBGd2ZzSjZ4MXF3ckJzVkhZbGlBNVhwRVpZM3ExcGswSDQKM3Z3aGJlK1o2MVNrVHF5SVBYUUwrTWM5T1Nsbm0xb0R2N0NtSkZNMUlMRVI3QTVGZnZKOEdFRjJ6dHBoaUlFMwpub1dtdHNZb3JuT2wzc2lHQ2ZGZzR4Zmd4eW8ybmlneFNVekl1bXNnVm9PM2ttT0x1RVF6cXpkakJ3TFJXbWlECklmMXBMWnoyalVnald4UkhCM1gyWnVVV1d1T09PZnpXM01LaE8ybHEvZi9DdS8wYk83c0x0MCt3U2ZMSU91TFcKcW90blZtRmxMMytqTy82WDNDKzBERHk5aUtwbXJjVDBnWGZLemE1dHJRSURBUUFCb0FBd0RRWUpLb1pJaHZjTgpBUUVMQlFBRGdnRUJBR05WdmVIOGR4ZzNvK21VeVRkbmFjVmQ1N24zSkExdnZEU1JWREkyQTZ1eXN3ZFp1L1BVCkkwZXpZWFV0RVNnSk1IRmQycVVNMjNuNVJsSXJ3R0xuUXFISUh5VStWWHhsdnZsRnpNOVpEWllSTmU3QlJvYXgKQVlEdUI5STZXT3FYbkFvczFqRmxNUG5NbFpqdU5kSGxpT1BjTU1oNndLaTZzZFhpVStHYTJ2RUVLY01jSVUyRgpvU2djUWdMYTk0aEpacGk3ZnNMdm1OQUxoT045UHdNMGM1dVJVejV4T0dGMUtCbWRSeEgvbUNOS2JKYjFRQm1HCkkwYitEUEdaTktXTU0xMzhIQXdoV0tkNjVoVHdYOWl4V3ZHMkh4TG1WQzg0L1BHT0tWQW9FNkpsYWFHdTlQVmkKdjlOSjVaZlZrcXdCd0hKbzZXdk9xVlA3SVFjZmg3d0drWm89Ci0tLS0tRU5EIENFUlRJRklDQVRFIFJFUVVFU1QtLS0tLQo=
  signerName: kubernetes.io/kube-apiserver-client
  expirationSeconds: 86400  # one day
  usages:
  - client auth
EOF
```

---

### 6. Service Account

Kubernetes 的帳號有兩種類型，分別為：

* ++使用者帳戶 (Normal Users)++
任何人想要連接並存取 Kubernetes 叢集，都需要先建立一個 "使用者帳戶" 並將憑證資訊提供給用戶端 (如: kubectl)，以便通過 Kubernetes 的 API server 的認證 (Authentication)
這個名字應該稱為 User Accounts 會比較好理解，但是 Kubernetes 官網稱一般使用者 (Normal Users)

* ++服務帳戶 (Service Accounts)++
任何跑在 Pod 裡面的 container(容器) 想要存取 Kubernetes 的 API 伺服器 (kube-apiserver)，就需要先有一個 "服務帳戶" 綁定在 Pod 身上，然後以便通過 Kubernetes 的 API 伺服器的身份認證 (Authentication)


```
kubectl create serviceaccount dashboard-sa
kubectl get serviceaccount
kubectl describe serviceaccount dashboard-sa
```
#### Pod & ServiceAccount by command

v1.24開始，建立ServiceAccount 不會自動建立 token，所以我們需要自己建立 token 並會在約1小時後到期失效

```
kubectl create token dashboard-sa
```

![](https://i.imgur.com/rm1iI8R.png)


#### Pod & ServiceAccount by yaml

當創建 pod 的時候，如果没有指定一個 service account，系统會自動的在與該 pod 相同的 namespace 下幫其指派一個 default service account
如果 describe pod 的原始 json 或 yaml 訊息（例如使用 kubectl get pods/podename -o yaml 命令），我們可以看到 spec.serviceAccountName 已经被設置為 automatically
```
k get pods <pod-name> -o yaml
```
![](https://i.imgur.com/h2LEhMA.png)

這樣就可以 在 pod 中使用自動掛載的 service account 憑證來訪問 API.

* You can opt out of automounting API credentials on ++/var/run/secrets/kubernetes.io/serviceaccount/token++ for a service account by setting automountServiceAccountToken: false on the ServiceAccount

選擇取消 service account 自訂掛載 API 憑證，只需在 service account 中設置 [automountServiceAccountToken: false](https://kubernetes.io/docs/tasks/configure-pod-container/configure-service-account/)
```
apiVersion: v1
kind: ServiceAccount
metadata:
  name: build-robot
automountServiceAccountToken: false
...
```

* You can also opt out of automounting API credentials for a particular Pod:
```
apiVersion: v1
kind: Pod
metadata:
  name: my-pod
spec:
  serviceAccountName: build-robot
  automountServiceAccountToken: false
  ...
```

#### Question 1 - deployment edit serviceaccount

[kodekloud: practice-test-service-account](https://kodekloud.com/topic/practice-test-service-accounts-2/)

However currently, the default service account is mounted. 
Update the deployment to use the newly created ServiceAccount

```
k edit deployments.apps web-dashboard
```

![](https://i.imgur.com/Z1yqKW8.png)





### 7. Image Sercurity

如果今天要從本地端去抓取一個 private container registry，我們第一件要做的事情就是 docekr login.
對於 Kubernetes 來說，其會使用 secret 的特殊型態 docker-registry 作為登入任何 private container registry 的帳號密碼來源.

++這邊有兩種方式可以使用++
* 第一種是先透過 docker login 登入，之後將登入後的設定檔案送給 Kubernetes secret 物件
* 第二種則是創建 Kubernetes secret 時使用明碼的帳號密碼

```
kubectl create secret
k create secret docker-registry --> 從 private harbor 拉 image
```
![](https://i.imgur.com/3PkNc8g.png)

#### Commandline create secret object

![](https://i.imgur.com/lB2YhtN.png)

```
kubectl create secret docker-registry -h

k create secret docker-registry private-reg-cred \ 
--docker-server=myprivateregistry.com:5000 --docker-username=dock_user \ 
--docker-password=dock_password --docker-email=dock_user@myprivateregistry.com
```
![](https://i.imgur.com/nI0u0P8.png)

#### Create a Pod that uses your Secret
[k8s.io](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/#create-a-pod-that-uses-your-secret)
```
apiVersion: v1
kind: Pod
metadata:
  name: private-reg
spec:
  containers:
  - name: private-reg-container
    image: <your-private-image>
  imagePullSecrets:
  - name: regcred
```



### 8. Network

網路策略(Network Policy) 是用在控制Pod之間如何溝通以及如何與其他網路溝通的規範, 目的在幫K8s 實現更精細的流量控制以及租戶隔離機制.
K8s 提供一個 NetworkPolicy 供使用, 有效範圍是整個Namespace.

NetworkPolicy 會使用標籤選擇器定義一組Pod作為控制對象, 管控入站流量的是Ingress, 負責出站流量的是Egress, 兩個可以共用, 負責規範生效範圍的是 spec.policyType

Pod 預設可以接收任何來源的流量, 也可以向外部發出期望的所有流量, 一旦Nameapace 中有NetworkPolicy規範Pod, Pod就會依照NetworkPolicy的規範拒絕請求, 並且若是在spec中定義了沒有規則的Ingress或Egress就會造成拒絕相關的一切流量。

![](https://i.imgur.com/vpBuGpN.png)

---

#### Ingress 入站流量管控

:::warning
++Ingress 字段是一個列表, 主要有兩個字段組成:++
1. from <[]Object> : 可訪問的Pod列表, 如果設定多項則判斷邏輯為"聯集", 是一個白名單的概念.
2. ports <[]Object>: 可訪問的Pod上面的允許Port列表, 有設定就就是白名單.
:::

![](https://i.imgur.com/Y6aK1A2.png)

* ++拒絕所有流量配置 YAML++
podSelector : pod 標籤選擇器, 給空值代表全選
port : 未定義時匹配所有端口
```
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: deny-all-ingress
spec:
  podSelector:{}
  policyType:["Ingress"]
```

* ++接收所有流量 YAML++
```
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: allow-all-ingress
spec:
  podSelector:{}
  policyType:["Ingress"]
  ingress:
  - {}
```

#### Egress 出站流量管控

![](https://i.imgur.com/C59SpVx.png)
:::warning
++Egress 字段是一個列表, 主要有兩個字段組成:++
1. to <\[\]Object>: 可訪問的Pod列表, 如果設定多項則判斷邏輯為 "聯集", 是一個白名單的概念.
2. ports <\[\]Object> : 可訪問的Pod上面的允許Port列表, 有設定就就是白名單.
:::


* ++拒絕所有流量配置 YAML++
```
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: deny-all-egress
spec:
  podSelector:{}
  policyType:["Egress"]
```
* ++接收所有流量 YAML配置++
```
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: allow-all-egress
spec:
  podSelector:{}
  policyType:["Egress"]
  egress:
  - {}
```


### 9. Network Policies


```
k get networkpolicie
k get netpol
k describe netpol <networkpolicie-name>
```


![](https://i.imgur.com/6V34uN0.png)

上面這個設定代表只允許 pod=interval 透過 8080 port 訪問 pod=payroll

![](https://i.imgur.com/aM6eZdQ.png)



#### ++Create a network policy++

![](https://i.imgur.com/wzsw9T6.png)

```
k create -f internal.yaml
```
![](https://i.imgur.com/Xkt1qWu.png)


```
# internal.yaml
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: internal-policy
  namespace: default
spec:
  podSelector:
    matchLabels:
      name: internal
  policyTypes:
    - Egress
  egress:
    - to:
        - podSelector:
            matchLabels:
              role: payroll
      ports:
        - protocol: TCP
          port: 8080
    - to:
        - podSelector:
            matchLabels:
              role: mysql
      ports:
        - protocol: TCP
          port: 3306
```


## Storage

[Kubernetes-Storage.pdf](https://github.com/oldelette/oldelette.github.io/blob/master/Udemy/Section8-Storage/Kubernetes%2B-CKA-%2B0700%2B-%2BStorage.pdf)

要將容器中的檔案留存在主機上，Docker 有三種作法：
1. ++volume++
Volume 存放在主機檔案系統中由 Docker 管理的地方，在 Linux 作業系統是 /var/lib/docker/volumes/ 此路徑。非 Docker 的行程不應該修改檔案系統中的這一部分.
要在 Docker 中留存資料，volumes 是最好的方法
2. ++bind mount++
可存放在主機檔案系統中的任何地方，非 Docker 行程或 Docker 容器可隨時修改。
3. ++tmpfs mount++ (only 在 Linux 作業系統上的 Docker)
只存放在主機的記憶體中，不會寫入主機的檔案系統

* 這三種方式的差異可用下圖表示

![](https://i.imgur.com/JLhgr7g.png)

---
* ++Volume driver & Storage driver++

![](https://i.imgur.com/VJ6Uhts.png)



### 1. Volume

:::success
Volume 可以是一個 Node 或是 一個雲端儲存平台，是 K8s 實現儲存資料的方.
透過將 Volume mount 到Pod上，就可以實現儲存Pod的資料
:::

### 1.1 Volume Type

multinode cluster 不建議使用 hostPath, 因為每個 node 中的路徑不一定會相同
![](https://i.imgur.com/sMyQSFE.png)

K8S 提供的 Vloume類型非常多，下面舉兩個為例子:

#### ++1. emptyDir++
當 Pod 調度某給Node時，首先創建 emptyDir Volume，並且該Pod在該Node上運行時就存在。
顧名思義，它最初是空的。Pod中的容器都可以在emptyDir Volume中讀取和寫入相同的文件，儘管該Volume可以安裝在每個 Container 中的相同或不同路徑上。
當 Pod 從 Node 中被刪除時，emptyDir 中的數據也將被刪除。
emptyDir 的用途包含：
1. 臨時空間，例如用於某些應用程式執行階段所需的臨時目錄，且無須永久保存
2. 長時間工作的中間過程 CheckPoint 的臨時儲存目錄
3. 多容器共用目錄

```
apiVersion: v1
kind: Pod
metadata:
  name: test-pd
spec:
  containers:
  - image: k8s.gcr.io/test-webserver
    name: test-container
    volumeMounts:
    - mountPath: /cache
      name: cache-volume
  volumes:
  - name: cache-volume
    emptyDir: {}
```

#### ++2. hostPath++

hostPath Volume 將 Node檔案系統中的檔案或目錄 mount到 Pod 中。
hostPath 的生命週期與 Node 相同，並不會隨著 Pod消失而消失。
hostPath的用途包含：
1. 容器應用程式產生的紀錄檔需要永久儲存時，可以使用Node的檔案系統進行儲存
2. 需要存取Node上Docker內部資料結構的容器應用時，可以透過定義 hostPath 為 Node 的 /var/lib/docker 目錄，使容器可以直接存取 Docker的檔案系統

```
apiVersion: v1
kind: Pod
metadata:
  name: test-pd
spec:
  containers:
  - image: k8s.gcr.io/test-webserver
    name: test-container
    volumeMounts:
    - mountPath: /tmp
      name: test-volume
  volumes:
  - name: test-volume
    hostPath:
      # directory location on host
      path: /data
      # this field is optional
      type: Directory
```

:::success
帶來的問題：發布環境侷限不能進行遷移 → 要遷移就要修改文件
:::


### 2. PersistentVolume(PV) & Persistent Volume Claim (PVC)

PersistentVolume 是存放資源的地方,簡單想像的話就是個 Disk 空間,PersistentVolume 分為兩種:
1. 靜態：手動建立 PersistentVolume 稱為「靜態」綁定
2. 動態：PVC在批配 PV時，若不符合規則會透過 StorageClass 自動建立新的PV，此時PV我們會稱為「動態」綁定，並且繼承 StorageClass 規定的回收政策


![](https://i.imgur.com/0dtZy2s.png)

[解決](https://medium.com/k8s%E7%AD%86%E8%A8%98/kubernetes-k8s-pv-pvc-%E5%84%B2%E5%AD%98%E5%A4%A7%E5%B0%8F%E4%BA%8B%E4%BA%A4%E7%B5%A6pv-pvc%E7%AE%A1%E7%90%86-4d412b8bafb5) Pod 與具體存儲 Volume 耦合 → 引入了 PV/ PVC 進行解藕

---
```
kubectl get persistentvolumeclaim
kubectl delete persistentvolumeclaim <my-claim> --> 刪除 persistentvolumeclaim
```
![](https://i.imgur.com/rfXbxhu.png)


---

[PersistentVolumeClaim](https://kubernetes.io/docs/concepts/storage/persistent-volumes/#persistentvolumeclaims) 可以透過我們設定好的 Storage Class 的模板，創建出我們所需要的資源.

:::success
每個 PV 都有自己的一組 accessModes (訪問模式)，用於描述該特定 PV 的功能.
分為三種:
1. ReadWriteOnce：只可以掛載在同一個 Node 上提供讀寫功能.
2. ReadOnlyMany ：可以在多個 Node 上提供讀取功能.
3. ReadWriteMany：可以在多個 Node 上提供讀寫功能.
:::

當 PV 和 PVC 創建完成後，K8s 會根據 PVC 的 request 和 PV 的 properties，將最合適的 PV 及 PVC bind 起來，**每個 PVC 只會 bound 一個 PV**
在 binding 過程中，K8s 會為 PVC 挑選最合適的 PV，除了容量外，也會考量 accessModes、vloumeModes 和 storage class 等等..

* PVC 該如何與 PV 進行綁定:
    *  透過 Label 標籤，找到相同 PV. (客製 binding 到特別的 PV)
    * 透過 storageClassName 名稱，找到相同 PV.

#### Create Persistent Volumes

先從官網拉基本 sample 下來再去改 [k8s io](https://kubernetes.io/docs/concepts/storage/persistent-volumes/#persistent-volumes)

* ++capacity++: 定義這個 PV 的 storage 大小

```
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv0003
spec:
  capacity:
    storage: 5Gi
  volumeMode: Filesystem
  accessModes:
    - ReadWriteOnce
  persistentVolumeReclaimPolicy: Recycle
```
:::info
PV 如果處於 Available Status，代表目前沒有 PVC 存在，或是沒有合適的 PVC 可以 bind 此 PV，那我們就需要自己建一個
:::

左邊是題目, 右邊是答案
![](https://i.imgur.com/L9kkLox.png)


#### Create Persistent Volumes Claim

先從官網拉基本 sample 下來再去改 [k8s io](https://kubernetes.io/docs/concepts/storage/persistent-volumes/#persistentvolumeclaims)
```
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: myclaim
spec:
  accessModes:
    - ReadWriteOnce
  volumeMode: Filesystem
  resources:
    requests:
      storage: 8Gi
```

:::info
當創建 PVC 後，可能會看見它處於 Pending Status，這是因為沒有找到合適配對的 PV.
如果有配對到合適的 PV 後，K8s 會自動將兩者 binding 起來，就可以看見 PVC 是 Bound Status.
:::

![](https://i.imgur.com/H3jaJHB.png)


### 3. Storage Class

透過 Kubernetes 提供的 [Storage Class](https://kubernetes.io/docs/concepts/storage/storage-classes/) 元件，我們可以依據需求，根據 Volumes 的提供者 (provisioner)、類型 (type)、所在地 (Region)，以及回收政策 (reclaimPolicy) 去定義不同的 Storage Class.

![](https://i.imgur.com/DtbBII2.png)

當用戶在建立 Pod 服務使用到 PVC (PersistentVolumeClaim)，這時會自動找一個符合的 PV (PersistentVolume)進行批配，
若有批配到就直接進行綁定 (此時表示與 PV 進行「靜態」批配)
但是如果沒有符合的 PV，則會透過 StorageClass 建立一個新的 PV 再和 PVC 綁定. (此時表示與PV進行「動態」批配)

系統管理人員負責建置 PV ，而開發人員則是負責建立 PVC 與 Storage Class，並交由 PVC 自動尋找合適的 PV進行綁定，或者透過StorageClass建立一個新的PV再和PVC綁定


![](https://i.imgur.com/JCDMq5R.jpg)


```
kubectl get sc --> 看 Storage Class
```


#### Create Storage Class

先從官網拉基本 sample 下來再去改 [k8s io](https://kubernetes.io/docs/concepts/storage/storage-classes/#the-storageclass-resource)

```
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: standard
provisioner: kubernetes.io/aws-ebs
```

![](https://i.imgur.com/zyHx54p.png)


## Troubleshooting

[Troubleshooting.pdf](https://github.com/oldelette/oldelette.github.io/blob/master/Udemy/Section13-Troubleshooting/Kubernetes-CKA-1000-Troubleshooting.pdf)

這類考題通常會提供一個 K8s 集群環境，在環境中有一些 bug，例如 Pod 無法新增、kube-apiserver 故障或路徑問題等等，要求你解決這些問題.

[Ref](https://ithelp.ithome.com.tw/articles/10246043)
++**Trobleshooting方法**++
為了追蹤和找出 K8s 集群中執行的容器應用出現的問題，經常透過以下這些查錯方法：

1. 檢視 K8s 目前執行的階段資訊，特別是與物件連接的Event事件.
這些事件紀錄了相關主題、發生時間、最近發生時間、發生次數及事件原因等，對於Trobleshooting很有幫助.
此外，透過檢視物件的執行階段資料，我們還可以發現參數錯誤、連結錯誤、狀態例外等問題.
2. 對於服務、容器方面的問題，可能需要深入容器內部進行診斷，此時可以透過檢視容器的執行記錄檔來找出問題
3. 對於某些複雜問題，例如 Pod 調度排程這類的問題，涉及到整個集群的節點，因此可能需要查找節點內的服務紀錄檔來debug.
例如蒐集 Control Plane 上的kube-apiserver、kube-schedule、kube-controler-manager，Node 上的 kubelet 及 kube-proxy 等等物件的log.


### 常見問題

* ++Control Plane Failure++
這類問題包含 Pod 無法調度、認證沒通過，找不到目標檔案等等。建議解決方法可透過：
檢查 kube-system的物件
檢查 static pod路徑
要如何確認 Pod 運行在哪個Node上，可以透過kubectl get po -o wide命令，或看 Pod 名子，名子後面有接 master 的，通常代表運行在Control Plane上，可從Control Plane下手

* ++Pod 處於 Pending STATUS++
可能是 image不存在，檢查 image 的 Server (如Docker Hub)是否正常運行，或是網路狀況等等
* ++Service++
通常是 Service 的 Port mapping沒設定好，或是 Service 沒選取到Pod，可以檢查 Pod 的 label 和 Service 的 Selector
* ++NetworkPolicy++
檢查 NetworkPolicy 的 ingress和egress IP是否設定正確

### Application Failure


* ++Check ControlPlane Service++
```
service kube-apiserver status
service kube-controller-manager status
service kube-scheduler status
service kubelet status
service kube-proxy status
```

* ++Check Service Logs++
```
kubectl logs kube-apiserver-master -n kube-system
sudo journalctl -u kube-apiserver
```



#### Task 1 -

應用程式 port 可以通，但是 mysql-service 跟 webapp-mysql 連接上可能有些問題

![](https://i.imgur.com/QUVmZ0c.png)

```
kubectl config set-context --current --namespace=alpha
curl http://localhost:30081
```
curl 打 30081 port 從 error message 出發

![](https://i.imgur.com/Au9yoR1.png)

```
k describe deployments
k get svc
k edit svc mysql
k delete svc mysql
k create -f xxxx.yaml
```

發現 deployment 的 webapp-mysql container 跟 k8s 上的 service name 對不起來，那這就是錯誤所在, edit svc 內容的名稱後重啟即可

![](https://i.imgur.com/FysbTRk.png)


### Control Plane Failure

#### Task 1 - deploy check

---
The cluster is broken. We tried deploying an application but it's not working. Troubleshoot and fix the issue.
Start looking at the deployments.

---

![](https://i.imgur.com/XRRgIoM.png)

檢查 pod 的時候，發現 kube-system 底下有個 kube-scheduler-controlplane 的 pod 壞掉, 所以再深入去看他就發現是 kubelet exec command Error.

```
k describe pod kube-scheduler-controlplane -n kube-system
```
![](https://i.imgur.com/kNIwlMh.png)

因為 kube-scheduler 是個 static pod, 所以要去 /etc/kubernetes/manifests 路徑去搜尋他的定義文件並且修改.


#### Task 2 - Scale deployment

---
Q1: Scale the deployment app to 2 pods.

Q2: Even though the deployment was scaled to 2, the number of PODs does not seem to increase. Investigate and fix the issue.
Inspect the component responsible for managing deployments and replicasets.

---
參考 [k8s.io-Scaling a Deployment](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/#scaling-a-deployment)
```
k scale deployment app --replicas=2
```

開始檢查為什麼 pods 數量沒有跟隨 deployment scale 2 而變成兩個, 發現 kube-system 底下有 pod CrashLoopBackOff.
但是這次 describe 沒有發現甚麼有用的訊息，所以改用 logs 去看, 就發現 error message 了.
```
k get pods -A
k describe pod kube-controller-manager-controlplane -n kube-system
k logs kube-controller-manager-controlplane -n kube-system
```

![](https://i.imgur.com/3HSnFK6.png)

因為 kube-controller-manager-controlplane 是個 static pod, 所以要去 /etc/kubernetes/manifests 路徑去搜尋他的定義文件並且修改.
```
vim /etc/kubernetes/manifests/kube-controller-manager.yaml
k get pods -n kube-system --watch
k get deployments -n kube-system
```

#### Task 3 - Scale deployment_2

---
Something is wrong with scaling again. We just tried scaling the deployment to 3 replicas. But it's not happening.
Investigate and fix the issue.

---

```
k get pods -A
k logs kube-controller-manager-controlplane -n kube-system
```
![](https://i.imgur.com/NsXi41D.png)
這次觀察出是憑證問題, error log 說 "/etc/kubernetes/pki/ca.crt: no such file or directory", 但是實際去看是有的.


```
vim /etc/kubernetes/manifests/kube-controller-manager.yaml
```
看證書的 volumeMounts 名稱為 k8s-certs, 再往下找到 error (紅色箭頭處)
![](https://i.imgur.com/iqpvsZ1.png)



### Worker Node Failure




## Others


### Deplyment 

[Ref1 從題目中學習k8s](https://ithelp.ithome.com.tw/articles/10237456)
[Ref2 --record has been deprecated, then what is the alternative](https://stackoverflow.com/questions/73814500/record-has-been-deprecated-then-what-is-the-alternative)

![](https://i.imgur.com/JnZHgeD.png)


---

* Answer
```
sudo kubectl create deploy nginx-app --image=nginx:1.11.0-alpine --replicas 3
sudo kubectl set image deployment nginx-app nginx=nginx:1.11.3-alpine --> 升版
sudo kubectl annotate deployment nginx-app kubernetes.io/change-cause="version change to 1.11.0 to 1.11.3" --overwrite=true
sudo kubectl rollout history deployment nginx-app
sudo kubectl rollout undo deployment nginx-app --> 退版
```
![](https://i.imgur.com/leII1mB.png)



考點是 rolling update 和 rollout，也就是K8s中重要的版本控制、版本升級與版本回滾 (版本是指 image 的版本)

在K8s中有兩種版本升級的 strategies
* ++recreate++
recreate方法很直覺，就是直接將所有Pod一次升級，一次刪除所有舊版Pod，再一次創建所有新版Pod，缺點是更新過程會暫時中斷服務
* ++rolling update++
rolling update則是將舊版Pod一個一個刪除，再一個一個創建新的，可以保證更新期間提供的服務不會中斷。
:::success
創建 Deployment 時不必特別指定 update strategy type
K8s中 default的策略就是 rolling update (可以用 kubectl describe deploy 命令查看)
:::

### 22. Volume-Test

[Ref](https://ithelp.ithome.com.tw/articles/10241004)
![](https://i.imgur.com/1co2rP7.png)



---

* Answer

1.創建一個 Pod
2.mount 到類型為emptyDir的Volume中
```
1.sudo kubectl run redis-storage --image=redis --dry-run=client -o yaml > q7-pod.yaml
```

[q7.yaml](https://github.com/oldelette/oldelette.github.io/blob/master/secret/q7.yaml)


## Mock Exam

[小莫 CKA 講解](https://www.youtube.com/@xiaomoinfo/videos?view=2&sort=dd&live_view=503&shelf_id=0)

## Mock Exam - 1

2023.2.25
![](https://i.imgur.com/qaji2FR.png)

[Answer mock_exam_1](https://github.com/oldelette/oldelette.github.io/tree/master/Udemy/mock_exam/1)

### Q5
![](https://i.imgur.com/05HUJEG.png)

### Q7
![](https://i.imgur.com/fR3kboK.png)

### Q8
![](https://i.imgur.com/KjQ2ec2.png)

### Q10
![](https://i.imgur.com/VnVrvrl.png)

### Q11
![](https://i.imgur.com/PkWKMGP.png)

[k8s.io - JSONPath Support](https://kubernetes.io/docs/reference/kubectl/jsonpath/)
```
k get nodes -o=jsonpath='{.items[*].status.nodeInfo.osImage}'
```

### Q12

[k8s.io - Persistent Volumes](https://kubernetes.io/docs/concepts/storage/persistent-volumes/#persistent-volumes)

![](https://i.imgur.com/6wnmdxr.png)


## Mock Exam - 2

[Answer mock_exam_2](https://github.com/oldelette/oldelette.github.io/tree/master/Udemy/mock_exam/2)

### Q3 Security

![](https://i.imgur.com/cWqQX30.png)

[這題](https://ithelp.ithome.com.tw/articles/10241665)的重點是在 **Allow the pod to be able to set system_time**

### Q4 PV & PVC

![](https://i.imgur.com/1vi89ne.png)

### Q6 CSR - Certificate Signing Request

![](https://i.imgur.com/FEcGZeB.png)

```
# Create CertificateSigningRequest
cat CKA/john.csr | base64 | tr -d "\n"
k create -f CKA/q6-csr.yaml
--> certificatesigningrequest.certificates.k8s.io/john-developer created
k get csr
k certificate approve john-developer
```
![](https://i.imgur.com/bCWpTKt.png)


這題創建必要的 role 根 role binding 使得 john 能夠 list / delete / create / delete / get pods

```
k create -f q6.yaml
k describe role -n development
k auth can-i list pods -n=development --as john --> no (rolebind 尚未有權限)

k create -f q6-1.yaml
k describe rolebindings.rbac.authorization.k8s.io -n development
k auth can-i list pods -n=development --as john --> yes (rolebind 完就可以看到有權限了)
```

![](https://i.imgur.com/NO7nzTQ.png)


### Q7 nslookup

![](https://i.imgur.com/38h3btB.png)

```
k run nginx-resolver --image=nginx
k expose pod nginx-resolver --name=nginx-resolver-service --port=80
k run busybox --image=busybox:1.28 -- sleep 4000

k exec busybox -- nslookup nginx-resolver-service
```

:::info
K8s 內有自己的網路, 相同 name space 內的服務都可以透過內部網路直接溝通, 也有自己的 DNS 可以將我們建立的 service name 轉成內部的 IP.
因此內部 DNS 如果無法正常運作, 就會造成內部使用 service name 進行溝通的服務出現問題
:::

* ++svc++
檢測 K8s 內的 DNS
```
k exec busybox -- nslookup nginx-resolver-service > /root/CKA/nginx.svc
```
![](https://i.imgur.com/PKGnoQF.png)

* ++pod++
```
k exec busybox -- nslookup 10-244-192-2.default.pod.cluster.local > /root/CKA/nginx.pod
```
![](https://i.imgur.com/IuF3yFx.png)


#### DNS for Pod

* ++A record++
跟 service 相同，每個 pod 在產生的時候也會以下述的格式分配一個 DNS A record 在 k8s DNS service 中：
:::success
[pod-ip-address].[namespace-name].pod.cluster.local
:::

因此假設 pod 的 ip 為 1.2.3.4，namespace 為 default，且 cluster domain 為 cluster.local，那就會有 1-2-3-4.default.pod.cluster.local 這筆 A record 產生

* ++SRV records++
除了 A record 之外，k8s DNS 還會額外建立相對應的 SRV record，並且用以下的命名規則來產生：
:::success
[_my-port-name].[_my-port-protocol].[svc_name].[namespace_name].svc.cluster.local
:::


### Q8 Static pod on worker node

![](https://i.imgur.com/KEF3C8H.png)

這題的重點是要在 woker node 上執行 pod, 所以我們要先在 controlplane 上生出 pod 的 yaml file, 在 scp  送進去 worker node 上的 static pod path: /etc/kubernetes/manifests

```
kubectl run nginx-critical --image=nginx --dry-run=client -o yaml > static.yaml
```
* Copy the contents of this file or use scp command to transfer this file from controlplane to node01 node.
```
scp static.yaml node01:/root/
kubectl get nodes -o wide
# On node01 node
ssh node01
cp /root/static.yaml /etc/kubernetes/manifests/
```

## Mock Exam - 3
[Answer mock_exam_3](https://github.com/oldelette/oldelette.github.io/tree/master/Udemy/mock_exam/3)

2023.3.1
![](https://i.imgur.com/v1APc0a.png)


### Q1 - Service account / Cluster Role & Cluster RoleBinding

![](https://i.imgur.com/WCGQYLF.png)

```
k create serviceaccount pvviewer
k describe clusterrole pvviewer-role
k describe clusterrolebindings pvviewer-role-binding
```

![](https://i.imgur.com/zxlcysh.png)


### Q2 - json path

![](https://i.imgur.com/C8gUjIu.png)

[k8s-io: Viewing and finding resources](https://kubernetes.io/docs/reference/kubectl/cheatsheet/#viewing-and-finding-resources)
```
 k get nodes -A -o jsonpath='{.items[*].status.addresses[?(@.type=="InternalIP")].address}' > /root/CKA/node_ips
```

### Q3 - Environment Variables

![](https://i.imgur.com/d7ny2Wm.png)

[k8s-io: Define an environment variable for a container](https://kubernetes.io/docs/tasks/inject-data-application/define-environment-variable-container/#define-an-environment-variable-for-a-container)

### Q4 - Security

![](https://i.imgur.com/A4vsWaM.png)


### Q5 - NetworkPolicy

![](https://i.imgur.com/WAvSVfm.png)

利用 curl image 去測試 service: np-test-service 有沒有通, 會發現不通(卡住), 所以要建立 networkpolicies
```
k run curl --image=alpine/curl --rm -it -- sh
curl np-test-service
```

![](https://i.imgur.com/MdKwTgz.png)


### Q6 - Taint / Toleration

![](https://i.imgur.com/0HVGWZp.png)

```
k taint node node01 env_type=production:NoSchedule
```

### Q7 - Namespace

![](https://i.imgur.com/NuTEqBi.png)

```
k create namespace hr
```

### Q8 - Kubeconfig

![](https://i.imgur.com/CSzN4Z5.png)

用 --kubeconfig 指定要使用自己的 kubeconfig file 去 check 正確性
```
k get nodes --kubeconfig CKA/super.kubeconfig
```


### Q9 - Troubleshooting

![](https://i.imgur.com/4AxMlrW.png)

```
k edit deployments.apps nginx-deploy
```
發現沒有正常擴展到 replica=3, 去看 controller-manager.
(因為 Deployment 是通過控制 ReplicaSet，ReplicaSet 再控制 Pod，最终由 controller-manager 驅動達到期望狀態)

```
k describe pod kube-contro1ler-manager-controlplane -n kube-system
vim /etc/kubernetes/manifests/kube-controller-manager.yaml
```

![](https://i.imgur.com/8a40q0w.png)

contro1ler 修正成 controller 即可 (有5個地方錯)


## Lightning Lab

[Answer mock_exam_Lightning Lab](https://github.com/oldelette/oldelette.github.io/tree/master/Udemy/mock_exam/Lightning%20Lab)

### Q1 Upgrade & Taint

![](https://i.imgur.com/STO246l.png)

重點在 Pods for gold-nginx should run on the controlplane node

```
kubectl drain controlplane --ignore-daemonsets
kubectl uncordon controlplane
```
Before draining node01, we need to remove the taint from the controlplane node.

```
# Identify the taint first. 
root@controlplane:~# kubectl describe node controlplane | grep -i taint

# Remove the taint with help of "kubectl taint" command.
root@controlplane:~# kubectl taint node controlplane node-role.kubernetes.io/control-plane:NoSchedule-

# Verify it, the taint has been removed successfully.  
root@controlplane:~# kubectl describe node controlplane | grep -i taint
```
drain the node01
```
kubectl drain node01 --ignore-daemonsets
kubectl uncordon node01
kubectl get pods -o wide | grep gold (make sure this is scheduled on node)
```


### Q2 - json path

![](https://i.imgur.com/GqcZWzn.png)

[k8s-io Custom columns](https://kubernetes.io/docs/reference/kubectl/#custom-columns)
[To print a list of pods sorted by name](https://kubernetes.io/docs/reference/kubectl/#syntax-2)

```
k get deployments.apps -n admin2406 -o custom-columns=DEPLOYMENT:.metadata.name,\
CONTAINER_IMAGE:.spec.template.spec.containers[].image,\
READY_REPLICAS:.status.readyReplicas,NAMESPACE:.metadata.namespace --sort-by=.metadata.name > /opt/admin2406_data
```

### Q3 - KubeConfig (Troubleshoot)

![](https://i.imgur.com/Ez2Skpu.png)


### Q4 - Rolling update

![](https://i.imgur.com/rrTzcg5.png)

```
k edit deployments.apps nginx-deploy --record
k rollout history deployment nginx-deploy
```


### Q5 - PVC (Troubleshoot)

![](https://i.imgur.com/ee23gz0.png)


```
k describe deployments alpha-mysql -n alpha
k describe pvc -n alpha
```

![](https://i.imgur.com/CFWwTNI.png)

要特別注意 PV Capacity 跟 PVC Request  
```
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: mysql-alpha-pvc
  namespace: alpha
spec:
  accessModes:
    - ReadWriteOnce
  volumeMode: Filesystem
  resources:
    requests:
      storage: 1Gi
  storageClassName: slow
```

### Q6 - ETCD Save

![](https://i.imgur.com/MmDhfDN.png)

```
cat /etc/kubernetes/manifests/etcd.yaml --> 找必要檔案的路徑

export ETCDCTL_API=3
ETCDCTL_API=3 etcdctl --endpoints=https://192.31.5.9:2379\
--cacert=/etc/kubernetes/pki/etcd/ca.crt --cert=/etc/kubernetes/pki/etcd/server.crt --\
key=/etc/kubernetes/pki/etcd/server.key snapshot save /opt/etcd-backup.db
-->Snapshot saved at /opt/etcd-backup.db
```

### Q7 - Secret

![](https://i.imgur.com/PyUUVjM.png)

[k8s-io - Optional Secrets](https://kubernetes.io/docs/concepts/configuration/secret/#restriction-secret-must-exist)
將 Secret 製作為一個檔案，Pod 以 Volume 的形式將此檔案掛載 (mount) 到容器上.


```
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: secret-1401
  name: secret-1401
  namespace: admin1401
spec:
  containers:
  - image: busybox
    name: secret-admin
    resources: {}
    command: ["/bin/sh"]
    args: ["-c", "sleep 4800"]
    volumeMounts:
    - name: secret-volume
      mountPath: "/etc/secret-volume"
      readOnly: true
  volumes:
  - name: secret-volume
    secret:
      secretName: dotfile-secret
      optional: true
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
```


## 真題

### 1. RBAC

![](https://i.imgur.com/ZqO8UkH.png)

:::danger
role / rolebinding  有 ns
clusterrole / clusterrolebinding  無 ns
serviceaccount  有 ns

pv 無 ns
pvc 有 ns
:::

```
kubectl create clusterrole deployment-clusterrole --verb=create \
--resource=deployment,daemonset,statefulset

kubectl create serviceaccount cicd-token -n app-team1

kubectl create rolebinding cicd-token-rolebinding --serviceaccount=app-team1:cicd-token \
--clusterrole=deployment-clusterrole -n app-team1
```

![](https://i.imgur.com/l1rp3x6.png)


### 2. CPU

![](https://i.imgur.com/CVJw3y4.png)

```
k top pod -l name=cpu-utilizer -A --sort-by cpu > /opt/KUTR00401/KUTR00401.txt
```

### 3. Networkpolicy

![](https://i.imgur.com/hYYkdDT.png)

```
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: allow-port-from-namespace
  namespace: my-app
spec:
  podSelector: {}
  policyTypes:
    - Ingress
  ingress:
    - from:
        - namespaceSelector:
            matchLabels:
              name: big-corp # 訪問者的 ns labels
      ports:
        - protocol: TCP
          port: 8080
```

#### 檢查: 
allow-port-from-namespace

![](https://i.imgur.com/bHH7uSo.png)

### 4. Service

![](https://i.imgur.com/yg3M6Kk.png)

deployment 修改 pod container port

```
k edit deployment front-end

...
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx
        ports:                #　增加
        - containerPort: 80　　#　增加
          name: http　　　　　　#　增加
          
kubectl expose deployment front-end --port=80 --target-port=80 ＼
--type=NodePort --name=front-end-svc
```

#### 檢查

```
1. curl <node>:<Nodeport>
--> curl cluster-node2:32566
2. curl <svc>:80
--> curl 10.108.194.230:80
```
![](https://i.imgur.com/5iku3CA.png)


### 5. Ingress

![](https://i.imgur.com/TmKsgR8.png)

* -k：跳過 SSL 證書檢測 
* -L：跟随跳轉，比如網站做了重定向，不加這個選項的话只會看到一個 302 的訪問 code 就结束了，加上的話會看到完整的跳轉情况

```
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: pong
  namespace: ing-internal
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
spec:
  ingressClassName: nginx-example
  rules:
  - http:
      paths:
      - path: /hello
        pathType: Prefix
        backend:
          service:
            name: hello
            port:
              number: 5678
```

#### 檢查

```
kubectl get ingress -ning-internal
curl -kL internal_IP/hello
```


### 6. Deployment scale

![](https://i.imgur.com/WoI8ARW.png)

```
k scale deployment loadbalancer --replicas 2
```

### 7. nodeSelector

![](https://i.imgur.com/GGYzQGp.png)

[k8s-io Using Pods](https://kubernetes.io/docs/concepts/workloads/pods/#using-pods) 再手動新增 **nodeSelector 欄位**

```
apiVersion: v1
kind: Pod
metadata:
  name: nginx-kusc00401
spec:
  containers:
  - name: nginx-kusc00401
    image: nginx
  nodeSelector: #這個是和 containers 同一级别的。
    disk: ssd
```

### 8. Node Scheduled

![](https://i.imgur.com/5IpxBFc.png)

```
k describe node | grep Taint | grep -vc NoSchedule > /opt/KUSC00402/kusc00402.txt
```
* -c 代表統計個數
* -v 代表排除

#### 檢查
```
cat /opt/KUSC00402/kusc00402.txt
--> 2
```

### 9. Multi-container

![](https://i.imgur.com/VutY8Tk.png)
[k8s-io Using Pods](https://kubernetes.io/docs/concepts/workloads/pods/#using-pods) 在手動增加多個 container
```
apiVersion: v1
kind: Pod
metadata:
  name: kucc4
spec:
  containers:
  - name: nginx
    image: nginx
  - name: redis
    image: redis
  - name: memcached
    image: memcached
```


#### 檢查
```
k get pods 確認有 3/3
```
![](https://i.imgur.com/sK54hYd.png)


### 10. PV

![](https://i.imgur.com/zIL4D3U.png)

[k8s-io Persistent Volumes](https://kubernetes.io/docs/concepts/storage/persistent-volumes/#persistent-volumes) 再去修正成題目要的

```
apiVersion: v1
kind: PersistentVolume
metadata:
  name: app-data
spec:
  capacity:
    storage: 2Gi
  volumeMode: Filesystem
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: /srv/app-data
```

### 11. PVC

![](https://i.imgur.com/CJx0FbT.png)

[k8s-io PersistentVolumeClaims](https://kubernetes.io/docs/concepts/storage/persistent-volumes/#persistentvolumeclaims) 再去修正成題目要的

```
# PVC
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: pv-volume
spec:
  accessModes:
    - ReadWriteOnce
  volumeMode: Filesystem
  resources:
    requests:
      storage: 10Mi
  storageClassName: ci-hostpath-sc
```
[k8s-io Claims As Volumes ](https://kubernetes.io/docs/concepts/storage/persistent-volumes/#claims-as-volumes) 再去修正成題目要的
```
# Pod
      volumeMounts:
      - mountPath: "/usr/share/nginx/html"
        name: mypd
  volumes:
    - name: mypd
      persistentVolumeClaim:
        claimName: pv-volume
```
更改 pvc 容量
```
kubectl edit pvc pv-volume
```

![](https://i.imgur.com/Ey8ttem.png)


### 12. Pod log

![](https://i.imgur.com/sPb2EZe.png)

```
k logs bar | grep "file-not-found" > /opt/KUTR00101/bar
```


### 13. Sidecar

![](https://i.imgur.com/EeVt4ek.png)


sidecar 邊車容器不是作為主容器，而是輔助主容器做一些功能

```
# 先把運行中的 legacy-app pod 倒出來成 yaml file, 然後備份
kubectl get pods leagcy-app -o yaml > sidecar.yaml
cp sidecar.yaml sidecar.yaml.bkg

# 修改好後
kubectl delet -f sidecar.yaml
kubectl create -f sidecar.yaml
```

[k8s-io Using a sidecar container with the logging agent](https://kubernetes.io/docs/concepts/cluster-administration/logging/#sidecar-container-with-logging-agent) 再去修正成題目要的

* 增加之內容
```
... 
    volumeMounts:
    - name: varlog
      mountPath: /var/log
  - name: count-log-1
    image: busybox:1.28
    args: [/bin/sh, -c, 'tail -n+1 -F /var/log/1.log']
    volumeMounts:
    - name: varlog
      mountPath: /var/log
  volumes:
  - name: varlog
    emptyDir: {}
```



### 14. Upgrade

[CKA真题14](https://www.youtube.com/watch?v=LTvaD8aWTiM&ab_channel=%E5%B0%8F%E8%8E%AB)

![](https://i.imgur.com/kqNGyHf.png)

**题目要求不升级 etcd，這裡手動補上**

```
1、切換環境
kubectl config use-context mk8s

2、配置
#升级kueadm
kubectl drain mk8s-master-0 --ignore=daemonsets

ssh mk8s-master-0
sudo -i 

 apt-mark unhold kubeadm && \
 apt-get update && apt-get install -y kubeadm=1.26.0-00 && \
 apt-mark hold kubeadm

kubeadm upgrade apply v1.26.0 --etcd-upgrade=false

#升级 kubelt
apt-mark unhold kubelet kubectl && \
apt-get update && apt-get install -y kubelet=1.26.0-00 kubectl=1.26.0-00 && \
apt-mark hold kubelet kubectl

sudo systemctl daemon-reload
sudo systemctl restart kubelet #這裡要重啟 kubelt 的

exit
exit

kubectl uncordon mk8s-master-0

3、驗證
kubectl get node -o wide
kubectl --version
kubelet --version
```


### 15. ETCD backup & Restore

[CKA真题15](https://www.youtube.com/watch?v=ZNOMtTUU7XM&ab_channel=%E5%B0%8F%E8%8E%AB)

![](https://i.imgur.com/yGnWxub.png)

```
1、確定當前環境
kubectl get node
2. 配置
export ETCDCTL_API=3
etcdctl --endpoints=https://127.0.0.1:2379 \
  --cacert=<trusted-ca-file> --cert=<cert-file> --key=<key-file> \
  snapshot save /var/lib/backup/etcd-snapshot.db
  
# Restoring an etcd cluster
ll /data/backup/etcd-snapshot-previous.db
# (不確定對不對) 直接還原, 不要加 --data-dir
etcdctl snapshot restore /var/lib/backup/etcd-snapshot-previous.db
```
```
# 正常流程, 還要去修正 /etc/kubernetes/manifests/etcd.yaml 裡面的 hostPath: 路徑
etcdctl snapshot restore --data-dir <data-dir-location> \
/var/lib/backup/etcd-snapshot-previous.db
vim /etc/kubernetes/manifests/etcd.yaml
```

* –data-dir: 自己定義要將 ETCD 還原的位置 \<data-dir-location\> ，會自動幫創建資料夾


以下應該正確 restore
```
mv /etc/kubernetes/manifests /etc/kubernetes/manifests_bkg
ETCDCTL_API=3 etcdctl snapshot restore /tmp/etcd-backup.db
cp -r /etc/kubernetes/manifests_bkg /etc/kubernetes/manifests
# 裡面的 hostPath 路徑修正
vim /etc/kubernetes/manifests/etcd.yaml 
#　
```

![](https://i.imgur.com/T76Ky8q.png)


### 16. Troubleshoot
[CKA真题16](https://www.youtube.com/watch?v=H5Qtns6Y3UU&ab_channel=%E5%B0%8F%E8%8E%AB)

![](https://i.imgur.com/G0Mwkqo.png)


通過 get nodes 查看異常節點，登入節點查看 kubelet 等组件的 status 並判斷原因.
真實考试时，這個異常節點的 kubelet 服務没有啟動導致的.
journalctl -u kubelet


```
k config use-context wk8s
k get node
ssh wk8s-node-0
sudo -i
systemctl status kubelet
systemctl start kubelet
systemctl enable kubelet
```

### 17. Drain & Cordon

[CKA真题17](https://www.youtube.com/watch?v=jx1R0EzMWEA&ab_channel=%E5%B0%8F%E8%8E%AB)

![](https://i.imgur.com/hHSa1AZ.png)

```
k config use-context wk8s
kubectl cordon ek8s-node-1
kubectl drain ek8s-node-1 --ignore-daemonsets

# 如果 --ignore-daemonsets 報錯, 使用
kubectl drain ek8s-node-1 --ignore-daemonsets --delete-local-data --force
kubectl drain ek8s-node-1 --ignore-daemonsets --delete-emptydir-data --force

# 驗證
kubectl get node
```

正常來說已經沒有 pod 會在 ek8s-node-1 上了, 但是 daemonsets 模式的 pod 還是會顯示在 ek8s-node-1 上 (因為上面加了 --ignore-daemonsets)