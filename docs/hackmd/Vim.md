###### tags: `Lesson-study`

# Vim

[每個開發者都應該要會用的編輯器–Vim](https://medium.com/@jinghua.shih/%E6%AF%8F%E5%80%8B%E9%96%8B%E7%99%BC%E8%80%85%E9%83%BD%E6%87%89%E8%A9%B2%E8%A6%81%E6%9C%83%E7%94%A8%E7%9A%84%E7%B7%A8%E8%BC%AF%E5%99%A8-vim-5f83349973a3)

![](https://i.imgur.com/7cvAjV6.png)

* Undo: use the u, :u or :undo commands
* Redo: use the Ctrl-R or :redo

:::success
CTRL-w + w => 將指標移到下一個分割視窗
CTRL-w + W => 將指標移到上一個分割視窗
:::

---
在現代 IDE 各種酷炫功能的情況下，Vim 這個被很多人嫌古老的編輯器當然不甘示弱地出了很多厲害的外掛。
[Vim Awesome](https://vimawesome.com/) 是一個集大成的網頁，可以上去挑選自由取用

## Vundle

[Vundle：管理 vim 套件的工具](https://noob.tw/vundle/)


Vundle 是個可以管理 Vim 外掛的外掛。
只需要輸入套件的名字、儲存，Vundle 就會自動安裝，可以很方便的管理。

其他的還有 [vim-pathogen](https://github.com/tpope/vim-pathogen) 與 [vim-plug](https://github.com/junegunn/vim-plug)。

```
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
```

常用命令
```
# 更新外掛
:PluginInstall!
# 清除不再使用的外掛
:PluginClean
# 列出所有外掛
:PluginList
# 查詢外掛
:PluginSearch
```


### 使用 Plugin 插件

儲存後，使用 vim 進入到編輯器，使用命令行模式，輸入 :PluginInstall。


外觀風格插件
```
Plugin 'flazz/vim-colorschemes'
```
flazz/vim-colorschemes : https://github.com/flazz/vim-colorschemes


## .vimrc 優化

[vim 強大的地方在於使用者可以自由的設定和擴充功能](http://wiki.csie.ncku.edu.tw/vim/vimrc?printable)，讓 vim 為自己工作，進而擁有良好得編輯體驗


### 介面優化

[影片介紹: Vim 編輯器 配置指南](https://www.youtube.com/watch?v=slkMoo-UZx4)
[文章介紹: 【Vim 編輯器 配置指南】訂製個人的編輯神器](https://ithelp.ithome.com.tw/articles/10258222)


* :set nu
顯示行號：對於 debug 相當有幫助!
* :set ai
自動對齊縮排：如果上一行有兩個 tab 的寬度，按 enter 繼續編輯下一行時會自動保留兩個 tab 鍵的寬度。
* :set cursorline
光標底線：光標所在的那一行會有底線，幫助尋找光標位置

* :set incsearch
在關鍵字尚未完全輸入完畢前就顯示結果
如果覺得這功能太過熱心的話，可以使用 ctrl+n 來達成自動補完的功能





### My Setting

:::success
sudo apt install silversearcher-ag
sudo apt install ack
sudo apt-get install exuberant-ctags  --> Tagbar
sudo apt install cmake --> YouCompleteMe
:::


* 2021/12/31

~/.vimrc

```
set nocompatible              " be iMproved, required
filetype off                  " required

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'flazz/vim-colorschemes'
Plugin 'scrooloose/nerdtree'
Plugin 'jistr/vim-nerdtree-tabs'
Plugin 'Valloric/YouCompleteMe'
Plugin 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' } 
Plugin 'junegunn/fzf.vim'
" Plugin 'kien/ctrlp.vim'
" Plugin 'tacahiroy/ctrlp-funky'
Plugin 'majutsushi/tagbar'
Plugin 'mileszs/ack.vim'
Plugin 'tomtom/tcomment_vim'

call vundle#end()            " required
filetype plugin indent on    " required

" NERDTreeToggle
nnoremap <c-a> : NERDTreeToggle<CR>
nnoremap <F8> : TagbarToggle<CR>
let g:tagbar_width = 30
autocmd BufReadPost *.py call tagbar#autoopen() 

syntax enable

set nu
set t_Co=256
set cursorline
set background=dark
"colorscheme molokai
colorscheme gruvbox
"colorscheme wombat

" Use highlighting when doing a search.
set hlsearch 
set path=.,/usr/include,,**

" Set tab width to 4 columns.
set tabstop=4
set expandtab
set autoindent

map <C-s> :call SwitchLineNumber()<CR>
map! <C-s> <Esc>:call SwitchLineNumber()<CR>
function SwitchLineNumber()
    if (&nu == 0)
        set nu
        echo "Enable line number."
    else
        set nonu
        echo "Disable line number."
    endif
endfunction

"ctrl+o Support mouse
map <C-o> :call SwitchMouseMode()<CR>
map! <C-o> <Esc>:call SwitchMouseMode()<CR>
function SwitchMouseMode()
    if (&mouse == "a")
        let &mouse = ""
        echo "Mouse is disabled."
    else
        let &mouse = "a"
        echo "Mouse is enabled."
    endif
endfunction

" Runnung code F5 setting
nmap <F5> :call CompileRun()<CR>
func! CompileRun()
        exec "w"
if &filetype == 'python'
            exec "!time python3 %"
endif
    endfunc

" JSON format setting
command! JSONFormat :execute '%!python3 -m json.tool'

" YouCompleteMe
let g:ycm_global_ycm_extra_conf='~/.vim/bundle/YouCompleteMe/.ycm_extra_conf.py'
let g:ycm_confirm_extra_conf=0
let g:ycm_python_binary_path='/usr/bin/python3'

"" CtrlP
"let g:ctrlp_map = '<c-p>'
"let g:ctrlp_cmd = 'CtrlP'
"let g:ctrlp_working_path_mode = 'ra'
"let g:ctrlp_custom_ignore = {
"  \ 'dir':  '\v[\/]\.(git|hg|svn)$',
"  \ 'file': '\v\.(exe|so|dll)$',
"  \ 'link': 'some_bad_symbolic_links',
"  \ }
"
"" The Silver Searcher
"if executable('ag')
"  " Use ag over grep
"  set grepprg=ag\ --nogroup\ --nocolor
"  " Use ag in CtrlP for listing files. Lightning fast and respects .gitignore
"  let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'
"  " ag is fast enough that CtrlP doesn't need to cache
"  let g:ctrlp_use_caching = 0
"endif

" fzf settings
" This is the default extra key bindings
nnoremap ff :Files<CR>
nnoremap fa :Ag<CR>
let g:fzf_action = {
            \ 'ctrl-t': 'tab split',
            \ 'ctrl-x': 'split',
            \ 'ctrl-v': 'vsplit' }

" Customize fzf colors to match your color scheme
let g:fzf_colors =
            \ { 'fg':      ['fg', 'Normal'],
            \ 'bg':      ['bg', 'Normal'],
            \ 'hl':      ['fg', 'Comment'],
            \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
            \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
            \ 'hl+':     ['fg', 'Statement'],
            \ 'info':    ['fg', 'PreProc'],
            \ 'prompt':  ['fg', 'Conditional'],
            \ 'pointer': ['fg', 'Exception'],
            \ 'marker':  ['fg', 'Keyword'],
            \ 'spinner': ['fg', 'Label'],
            \ 'header':  ['fg', 'Comment'] }

" explicitly bind the keys to down and up in your $FZF_DEFAULT_OPTS.
let g:fzf_history_dir = '~/.local/share/fzf-history'


"Ack
if executable('ag')
  let g:ackprg = 'ag --vimgrep'
endif
nnoremap FF :Ack!<Space>


```

### Vim 功能快捷鍵設置

[參考設定教學: ](https://magiclen.org/vimrc/)

```
"ctrl+n Enable/Disable LineNumber
map <C-s> :call SwitchLineNumber()<CR>
map! <C-s> <Esc>:call SwitchLineNumber()<CR>
function SwitchLineNumber()
    if (&nu == 0)
        set nu
        echo "Enable line number."
    else
        set nonu
        echo "Disable line number."
    endif
endfunction

"ctrl+n Support mouse
map <C-n> :call SwitchMouseMode()<CR>
map! <C-n> <Esc>:call SwitchMouseMode()<CR>
function SwitchMouseMode()
    if (&mouse == "a")
        let &mouse = ""
        echo "Mouse is disabled."
    else
        let &mouse = "a"
        echo "Mouse is enabled."
    endif
endfunction
```



## 快捷鍵


### 快速註解

[快速註解程式碼的步驟](https://clay-atlas.com/blog/2020/11/05/vim-cn-comment-delete-code/)

Step1: 按下 CTRL + v 進入 -- VISUAL BLOCK -- 模式
Step2: 選擇要註解的區塊
Step3: 按下 Shift + i 進入輸入模式，輸入註解符號（假設是 #）
Step4: 按下 Esc 鍵




### 快速取消註解

Step 1: 按下 CTRL + v 選擇要取消註解的區塊
Step 2: 按下 d 刪除註解符號


### 刪除全部code

ggdG

gg: 跳到文字首行
d: 刪除
G: 跳到文件尾行


### 跳到指定行

ngg 或者 nG (n 帶入指定的行數)

ex: 25gg




# Vim Plugin

[好用的 Vim 外掛介紹 -ctrlp  ](https://www.youtube.com/watch?v=wjoSbLGZTao&t=185s&ab_channel=%E9%AB%98%E8%A6%8B%E9%BE%8D)
[好用的 Vim 外掛介紹 - tComment](https://www.youtube.com/watch?v=Q0NWJvWI9xQ&ab_channel=%E9%AB%98%E8%A6%8B%E9%BE%8D)


## YouCompleteMe

[YouCompleteMe](https://github.com/ycm-core/YouCompleteMe) 是vim 程式的提示套件

[YouCompleteMe 安裝步驟](https://clay-atlas.com/blog/2020/04/15/%E5%9C%A8-vim-%E4%B8%AD%E4%BD%BF%E7%94%A8-youcompleteme-%E8%87%AA%E5%8B%95%E8%A3%9C%E5%AE%8C-python-%E7%A8%8B%E5%BC%8F%E7%A2%BC/)

[ubuntu16.4下vim安装YouCompleteMe教程](https://segmentfault.com/a/1190000019949732)

### step1

```
cd ~/.vim/bundle
git clone https://github.com/ycm-core/YouCompleteMe.git
git submodule update --init --recursive
sudo apt install cmake
```


### step2

添加 ".vimrc" 的配置

```
" YouCompleteMe
let g:ycm_global_ycm_extra_conf='~/.vim/bundle/YouCompleteMe/.ycm_extra_conf.py'
let g:ycm_confirm_extra_conf=0
let g:ycm_python_binary_path='/usr/bin/python3'
```

其中 "let g:ycm_python_binary_path" 設定的路徑應為你的 Python Interpreter 的路徑。
結束設置之後儲存文件退出

### step3

```
:source %
Plugin 'Valloric/YouCompleteMe'
```

:PluginInstall



### step4
編譯 YouCompleteMe

```
cd ~/.vim/bundle/YouCompleteMe
./install.py --clang-completer
```


* ++成果++

![](https://i.imgur.com/oLhrSHz.png)


## Vim-gitgutter

快速查看本次 commit 新增了什麼 [Ref](https://noob.tw/vim-gitgutter/)
* setting in .vimrc
```
set updatetime=100
highlight GitGutterAdd    ctermfg=blue
highlight GitGutterChange ctermfg=green
highlight GitGutterDelete ctermfg=red
```


## The-NERD-tree

可以在 Vim 執行時顯示目錄和檔案結構


q: 關閉 NerdTree 介面
ctrl+w+w: 游標自動在左右側介面切换 


## ack

[ack.vim](https://github.com/mileszs/ack.vim)


```
sudo apt install ack
Plugin 'mileszs/ack.vim'
```

* 使用 the_silver_searcher

```
if executable('ag')
  let g:ackprg = 'ag --vimgrep'
endif
```


* Rename

```
nnoremap FF :Ack!<Space>
```


## fzf

[fzf ❤️ vim](https://github.com/junegunn/fzf.vim#Commands)
[Vim Plugin Highlight: fzf.vim! Fuzzy File Finding Fun!](https://www.youtube.com/watch?v=DpURGnb4Fyk&ab_channel=codevion)

```
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
```



### 快捷鍵配置

ff就能快速開啟檔案搜尋視窗、輸入,fa後可以快速開啟全域搜尋的視窗

```
nnoremap ff :Files<CR>                                          
nnoremap fa :Ag<CR>
```


## Ctrlp



fuzzy search。
按下 Ctrl+P 打下任何關鍵字，演算法就會馬上計算，去你當前目錄裡面搜相符程度最高的檔案給你，非常便利的一項工具

```
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'
let g:ctrlp_working_path_mode = 'ra'
```



* 按下 Esc 或 \<Ctrl-c> 可退出ctrlp，返回到Vim窗口中
* ctr+d 搜尋依照檔名


### Faster Grepping in Vim

[Faster Grepping in Vim](https://thoughtbot.com/blog/faster-grepping-in-vim)
CtrlP 預設使用 grep 來搜索，我們可以把它改成下面的 ack，會讓表現更好。
在 .vimrc 加上：

```
" The Silver Searcher
if executable('ag')
  " Use ag over grep
  set grepprg=ag\ --nogroup\ --nocolor

  " Use ag in CtrlP for listing files. Lightning fast and respects .gitignore
  let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'

  " ag is fast enough that CtrlP doesn't need to cache
  let g:ctrlp_use_caching = 0
endif
```


## Tagbar

```
Plugin 'majutsushi/tagbar'
```


### user setting

手動啟動 f8
自動啟動在 *.py 的檔案

```
nnoremap <F8> : TagbarToggle<CR>
let g:tagbar_width = 25
autocmd BufReadPost *.py call tagbar#autoopen() 
```

* Tagbar: Exuberant ctags not found!
```
sudo apt-get install exuberant-ctags
```

## tcomment

[tcomment_vim](https://github.com/tomtom/tcomment_vim)

```
Plugin 'tomtom/tcomment_vim'
```

按兩次 ctrl + /





## gedit

修改 gedit 背景


![](https://i.imgur.com/iZ5G06B.png)





# Emac

使用 Bash 時練習 Emacs [REF](https://www.gushiciku.cn/pl/p52z/zh-tw)

```
c對應ctrl，m對應alt，s對應shift
```

瞭解 Emacs 鍵盤快捷鍵的一個好處是，其中許多快捷鍵也適用於 Bash。

* C-a：到行首
* C-e：到行尾
* C-u：剪下整行
* M-f：向前一個字
* M-b：向後一個字
* M-d：剪下一個字
* C-y：貼回（貼上）最近剪下的內容
* M-Shift-U：大寫一個詞
* C-t：交換兩個字元（例如，sl 變成 ls）

