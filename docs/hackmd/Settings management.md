###### tags: `Work`

# Settings management

將一些重要的資料存在環境變數(environment variable)中，是開發時常見的手段，不僅可以避免將重要的資料不小心 commit 進 codebase 之外，也可以利用環境變數儲存系統或程式設定。

實務上也經常利用環境變數區隔開發環境(development)與生產環境(production)。
```python=
if os.getenv('MODE') == 'development':
    # do development-related things
    pass
else:
    # do production-related things
    pass
```

## driconfig

The usage of YAML files to store configurations and parameters is widely accepted in the Python community, especially in Data Science environments.
[DriConfig](https://dribia.github.io/driconfig/) provides a clean interface between your Python code and these YAML configuration files.
It is heavily based on Pydantic's Settings Management, preserving its core functionalities and advantages.

```
pip install driconfig
```

與 pydantic  和 dotenv 差別是在 Config 是 yaml file.

Let's say we have a YAML configuration file config.yaml with the following data:

```
# config.yaml
model_parameters:
  eta: 0.2
  gamma: 2
  lambda: 1

date_interval:
  start: 2022-01-06
  end: 2022-12-31
```

Then we can parse with driconfig as follows:

```python=
from datetime import date
from typing import Dict

from driconfig import DriConfig
from pydantic import BaseModel


class DateInterval(BaseModel):
  """Model for the `date_interval` configuration."""
  start: date
  end: date


class AppConfig(DriConfig):
   """Interface for the config/config.yaml file."""

   class Config:
       """Configure the YAML file location."""

       config_folder = "."
       config_file_name = "config.yaml"

   model_parameters: Dict[str, float]
   date_interval: DateInterval

config = AppConfig()
print(config.json(indent=4))
```



## Pydantic
pydantic 是透過型別註記 (type annotations)提供資料型別驗證與設定管理的套件。

```
pip install pydantic
```

```python=
from datetime import datetime
from pydantic import BaseModel

class Car(BaseModel):
    brand_id: int
    brand_name: str
    wheels: int = 4
    created_at: datetime = datetime.now()
    
Car(brand_id='bad_id', brand_name='Bad name')
```

執行結果會出現類似以下的錯誤，顯示我們應該將 brand_id 設定為整數型別：

```
File "pydanticc.py", line 12, in <module>
  Car(brand_id='bad_id', brand_name='Bad name')
File "pydantic/main.py", line 331, in pydantic.main.BaseModel.__init__
pydantic.error_wrappers.ValidationError: 1 validation error for Car
brand_id
  value is not a valid integer (type=type_error.integer)
```

### BaseSettings 

pydantic 的設定管理可以透過繼承 [BaseSettings](https://myapollo.com.tw/zh-tw/python-pydantic/) 類別進行實作，該類別與 BaseModel 類別最大差別在於 BaseSettings 提供環境變數(environment variables)與 dotenv 的整合。

當我們建立繼承 BaseSettings 類別的實例(instance)時， BaseSettings 會自動載入環境變數：

```python=
from pydantic import BaseSettings

class Settings(BaseSettings):
    HOME: str
    SHELL: str
print(Settings())
**印出**
HOME='/home/gavin' SHELL='/bin/bash'
```


我們就能夠針對不同的環境設計不一樣的設定，例如以下範例，我們將通用的設定放在 Settings 類別中，藉由繼承 Settings 類別覆寫(override)因環境有所不同的設定：


```python=
import os
from pydantic import BaseSettings

class Settings(BaseSettings):
    API_BASE: str
    DB_HOST: str
    DB_PORT: int
    DB_NAME: str
    TESTING: bool = False

    class Config:
        env_file = '.env'

class Production(Settings):
    API_BASE = 'https://example.com/api'
    DB_NAME = 'production'

class Testing(Settings):
    API_BASE = 'https://testing.example.com/api'
    DB_NAME = 'testing'

    class Config:
        env_file = '.testing.env'

def get_settings():
    env = os.getenv('ENV', 'TESTING')
    if env == 'PRODUCTION':
        return Production()
    return Testing()
settings = get_settings()

print('DB Host =', settings.DB_HOST)
print('DB Port =', settings.DB_PORT)
**印出**
DB Host = 127.0.0.1
DB Port = 6666
```

.env 內容：
```
DB_HOST=localhost
DB_PORT=8888
BLAH_BLAH=Banana
```
.testing.env 內容：
```
DB_HOST=127.0.0.1
DB_PORT=6666
```

必須注意的是 .env 中的 BLAH_BLAH=Banana 並不是前述 Settings 與 Production 2 類別內所定義的屬性之一，所以並不會被自動載入到 Production 內，如果想要載入 BLAH_BLAH 就必須為 Settings 或者 Production 類別內定義 BLAH_BLAH 屬性才行。



## Python dotenv3

[Python dotenv](https://myapollo.com.tw/zh-tw/python-dotenv/) 的運作十分簡單，預設 python-dotenv 會載入 .env 檔案，然後將設定寫入環境變數之中，接著就能夠透過 os.getenv(key, default=None) 取得環境變數中的值。

```
pip install python-dotenv
```
```
export DBHOST=localhost
export DBPORT=5432
```

python-dotenv 中可以省略 export ，讓 .env 檔更簡潔：
python-dotenv 也支援 [POSIX parameter expansion](https://pubs.opengroup.org/onlinepubs/009695399/utilities/xcu_chap02.html#tag_02_06_02) 的用法，例如讓變數中可以再放入變數：
```
MODE=development
DBHOST=localhost
DBPORT=5432
DBCONN_STR=${DBHOST}:${DBPORT}
```


### load_dotenv()

load_dotenv() 會自動載入 .env 檔

```python=
import os
from dotenv import load_dotenv

print('Before load_dotenv()', os.getenv('DBCONN_STR'))
load_dotenv()
print('After load_dotenv()', os.getenv('DBCONN_STR'))
```

:::success
gavin@svrdhcp:~/gavin/adv_python/pydantic_practice$ python3 test.py 
Before load_dotenv() None
After load_dotenv() localhost:5432
:::


### dotenv_values

dotenv_values just returns a dict with the values parsed from the .env file.

```
config = dotenv_values(".env")
印出
OrderedDict(
[('MODE', 'development'), 
('DBHOST', 'localhost'), 
('DBPORT', '5432'), 
('DBCONN_STR', 'localhost:5432')])
```


### setting multiple env


```
import os
from dotenv import dotenv_values

config = {
    **dotenv_values(".env.shared"),  # load shared development variables
    **dotenv_values(".env.secret"),  # load sensitive variables
    **os.environ,  # override loaded values with environment variables
}
```
