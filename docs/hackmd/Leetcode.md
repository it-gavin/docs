###### tags: `Language`

# Leetcode 

[Personal Schedule Table](https://docs.google.com/spreadsheets/d/11FAqPDPMvcHntCI-pclm5yC5zeW1jqCtftk9X2Xmsx0/edit#gid=0)


[這個是在 Blind 這個論壇有個 FB 工程師整理出來的](https://www.ptt.cc/bbs/Soft_Job/M.1605589986.A.CBA.html)。
很多人非常推。
這個清單集合大部分的經典題目。
可以把它想成是基礎題目，很多其他題目都是由這些題目衍伸出去的。

[這個就有點像是上面的擴充板](https://www.programcreek.com/2013/08/leetcode-problem-classification/)。
這個清單也會依照不同的主題分類讓你想要一次大量練習某個主題的經典題時很方便。我會建議裡面的 Dynamic Programming可以先跳過。


## Array

### 1. Two Sum

![](https://i.imgur.com/fGfevj4.png)

```python=
class Solution:
    def twoSum(self, nums, target):
        for i,num in enumerate(nums):
            sub=target-num
            if sub in nums:
                if nums.index(sub)<i:
                    return [nums.index(target-num),i]
```

```c=
int* twoSum(int* nums, int numsSize, int target, int* returnSize){
    *returnSize=2;
    int sub=0,i=0,j=0;
    int *res=malloc(sizeof(int)*2);
    for(i=0;i<numsSize;i++)
    {
        sub=target-nums[i];
        for(j=i+1;j<numsSize;j++)
        {
            if (sub==nums[j])
            {
                res[0]=i;
                res[1]=j;
            }
        }
    }
    return res;
}
```


類似 167. Two Sum II - Input array is sorted


### 4. Median of Two Sorted Arrays

這題的難點是在 時間複雜度要求為 O(log (m+n))

* 使用 .sort() 可以對 list/tuple/set 做排序

時間複雜度: O(nlogn) Timesort

```python=
class Solution:
    def findMedianSortedArrays(self, nums1: List[int], nums2: List[int]) -> float:
        res=nums1+nums2
        res=sorted(res)
        ln=len(res)
        #print(res)
        if ln%2==1:
            return res[int(ln/2)]
        else:
            if (res[int(ln/2)]+res[int(ln/2)-1])!=0:
                return (res[int(ln/2)]+res[int(ln/2)-1])/2
            else:
                return 0
```


### 7. Reverse Integer

![](https://i.imgur.com/Mo46RfH.png)

* 方法一 ([::-1] string reverse)
```python
class Solution:
    def reverse(self, x: int) -> int:
        if x >= 2**31-1 or x <= -2**31:
            return 0
        if x>0:
            a=str(x)
            b=int(a[::-1])
        else:
            a=str(abs(x))
            a="-"+a[::-1]
            b=int(a)
        if b>=2**31-1 or b<=-2**31:
            return 0
        return b

```

* 方法二 (只有考慮單純reverse 不管範圍)
```
def revers(x):
    new_x=0
    if x<0:
        x*=-1
        flag=1
    while x:
        new_x= 10*new_x + x%10
        x/=10
        x=int(x)
    if flag==1:
        new_x*=-1

    return new_x
```


### 15. 3Sum


想法: (Time Limit Exceeded)
判斷是否有target
```
class Solution:
    def threeSum(self, nums: List[int]) -> List[List[int]]:
        nlen=len(nums)
        res=[]
        temp=[]
        target=0
        if nlen<3:
            return res
        if nums.count(0)==3:
            res.append([0,0,0])
        for i in range(nlen):
            for j in range(i+1,nlen):
                target=0-nums[i]-nums[j]
                #print("aa:",nums.index(target))
                if target in nums and nums.index(target)!=i and nums.index(target)!=j :
                    ir=0
                    temp= [nums[i], nums[j], target]
                    #print(temp)
                    for g in res:
                        if set(g)==set(temp):
                            ir=1
                    if ir==0:
                        res.append(temp)
        return res
```

[Ref: [Day 4] 從LeetCode學演算法 - 0015. 3Sum (Medium)](https://ithelp.ithome.com.tw/articles/10213264)


1. V 固定，假設其中一個是 X ， 另一個數target只能是 -V-X (和為0)
2. 每次對應的互補數(-V-X) 加入dic 中，如存在的話放入res中。

```
class Solution:
    def threeSum(self, nums: List[int]) -> List[List[int]]:
        n=len(nums)
        if len(nums) < 3:
            return []
        nums.sort()
        res=set()  # build empty set
        for i,v in enumerate(nums[:-2]):
            if i>=1 and v==nums[i-1]: #same value skip, 可不加
                continue
            d={}
            for x in nums[i+1:]:
                target=-v-x
                if x not in d:
                    d[target]=1
                else:
                    res.add((v,target,x))
        return list(res)
```


### 18. 4Sum

two pointer 解法

```python
class Solution:
    def fourSum(self, nums: List[int], target: int) -> List[List[int]]:
        ans=set()
        nums.sort()
        n = len(nums)
        for i in range(n):
            for j in range(i+1, n):
                left = j + 1
                right = n - 1

                while left < right:
                    total = nums[i] + nums[left] + nums[right] + nums[j]
                    if total > target:
                        right -= 1
                    elif total < target:
                        left += 1
                    else:
                        ans.add(tuple(sorted((nums[i], nums[left], nums[right], nums[j]))))
                        left += 1
                        right -= 1
        return ans
```

:::success
**immutable:** (一旦創立 不可修改) == **hashable**
integer, string, tuple

**mutable:**
list, dictionary, set
:::


### 26. Remove Duplicates from Sorted Array

![](https://i.imgur.com/JzSSKx8.png)


* modifying the input array in-place with O(1) extra memory.
不能使用額外的空間

*  input array is passed in by reference, which means a modification to the input array will be known to the caller as well.
pass by reference 直接修改input array


```
class Solution(object):
    def removeDuplicates(self, nums):
        count = 0
        le=len(nums)
        for i in range(1, le):
            if nums[i] == nums[i-1]:
                count += 1
            else: #不重複的值往前挪，後面的不用管
                nums[i-count] = nums[i]
        return len(nums) - count
```



### 27. Remove Element

移除 array 中值等於 val 的 element

ex:
[0,1,2,2,3,0,4,2] val=2
count: val代表重複個數，
所以後面假設有不重複要扣掉val重複個數所佔的位置，**nums[i-count]=nums[i]**。


```
class Solution:
    def removeElement(self, nums: List[int], val: int) -> int:
        le=len(nums)
        count=0
        for i in range(0,le):
            if nums[i]==val:
                count+=1
            else:
                nums[i-count]=nums[i]
                
        return le-count
```

### 33. Search in Rotated Sorted Array

重點是在要符合時間複雜度 O(logN)
binary search



* 非正解 因為不符合題目要求的時間複雜度 O(N)
```python!
class Solution:
    def search(self, nums: List[int], target: int) -> int:
        if target in nums:
            return nums.index(target)
        return -1
```

![](https://i.imgur.com/RFBHR60.png)

1.我們要如何知道要 middle 是屬於 array 的左半邊排序還是右半邊排序
nums[left] <= nums[mid]: 代表 mid 是屬於左排序
nums[left] > nums[mid]: 代表 mid 是屬於右排序


* binary search 變形
```python!
class Solution:
    def search(self, nums: List[int], target: int) -> int:
        left = 0
        right = len(nums) - 1

        while left <= right:
            mid = (left + right) // 2
            if nums[mid] == target:
                return mid
            # left sorted portion
            if nums[left] <= nums[mid]:
                if target > nums[mid] or target < nums[left]:
                    left = mid + 1
                else:
                    right = mid - 1
            # right sorted portion
            else:
                if target < nums[mid] or target > nums[right]:
                    right = mid - 1
                else:
                    left = mid + 1
        return -1
```

### 34. Find First and Last Position of Element in Sorted Array

複雜度要求 O(log n) => binary search

* 個人直觀解法，時間複雜O(n)，但Accepted?
Runtime: 84 ms 
```
class Solution:
    def searchRange(self, nums: List[int], target: int) -> List[int]:
        res=[]
        flag=0
        index=0
        for i in range(len(nums)):
            if nums[i]==target and flag==0:
                res.append(i)
                flag+=1
                index=i
            elif nums[i]==target and flag>0:
                index=i
        if flag==0:
            return [-1,-1]
        else:
            res.append(index)
        return res
```

* binary search
Runtime: 84 ms

```
class Solution:
    def searchRange(self, nums: List[int], target: int) -> List[int]:
        l=0
        r=len(nums)-1
        while l<=r:
            mid=(l+r)//2
            if nums[mid]==target:
                l=r=mid
                while l>0 and nums[l-1]==target:
                    l-=1
                while r<len(nums)-1 and nums[r+1]==target:
                    r+=1
                return [l,r]
            elif nums[mid]<target:
                l=mid+1
            else:
                r=mid-1
        return [-1,-1]
```
### 54. Spiral Matrix

![](https://i.imgur.com/SYj8g6E.png)


```python=
class Solution:
    def spiralOrder(self, matrix: List[List[int]]) -> List[int]:
        if matrix == []: 
            return []
        top = 0  # 上
        bottom = len(matrix)-1  # 下
        left = 0  # 左
        right = len(matrix[0])-1  # 右
        res = []
        
        while top < bottom and left < right:
            res += matrix[top][left:right + 1]  # 輸出最上面數據
            for x in range(top + 1, bottom):  # 輸出最右邊數據
                res.append(matrix[x][right])
            res += matrix[bottom][left:right + 1][::-1]  # 輸出最下面數據
            for x in range(bottom - 1, top, -1):  # 輸出最左邊數據
                res.append(matrix[x][left])
            top, bottom, left, right = top + 1, bottom - 1, left + 1, right - 1  # 四個邊界，分别向内部移動一個位置
        if top == bottom:  # 如果最后剩下一行數據
            res += matrix[top][left:right + 1]
        elif left == right:  # 如果最后剩下一列數據
            for x in range(top, bottom + 1):
                res.append(matrix[x][right])
        return res
```


### 56. Merge Intervals

重疊區間合併問題
給定資料並非按照順序，所以先按照順序排列在一個個check 


```python=
class Solution:
    def merge(self, intervals: List[List[int]]) -> List[List[int]]:
        intervals=sorted(intervals)
        #print(intervals)
        res=[intervals[0]]
        for item in intervals[1:]:
            if(res[-1][1]>=item[0]):
                res[-1][1]=max(item[1],res[-1][1])
            else:
                res.append(item)
        return res
```

### 57. Insert Interval

給定一個已經排序好的 interval 序列, 要求插入新的 Interval 近來, 可以做到 時間複雜度 O(N)

* ++two pointer++
時間複查度: O(N)

#### 分為三種情況
1. ++newInterval[-1] < intervals[i][0]:++
代表新插入的這個 intervals 一定在當前比較序列的最左邊且不會有重疊的部分, 直接放入最左邊就回傳
![](https://i.imgur.com/sQaLJiL.png)

2. ++newInterval[0] > intervals[i][-1]:++
代表新插入的這個 intervals 一定在當前比較序列的最右邊且不會有重疊的部分
![](https://i.imgur.com/ww1JOxW.png)

3. ++others++
其餘狀況代表一定有重疊到的部分, 就要去比較左右值
![](https://i.imgur.com/loMJSWN.png)


```python!
class Solution:
    def insert(self, intervals: List[List[int]], newInterval: List[int]) -> List[List[int]]:
        res=[]
        for i in range(len(intervals)):
            if newInterval[-1] < intervals[i][0]:
                res.append(newInterval)
                return res + intervals[i:]
            elif newInterval[0] > intervals[i][-1]:
                res.append(intervals[i])
            else:
                newInterval = [min(newInterval[0], intervals[i][0]), max(newInterval[-1], intervals[i][-1])]
            
        res.append(newInterval)

        return res
```

* ++Easy method++
類似56題
插入後排序，再merge即可。
時間複雜度: O(NlogN)

```python=
class Solution:
    def insert(self, intervals: List[List[int]], newInterval: List[int]) -> List[List[int]]:
        intervals.append(newInterval)
        intervals=sorted(intervals)
        #print(intervals)
        res=[intervals[0]]
        for item in intervals[1:]:
            if res[-1][1]>=item[0]:
                res[-1][1]=max(res[-1][1],item[1])
            else:
                res.append(item)
        return res
```



### 66. Plus One


取出陣列最尾的元素加1
要多判斷是否需要進位的狀況

```python
# 2021/7/3
class Solution:
    def plusOne(self, digits: List[int]) -> List[int]:
        ln=len(digits)-1
        digits[ln]=digits[ln]+1
        while(ln):
            if digits[ln]==10:
                digits[ln]=0
                digits[ln-1]=digits[ln-1]+1
            ln-=1
        if digits[0]==10:
            digits[0]=0
            digits.insert(0,1)
        return digits
```



```python
# 2020/3/10
class Solution:
    def plusOne(self, digits):
        lenn=len(digits)
        for i in reversed(range(lenn)):
            if digits[i]==9:
                digits[i]=0
            else:
                digits[i]+=1
                return digits #jump for loop

        digits[0]=1
        digits.append(0)
        return digits
```

### 88. Merge Sorted Array

想法:
1.從nums1 後面放回來，不會有從前面放而重疊到的問題。

```
class Solution:
    def merge(self, nums1: List[int], m: int, nums2: List[int], n: int) -> None:
        """
        Do not return anything, modify nums1 in-place instead.
        """

        a,b=m-1,n-1
        if not nums2:
            return
        for i in range(m+n-1,0,-1):
            #print("i=",i)
            if nums1[a]>nums2[b]:
                nums1[i]=nums1[a]
                a-=1
            else:
                nums1[i]=nums2[b]
                b-=1
            if  a==-1 or b==-1:
                break
        #print(a,b)
        if b>-1:
            nums1[:b+1]=nums2[:b+1]
```

### 118. Pascal's Triangle

結尾跟開始為1
先用for決定有多少rows : for i in range(numRows):
再用另一個for loop所有需要generate的數字: for j in range(i+1):

如果j位置是是0與i也就是第一個與最後一個，append(1)

```python=
# 2021/7/4
class Solution:
    def generate(self, numRows: int) -> List[List[int]]:
        res=[]
        for i in range(numRows):
            res.append([])
            for j in range(i+1):
                if j==i or j==0:
                    res[i].append(1)
                else:
                    res[i].append(res[i-1][j-1] + res[i-1][j])
            #print(i,res)
        return res
```


### 119. Pascal's Triangle II

```python=
class Solution:
    def getRow(self, rowIndex: int) -> List[int]:
        res=[]
        for i in range(rowIndex+1):  
            res.append([])
            for j in range(i+1):  #i=2 j=0,1,2
                if j==0 or j==i:
                    res[i].append(1)
                else:
                    res[i].append(res[i-1][j-1]+res[i-1][j])
        #print(res)
        return res[rowIndex]
```


### 128. Longest Consecutive Sequence

![](https://i.imgur.com/LAnkdqF.png)

(key,value):(數字,值的長度)
建立一個字典，每次判斷左右兩邊是否值已存入字典中

```python=
class Solution:
    def longestConsecutive(self, nums: List[int]) -> int:
        dic={}
        maxval=0
        
        for num in nums:
            if num not in dic:
                left=dic.get(num-1,0) # not found return 0
                right=dic.get(num+1,0)
                ln=left+right+1
                
                maxval=max(maxval,ln)
                
                dic[num]=ln
                dic[num-left]=ln
                dic[num+right]=ln
        #print(dic)
        return maxval
```


### 136. Single Number

Given a non-empty array of integers nums, every element appears twice except for one. Find that single one.

利用邏輯運算中的 XOR(^) 性質: 自己XOR自己=0
==a ^ b ^ a = b==


```python
class Solution:
    def singleNumber(self, nums: List[int]) -> int:
        ans=0
        for i in nums:
            ans^=i
        return ans
```

### 137. Single Number II


![](https://i.imgur.com/skJ3rA8.png)


```
if res >= pow(2,31):
    res -= pow(2,32)
```

python 需要判斷正負數，因為他整數是無上限的。

```python
class Solution:
    def singleNumber(self, nums: List[int]) -> int:
        res=0
        for i in range(32):
            cnt=0
            mask = 1<<i
            for num in nums:
                if num & mask: # !=0
                    cnt+=1
            if cnt%3!=0:
                res|=mask
        if res >= pow(2,31):
            res -= pow(2,32)
        return res
```


解法二:
重點是字典中的key value要會用。
```
for key,val in dic.items():
```

```
class Solution:
    def singleNumber(self, nums: List[int]) -> int:
        dic={}
        for i in nums:
            if i in dic:
                dic[i]+=1
            else:
                dic[i]=1
        for key,val in dic.items():
            if val==1:
                return key
```

### 155. Min Stack

用python list 實作 stack

```
class MinStack:

    def __init__(self):
        """
        initialize your data structure here.
        """
        self.stack=[]
        nmin= float("inf")
        
    def push(self, x: int) -> None:
        self.stack.append(x)
        

    def pop(self) -> None:
        self.stack.pop()

    def top(self) -> int:
        return self.stack[-1]

    def getMin(self) -> int:
        return min(self.stack)
```

### 167. Two Sum II - Input array is sorted
![](https://i.imgur.com/BFdnvMC.png)


想法: 
用兩個for loop 去檢查是否和為target
![](https://i.imgur.com/cNsYN23.png)

結果:
Time Limit Exceeded， 測資大筆直接爆開

* 解法
用兩個數 i,j 去跑，因為array本身是一個以排序的陣列，
如果比target小，前面的往後跑；相反的，後面的往前跑。
**前提: sorted array**
```
int* twoSum(int* numbers, int numbersSize, int target, int* returnSize){
    int i,j=numbersSize-1,*arr;
    *returnSize=2;
    while(i<j)
    {
        if((numbers[i]+numbers[j]==target))
        {
            arr=(int *)calloc(2,sizeof(int *));
            arr[0]=i+1;
            arr[1]=j+1;
            break;
        }
        else if(numbers[i]+numbers[j]>target)
            j--;
        else
            i++;
        
    }
    return arr;
}
```

### 169. Majority Element

speed: 180 ms
* Ans1
```
# 2021.4.13
class Solution:
    def majorityElement(self, nums: List[int]) -> int:
        dic={}
        ln=len(nums)
        for i in nums:
            if i in dic:
                dic[i]+=1
            else:
                dic[i]=1
            
            if dic[i]>int(ln/2):
                return i
```

speed: 160 ms
* Ans 2

```
class Solution:
    def majorityElement(self, nums: List[int]) -> int:
        nc=collections.Counter(nums)
        ln=len(nums)
        for a,b in nc.items():
            if b>int(ln/2):
                return a
```

### 189. Rotate Array

![](https://i.imgur.com/yFcRBFM.png)

* Python Code
```
class Solution:
    def rotate(self, nums: List[int], k: int) -> None:

        lenn=int(len(nums))
        old_num=nums.copy()  ## old_num=nums[:] 
        for i in range (lenn):
            nums[(i+k)%lenn]=old_num[i]
        return nums
```


**位置給值才對** 因為今天是值要換位置

位置給值
nums[(i+k)%lenn]=old_num[i]
值給位置
old_num[i]=nums[(i+k)%lenn]


* C code

```
void rotate(int* nums, int numsSize, int k){
    int i=0,old[numsSize];
    for(i=0;i<numsSize;i++)
            old[i]=nums[i];

    for(i=0;i<numsSize;i++)
    {
        nums[(i+k)%numsSize]=old[i];
    }

}
```

### 209. Minimum Size Subarray Sum

[找一個最短數組大於等於target值](https://blog.csdn.net/danspace1/article/details/86661149)

```python=
class Solution:
    def minSubArrayLen(self, target: int, nums: List[int]) -> int:
        res=float('inf')
        if sum(nums)<target:
            return 0
        left,tot=0,0
        for i,item in enumerate(nums):
            tot+=item
            while tot>=target:
                res=min(res,i-left+1)
                tot-=nums[left]
                left+=1
        return res
```


### 217. Contains Duplicate

```
class Solution:
    def containsDuplicate(self, nums: List[int]) -> bool:
        dic={}
        for i in nums:
            if i in dic:
                return True
            else:
                dic[i]=1
                
        return False
```

### 219. Contains Duplicate II

大測資超時:Time Limit Exceeded
```
class Solution:
    def containsNearbyDuplicate(self, nums: List[int], k: int) -> bool:
        for i in range(len(nums)):
            temp=nums[i]
            if temp in nums[i+1:i+k+1]:
                #print(temp,nums[i:i+k+1])
                return True
        return False
```

使用字典判斷
```
class Solution:
    def containsNearbyDuplicate(self, nums: List[int], k: int) -> bool:
        dic={}
        for i,item in enumerate(nums):
            #print(i,item,dic)
            if item in dic:
                if i-dic[item]<=k:
                    return True
            dic[item]=i
```

### 260. Single Number III

[所有元素進行XOR操作後](https://dgideas.net/2020/leetcode-260-single-number-iii-solution/)，我們得到的不再是一個單個數字，而是這兩個數字的位異或結果：
如果這個結果中有的二進制位為1，則說明這兩個數字中該二進制位存在差異（即一個數的相應位置為0，另一個數的對應位置必為1）。

我們將存在差異的二進制位稱作第i位。
此時，我們將情景描述為：兩個所求元素在該二進制位中存在差異，一個對應位置為0，另一個等於1

因此，我們容易將所有數字依據該位值為0/1分為兩組：第一組包含第i位為0的所有元素；而第二組包含該位為1的所有元素。我們便很容易地將兩個所求之數區分開。

此時，對第一組所有元素進行位異或操作，即可得到第一個數。同樣地，對第二組所有元素進行位異或操作，即可得到第二個數。

```python
# 2021/6/29
class Solution:
    def singleNumber(self, nums: List[int]) -> List[int]:
        tmp=0
        num1,num2=0,0
        for num in nums:
            tmp^=num
        mask=1
        while (mask & tmp)==0: #find the first nozero bit in tmp
            mask=mask<<1 #set mask=nozero bit in tmp
        for num in nums:
            if num&mask==0:
                num1^=num
            else:
                num2^=num
        return [num1,num2]
```



解法二:

```python
# 2021/6/29
class Solution:
    def singleNumber(self, nums: List[int]) -> List[int]:
        dic={}
        for i in nums:
            if i in dic:
                dic[i]+=1
            else:
                dic[i]=1
        ans=[]
        for key,val in dic.items():
            if val==1:
                ans.append(key)
        return ans
```



### 349. Intersection of Two Arrays

```python
class Solution:
    def intersection(self, nums1: List[int], nums2: List[int]) -> List[int]:
        dic={}
        res=[]
        for i in nums1:
            if i in dic:
                dic[i]+=1
            else:
                dic[i]=1
        for i in nums2:
            if i in dic:
                res.append(i)
        #print(type(set(res)))
        return set(res)
```

### 350. Intersection of Two Arrays II

類似 leetcode 349

```python
class Solution:
    def intersect(self, nums1: List[int], nums2: List[int]) -> List[int]:
        dic={}
        res=[]
        for i in nums1:
            if i in dic:
                dic[i]+=1
            else:
                dic[i]=1
        for i in nums2:
            if i in dic and dic[i]>0:
                res.append(i)
                dic[i]-=1
                
        return res
```



### 414. Third Maximum Number

```python
class Solution:
    def thirdMax(self, nums: List[int]) -> int:
        ns=set(nums)
        ns=list(sorted(ns))
        #print(ns)
        if len(ns)>=3:
            return ns[-3]
        else:
            return max(ns)
```


### 485. Max Consecutive Ones

```python
# 2021/6/30
class Solution:
    def findMaxConsecutiveOnes(self, nums: List[int]) -> int:
        tmp,count=0,0
        for i in nums:
            if i==1:
                tmp+=1    
            else:
                count=max(count,tmp)
                tmp=0
        count=max(count,tmp)
        return count
```



### 665. Non-decreasing Array

只能換一次數字的非遞減數列，設定count=1，假設最後count<0，則代表換超過1次。
有兩種狀況的換法:
a. [2,5,3,4] i=1時，nums[i-1]<=nums[i+1]，所以nums[i]=nums[i+1]。 
b. [3,4,2,5] i=1時，nums[i-1]>nums[i+1]，所以nums[i+1]=nums[i]。

時間複雜度: O(n)

```python=
class Solution:
    def checkPossibility(self, nums: List[int]) -> bool:
        count=1
        for i in range(len(nums)-1):
            #print(i,count)
            if nums[i+1]<nums[i]:
                count-=1
                if i-1<0 or nums[i-1]<=nums[i+1]: #[2,5,3,4]  
                    nums[i]=nums[i+1]
                else: #[3,4,2,5]
                    nums[i+1]=nums[i]
        #print(count)
        if count<0:
            return False
        return True
```


### 674. Longest Continuous Increasing Subsequence

```
class Solution:
    def findLengthOfLCIS(self, nums: List[int]) -> int:
        le=len(nums)
        if le==0:
            return 0
        maxx,temp=1,1
        for i in range(le-1):
            if nums[i]<nums[i+1]:
                temp+=1
            else:
                maxx=max(maxx,temp)
                temp=1
        maxx=max(maxx,temp)   
        return maxx
```

### 724. Find Pivot Index

找到一個pivot 使得左邊的數組等於右邊的數組

![](https://i.imgur.com/48EE5ZL.png)


```python
# 2021.7.17
class Solution:
    def pivotIndex(self, nums: List[int]) -> int:
        left=0
        right=sum(nums)
        for ind,item in enumerate(nums):
            right-=item 
            if left==right:
                return ind
            left+=item
        return -1
```


### 896. Monotonic Array

![](https://i.imgur.com/K2hnyXR.png)

```
class Solution:
    def isMonotonic(self, A: List[int]) -> bool:
        lenn=len(A)
        up=1
        down=1
        for i in range(lenn-1):
            if A[i]>A[i+1]:
                down=0
            elif A[i]<A[i+1]:
                up=0
        return (up|down)
```


### 977. Squares of a Sorted Array

簡單的回傳squares 陣列，不用for loop 而用map做
```python=
class Solution:
    def sortedSquares(self, nums: List[int]) -> List[int]:
        def f(x):
            return x*x
        nums=map(f,nums)
        nums=sorted(nums)
        return nums
```

### 997. Find the Town Judge

本來想法建字典，後來卡在無法判斷扣除。 => 已解決
改用array，可以直接利用index去當數字。

* Array 法
```python=
class Solution:
    def findJudge(self, N: int, trust: List[List[int]]) -> int:
        dic={}
        trusted=[0]*(N+1)
        for a,b in trust:
            trusted[a]-=1
            trusted[b]+=1
        
        for i in range(1, N+1):
            if trusted[i] == N-1:
                return i
        return -1
```

* Dictionary 法
```python=
class Solution:
    def findJudge(self, N: int, trust: List[List[int]]) -> int:
        dic={}
        for a,b in trust:
            if a not in dic:
                dic[a]=-1
            else:
                dic[a]-=1
            if b not in dic:
                dic[b]=1
            else:
                dic[b]+=1  
        if not dic and N==1: #special case to N=1 & trust=[]
            return N
        for key,val in dic.items():
            if val==N-1:
                return key
        return -1
```

### 1004. Max Consecutive Ones III

[講解影片: LeetCode 1004. Max Consecutive Ones III ](https://www.youtube.com/watch?v=DsHaTbrlbD0&ab_channel=happygirlzt)

找 subarr 問題，用 sliding window or prefix sum

這裡的presum 存的是0的個數，當presum 超過 k 時，就要把最前面的0拿掉再去算最大長度，依此類推。

```python
while(lo<i and nums[lo]==1):
    lo+=1
```

這兩行的目的就是要找到最先出現的0 的 index，準備替換掉。


```python
class Solution:
    def longestOnes(self, nums: List[int], k: int) -> int:
        lo,hi=0,0
        presum=0
        res=0
        for i in range(len(nums)):
            if nums[i]==0:
                presum+=1
            if presum>k:
                while(lo<i and nums[lo]==1):
                    lo+=1 #find the first 0 poistion
                presum-=1
                lo+=1
            res=max(i-lo+1,res)  #res store their max subarr length 
        return res
```



### 1827. Minimum Operations to Make the Array Increasing

題目:
每次只能加1，找出最少刺術後可以形成嚴格遞增的數列。
想法:
假設出現nums[i]<=nums[i+1] 的時候，讓後面的數字成為前面+1就好。

```python=
class Solution:
    def minOperations(self, nums: List[int]) -> int:
        count=0
        for i in range(len(nums)-1):
            if nums[i+1]<=nums[i]:
                count+=nums[i]+1-nums[i+1]
                nums[i+1]=nums[i]+1
        #print(count,nums)
        return count
```



## List


### 11. Container With Most Water

找尋最大水量(面積)
想法:
1. 設兩個指標，最左與最右，保留大的並往中間移動
2. 每次保留最佳面積



```
class Solution:
    def maxArea(self, height: List[int]) -> int:
        ll=len(height)
        left=0
        right=ll-1
        temp=0
        result=0
        #print(height[left])
        while left<right:
            if height[left]<height[right]:
                temp=(height[left])*(right-left)
                left+=1
                result=max(result,temp)
            else:
                temp=(height[right])*(right-left)
                right-=1
                result=max(result,temp)
            print(left,right,result)
        return result
```


### 42. Trapping Rain Water

類似leetcode.11
1. 設兩個指標，最左與最右，保留大的 且小的往中間移動
2. 每次儲存高度差
3. 遇到比較大的，設成新指標



```
class Solution:
    def trap(self, height: List[int]) -> int:
        lle=len(height)
        left=0
        right=lle-1
        temp=0
        while(left<right):
            mn=min(height[left],height[right])
            if (mn==height[left]):
                left+=1
                while(height[left]<mn):
                    temp+=mn-height[left]
                    left+=1
            else:
                right-=1
                while(height[right]<mn):
                    temp+=mn-height[right]
                    right-=1
                
                
        return temp
```




## Recursive

### 39. Combination Sum

[LeetCode 39. Combination Sum【公瑾讲解】
](https://www.youtube.com/watch?v=HdS5dOaz-mk&ab_channel=YuZhou)

題目:
candidates 內的數字可以重複使用，把可以組成target和的數字存到列表中返回。

遞迴解法:
自己要寫一個遞迴函式
```
def dfs(candidates,temp,rem,index):
```
temp:暫時儲存的數組
rem: 剩下的target目標值
```
for i in range(index,len(candidates)):
```
index: 為了不重複，所以 for從 index開始 



```
class Solution:
    def combinationSum(self, candidates: List[int], target: int) -> List[List[int]]:
        self.res=[]
        
        def dfs(candidates,temp,rem,index):
            # Condition
            if rem==0:
                self.res.append(temp[:])
                return
            if rem<0:
                return
            
            for i in range(index,len(candidates)):
                temp.append(candidates[i])
                #print(i,temp)
                dfs(candidates,temp,rem-candidates[i],i)
                temp.pop()
        
        dfs(candidates,[],target,0)
        return self.res
```


### 46. Permutations

排列

![](https://i.imgur.com/NRSYEiu.png)

[LeetCode 46 Permutations 【公瑾讲解】](https://www.youtube.com/watch?v=oCGMwvKUQ_I&ab_channel=YuZhou)

* Backtracking 法
```python=
class Solution:
    def permute(self, nums: List[int]) -> List[List[int]]:
        res=[]
        def dfs(nums,temp):
            if len(temp)==len(nums):
                res.append(temp[:])
            for i in range(len(nums)): # every level is total length
                if nums[i] in temp:
                    continue #end this for loop once
                temp.append(nums[i])
                dfs(nums,temp)
                temp.pop()
        dfs(nums,[])
        return res
```


```python=
# 2021/7/7
class Solution:
    def permute(self, nums: List[int]) -> List[List[int]]:
        res=[]
        used=[False]*len(nums)
        def dfs(nums,tmp):
            if len(tmp)==len(nums):
                res.append(tmp[:])
            for i in range(len(nums)):
                if used[i]:
                    continue
                tmp.append(nums[i])
                used[i]=True
                dfs(nums,tmp)
                tmp.pop()
                used[i]=False
        dfs(nums,[])
        return res
```

### 47. Permutations II

[跟 leetcode.46 差在去重](https://www.youtube.com/watch?v=imLl2s9Ujis)
```python
if i>0 and nums[i-1]==nums[i] and used[i-1]:
    continue
```

```python=
class Solution:
    def permuteUnique(self, nums: List[int]) -> List[List[int]]:
        res=[]
        used=[False]*len(nums)
        def dfs(nums,tmp):
            if len(tmp)==len(nums):
                res.append(tmp[:])
            for i in range(len(nums)):
                if used[i]:
                    continue
                if i>0 and nums[i-1]==nums[i] and used[i-1]:
                    continue
                tmp.append(nums[i])
                used[i]=True
                dfs(nums,tmp)
                tmp.pop()
                used[i]=False
        dfs(sorted(nums),[])
        return res
```


### 77. Combinations

![](https://i.imgur.com/zufgIww.png)

給一個n和k，找所有1-n 的k個組合。

* 不重複
![](https://i.imgur.com/vTxGPbQ.png)



* backtracking 法
```python=
class Solution:
    def combine(self, n: int, k: int) -> List[List[int]]:
        res=[]
        def dfs(start,comb):
            if len(comb)==k:
                res.append(comb[:])
            for i in range(start, n+1):
                comb.append(i)
                dfs(i+1,comb)
                comb.pop()
        dfs(1,[])
        return res
```
[Ref Vedio](https://www.youtube.com/watch?v=q0s6m7AiM7o&ab_channel=NeetCode)
![](https://i.imgur.com/eoum0jT.png)



* stack 解法
```
n-x+1<k-1
```

一個組合會有 k 個數字，現在我們有 l 個數字，所以我們還需要 k-l 個數字。
我們插入stack 是採用ascending order。
從 x 到 n ， 有 n-x+1 個數字大於stack 現有的數字。(因為stack不包含x 所以要+1) 

所以如果需要的數字數量 k-l < 能夠給的數字量 n-x+1 ， 就要停止。

```python=
class Solution:
    def combine(self, n: int, k: int) -> list:
        res = []
        stack=[]
        x=1
        while True:
            l=len(stack)
            if l==k:
                res.append(stack[:])
            elif l==k or x>n-k+l+1:
                if not stack:
                    return res
                x=stack.pop()
            else:
                stack.append(x)
                x+=1
```

* other
```
class Solution:
    def combine(self, n: int, k: int) -> list:
        res = []
        def dfs(start, out):
            if len(out) == k:
                res.append(list(out))
                return
            #print(start,out,k)
            for i in range(start, n + 1):
                if k - len(out) - 1 > (n - i):
                    break
                out.append(i)
                dfs(i + 1, out)
                out.pop()
        dfs(1, [])
        return res
```




### 78. Subsets


題目:
```
Input: nums = [1,2,3]
Output: [[],[1],[2],[1,2],[3],[1,3],[2,3],[1,2,3]]
```

![](https://i.imgur.com/yTpPt37.png)

按照圖片的思路
從 [] 開始，
首先[1]，再增加為[1,2]，再增加為[1,2,3]
走不下去後，pop 回 [1,2]，再到 [1,3]....

[LeetCode 78. Subsets 【公瑾讲解】](https://www.youtube.com/watch?v=Az3PfUep7gk&ab_channel=YuZhou)

* Backtracking
```
class Solution:
    def subsets(self, nums: List[int]) -> List[List[int]]:
        res=[]
        def dfs(nums,temp,index):
            res.append(temp[:])
            for i in range(index,len(nums)):
                temp.append(nums[i])
                dfs(nums,temp,i+1)
                temp.pop()
        dfs(nums,[],0)
        return res
```









## Link List

### 2. Add Two Numbers

```python=
#2021.7.28
class Solution:
    def addTwoNumbers(self, l1: ListNode, l2: ListNode) -> ListNode:
        new=ListNode(0)
        cur=new
        carry=0
        while l1 or l2 or carry:
            x=l1.val if l1 else 0
            y=l2.val if l2 else 0
                
            carry,val=divmod(x+y+carry,10)   #retrun (商,餘數)
            cur.next=ListNode(val)
            cur=cur.next
            
            l1=l1.next if l1 else None
            l2=l2.next if l2 else None
            
        return new.next
```


### 19. Remove Nth Node From End of List

* error 測資[1] n=1 時錯
```
# Error code
class Solution:
    def removeNthFromEnd(self, head: ListNode, n: int) -> ListNode:
        count=0
        temp=0
        current=head
        while current:
            count+=1
            current=current.next

        goal=count-n-1
        print(goal)
        head_node=head
        while head:
            if temp==goal:
                head.next=head.next.next
            else:
                head=head.next
            temp+=1
        #rint(head_node)
        return head_node
```

[利用的是fast和slow雙指標來解決。首先先讓fast從起始點往後跑n步。然後再讓slow和fast一起跑，直到fast.next為null時候，slow所指向的node就是需要刪除節點的前面的節點。](https://www.itread01.com/content/1547625272.html)


```python=
# 2021.3.28
class Solution(object):
    def removeNthFromEnd(self, head, n):
        fast=slow=head
        for i in range(n):
            fast=fast.next
        if not fast: #delete first node
            return head.next
        while fast.next:
            fast=fast.next
            slow=slow.next
            print("a")
        slow.next=slow.next.next
        
        return head
```

如果測資是 [1,2,3,4,5] n=2
第9行
```
while(fast):  印出aaa,多跑一次
while(fast.next) 印出aa
```


### 21. Merge Two Sorted Lists


ex: l1=[1,2,4]
ListNode{ val: 1, next: ListNode{val: 2, next: ListNode{val: 4, next: None}}}

```python
class Solution:
    def mergeTwoLists(self, l1: ListNode, l2: ListNode) -> ListNode:
        head=ListNode(0) #creat new listnode [ListNode{val: 0, next: None}]
        cu=head
        print(cu)
        while(l1 and l2):
            if l1.val<l2.val:
                cu.next=l1
                l1=l1.next
            else:
                cu.next=l2
                l2=l2.next
            cu=cu.next
        cu.next=l1 or l2 # the last of l1 orl2

        return head.next
```

### 24. Swap Nodes in Pairs

```python=
class Solution:
    def swapPairs(self, head: ListNode) -> ListNode:
        new=ListNode(0)
        first=new
        if not head or not head.next:
            return head
        while head and head.next:
            tmp=head.next
            head.next=tmp.next
            tmp.next=head
            first.next=tmp
            first=head
            head=head.next
        return new.next
```

下解可套用在不只兩兩交換上，k個也可以。

```python=
class Solution:
    def swapPairs(self, head: ListNode) -> ListNode:
        new=ListNode(0)
        new.next=head
        pre=new
        cur=head
        cnt=0
        while cur:
            cnt+=1
            if cnt%2==0:
                pre=self.reverse(pre,cur.next)
                cur=pre.next
            else:
                cur=cur.next
        return new.next
    
    def reverse(self,start,end):
        pre=start.next
        cur=pre.next
        while cur is not end:
            tmp=cur.next
            cur.next=start.next
            start.next=cur
            cur=tmp
        pre.next=end
        return pre
```

### 25.Reverse Nodes in k-Group

寫一個reverse 函式 

![](https://i.imgur.com/EdNNNqU.jpg)


```python=
class Solution:
    def reverseKGroup(self, head: ListNode, k: int) -> ListNode:
        new=ListNode(0)
        new.next=head
        pre=new
        cur=head
        cnt=0
        while cur is not None:
            cnt+=1
            if cnt%k==0:
                pre=self.reverse(pre,cur.next)
                cur=pre.next
            else:
                cur=cur.next
        return new.next
    
    def reverse(self,start,end):
        pre=start.next
        cur=pre.next
        while cur is not end:
            tmp=cur.next
            cur.next=start.next
            start.next=cur
            cur=tmp
        pre.next=end
        return pre
```

### 83. Remove Duplicates from Sorted List

想法:
1.Create a new Listnode
2.If not exist, put into new Listnode

```python=
class Solution:
    def deleteDuplicates(self, head: ListNode) -> ListNode:
        li=[]
        new=ListNode(0)
        cu=new
        while(head):
            if head.val not in li:
                li.append(head.val)
                cu.next=head
                cu=cu.next
            head=head.next
            #print(new)
        cu.next=head #final node
        return new.next
```

### 92. Reverse Linked List II

先找出 start 跟 end 位置，在做交換。
交換參考 leetcode.25 的reverse function

```python=
class Solution:
    def reverseBetween(self, head: ListNode, left: int, right: int) -> ListNode:
        new=ListNode(0)
        new.next=head
        p=new
        for i in range(left-1):
            p=p.next
        nleft=p
        for i in range(right-left+1):
            p=p.next
        nright=p.next
        print(nleft,nright)
        
        pre=nleft.next
        cur=pre.next
        while cur is not nright:
            tmp=cur.next
            cur.next=nleft.next
            nleft.next=cur
            cur=tmp
        pre.next=nright
        
        return new.next
```

### 141. Linked List Cycle
![](https://i.imgur.com/D9hWmOk.png)
題目: 檢查 link list是否有cycle
解法: 用一個快指針，一個慢指針，若有環最後一定會相遇。

* C
```c 2021.5.17
# define TRUE 1
# define FALSE 0
bool hasCycle(struct ListNode *head) {
    struct ListNode *slow,*first;
    slow=head;
    first=head;
    while(first && first->next)
    {
        first=first->next->next;
        slow=slow->next;
        if(slow==first)
            return TRUE;
    }
    return FALSE;
}
```

* Python 

```python
#2021.4.8
class Solution:
    def hasCycle(self, head: ListNode) -> bool:
        slow=fast=head
        if not head:
            return False
        while(fast and fast.next):
            slow=slow.next
            fast=fast.next.next
            if fast==slow:
                return True
        return False
```

### 142. Linked List Cycle II


[想法參考](https://ithelp.ithome.com.tw/articles/10223721)

![](https://i.imgur.com/D1nFgrE.png)

快指針走的距離: a+b+c+b
慢指針走的距離: a+b

快指針是慢指針的兩倍速度，所以 2(a+b)=a+2b+c，得a=c

所以從快慢指針相遇的點x，與從頭開始走的指針，一次走一步相遇的點就是環的起點。

```python
#2021.4.8
class Solution:
    def detectCycle(self, head: ListNode) -> ListNode:
        slow=fast=head

        while(fast and fast.next):
            slow=slow.next
            fast=fast.next.next

            if slow==fast:
                cu=head
                while (cu.next and slow.next): #while(cu)
                    if cu==slow:
                        return cu
                    cu=cu.next
                    slow=slow.next
```

### 148. Sort List


ans1: bubble sort

error: 超時

```python
# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def sortList(self, head: ListNode) -> ListNode:
        cnt=head
        ln=0
        while(cnt):
            cnt=cnt.next
            ln+=1
        #print(ln)
        
        for i in range(ln):
            #print(i)
            cur=head
            nx=cur.next
            pre=None
            while nx:
                if cur.val>nx.val:
                    if not pre: #cur is head
                        pre=cur.next
                        nx=nx.next
                        pre.next=cur
                        cur.next=nx
                        head=pre
                    else:
                        tmp=nx
                        nx=nx.next
                        pre.next=cur.next
                        pre=tmp
                        tmp.next=cur
                        cur.next=nx
                else:
                    pre=cur
                    cur=nx
                    nx=nx.next
        
        return head
```


ans2 merge sort

```python
class Solution:
    def sortList(self, head: ListNode) -> ListNode:
        def merge(l1,l2):
            new=ListNode(0)
            cu=new
            while(l1 and l2):
                if l1.val<l2.val:
                    cu.next=l1
                    l1=l1.next
                else:
                    cu.next=l2
                    l2=l2.next
                cu=cu.next
            cu.next=l1 or l2
            return new.next
        
        if not head or not head.next:
            return head
        slow=head
        fast=head.next
        while(fast and fast.next):
            fast=fast.next.next
            slow=slow.next
        mid=slow.next # set l2 head=mid
        slow.next=None #split l1

        return merge(self.sortList(head),self.sortList(mid)) 
```


### 160. Intersection of Two Linked Lists

找兩條linklist 相交的那點，如果沒有返回 None
想法:
設一個指標在headA 與 headB 頭部，開始往後走，如果走到 None 則換另一條走，
最後當 p1==p2 時，就是相交的那點 (步數一樣)


```
x+z+y = y+z+x
```

![](https://i.imgur.com/Be7FlNg.png)


```python
class Solution:
    def getIntersectionNode(self, headA: ListNode, headB: ListNode) -> ListNode:
        cur1=headA
        cur2=headB
        while(cur1!=cur2):
            if cur1==None:
                cur1=headB
            else:
                cur1=cur1.next
            if cur2==None:
                cur2=headA
            else:
                cur2=cur2.next
        return cur1
```


### 203. Remove Linked List Elements

刪除指定數值的val 在 Linklist 中

```c
struct ListNode* removeElements(struct ListNode* head, int val){
    struct ListNode *cur;
    cur=head;
    while(cur && cur->next)
    {
        if (cur->next->val==val)
        {
            cur->next=cur->next->next;
        }
        else
            cur=cur->next;
    }
    if (head)
        return head->val==val?head->next:head;
    else
        return head;
}
```

### 206. Reverse Linked List


```python
class Solution:
    def reverseList(self, head: ListNode) -> ListNode:
        q=None
        p=head
        while(p):
            r=q
            q=p
            p=p.next
            q.next=r
        head=q

        return q
```

```
# stored in list and reinsert
class Solution:
    def reverseList(self, head: ListNode) -> ListNode:
        li=[]
        new=cur=head
        ind=0
        while cur:
            li.append(cur.val)
            cur=cur.next
            
        li.reverse()
        while new:
            new.val=li[ind]
            ind+=1
            new=new.next
        return head
```


![](https://i.imgur.com/ZDUbFtR.png)


* C Lanuguage



```c
# 2021.5.17
struct ListNode* reverseList(struct ListNode* head){
    struct ListNode *p,*q,*r;
    q=NULL;
    p=head;
    while(p)
    {
        r=q;
        q=p;
        p=p->next;
        q->next=r;
    }
    head=q;
    return  head;
}
```







```
struct ListNode* reverseList(struct ListNode* head){
    struct ListNode *tmp,*curr,*prev;
    curr=head;
    prev=NULL;
    while(curr!=NULL)
    {
        tmp=curr->next;
        curr->next=prev;
        prev=curr;
        curr=tmp;
    }
    head=prev;
    
    return head;
}
```

### 234. Palindrome Linked List

#### Ans1: 用array

不符合 Space: O(1) 要求

```python
class Solution:
    def isPalindrome(self, head: ListNode) -> bool:
        temp_list=[]
        while head:
            temp_list.append(head.val)
            head=head.next
            
        length=len(temp_list)
        
        for i in range(0,int(length/2)):
            if temp_list[i]!=temp_list[length-i-1]:
                return False
        return True
```

Insert linknode.data to list
list(0~length/2) check


```c
#2021.5.18
# define TRUE 1
# define FALSE 0
bool isPalindrome(struct ListNode* head){
    int res[1000000],len=0,i=0;
    while(head)
    {
        res[i]=head->val;
        i+=1;
        head=head->next;
    }
    len=i;
    for(int i=0;i<len/2;i++)
    {
        if(res[i]!=res[len-i-1])
            return FALSE;
    }
    return TRUE;
}
```



#### Ans2:

利用 fast 走兩格， slow 走一個的方式，最終 slow 會走到 LinkedList 的一半，然後在往後做翻轉，去比對 head ，如果相同就是回文。

```python=
class Solution:
    def isPalindrome(self, head: ListNode) -> bool:
        fast=slow=head
        while(fast and fast.next):
            slow=slow.next
            fast=fast.next.next
            
        cur=slow
        pre=None
        while(cur):
            tmp=cur.next
            cur.next=pre
            pre=cur
            cur=tmp
        tmp_head=head
        #print(tmp_head,head)
        
        while(tmp_head and tmp_head!=slow and pre):
            if tmp_head.val != pre.val:
                return False
            tmp_head = tmp_head.next
            pre = pre.next
        return True
```

### 237. Delete Node in a Linked List

一般刪除一個節點是通過上一個節點來操作，現在只給了**當前節點**，那麼只能將後一節點的值賦給當前節點，將後一節點刪掉，則相當於刪掉了“當前節點” 。

```
class Solution:
    def deleteNode(self, node):
        """
        :type node: ListNode
        :rtype: void Do not return anything, modify node in-place instead.
        """
        #print(node.next)
        node.val=node.next.val
        node.next=node.next.next
```

###  707. Design Linked List

設計一個 linklist

```python
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class MyLinkedList:

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.head = None
        self.count = 0
        

    def get(self, index: int) -> int:
        """
        Get the value of the index-th node in the linked list. If the index is invalid, return -1.
        """
        if index >= self.count or index < 0:
            return -1
        curr = self.head
        while index!=0:
            curr = curr.next           
            index-=1
        return curr.val
        

    def addAtHead(self, val: int) -> None:
        """
        Add a node of value val before the first element of the linked list. After the insertion, the new node will be the first node of the linked list.
        """
        if not self.head:
            self.head = ListNode(val)
        else:
            new_node = ListNode(val)
            new_node.next = self.head
            self.head = new_node
        self.count+=1
                
            
        

    def addAtTail(self, val: int) -> None:
        """
        Append a node of value val to the last element of the linked list.
        """
        if self.count == 0:
            self.head = ListNode(val)  # empty list
        else:
            curr = self.head
            while curr.next:
                curr = curr.next
            curr.next = ListNode(val)
        self.count+=1
         

    def addAtIndex(self, index: int, val: int) -> None:
        """
        Add a node of value val before the index-th node in the linked list. If index equals to the length of linked list, the node will be appended to the end of linked list. If index is greater than the length, the node will not be inserted.
        """
        #self.printer() 
        if index == 0:
            return self.addAtHead(val)
        #elif index == self.count:
        #    return self.addAtTail(val)
        elif index > self.count or index < 0:
            return
        else:
            curr = self.head
            new_node = ListNode(val)
            for _ in range(index-1):
                curr = curr.next
            new_node.next = curr.next
            curr.next = new_node
            self.count+=1
        
            
    def deleteAtIndex(self, index: int) -> None:
        """
        Delete the index-th node in the linked list, if the index is valid.
        """
        if self.count == 1 and index == 0: # only one node in total linklist
            self.head = None
            self.count-=1
            return
            
        elif index == 0: #delete head in noempty linklist
            self.head = self.head.next

        elif index+1 > self.count or index < 0:
            return
        else:
            curr = self.head
            for _ in range(index-1):
                curr = curr.next
            curr.next = curr.next.next
        self.count-=1
        #self.printer() 
    
    def printer(self):
        curr = self.head
        while curr:
            print(curr.val,' ')
            curr = curr.next
        


# Your MyLinkedList object will be instantiated and called as such:
# obj = MyLinkedList()
# param_1 = obj.get(index)
# obj.addAtHead(val)
# obj.addAtTail(val)
# obj.addAtIndex(index,val)
# obj.deleteAtIndex(index)
```


### 876. Middle of the Linked List
![](https://i.imgur.com/ntQ4jwk.png)


題目: 回傳一個 linklist，是從原本Linklist中間點開始的。
解法: 設定一快一慢指針，快的走兩步，慢的走一步，當快的到最後一個點時，慢的剛好到中間。

* C code
```c
struct ListNode* middleNode(struct ListNode* head){
    struct ListNode *fast,*slow;
    fast=head;
    slow=head;
    while(fast!=NULL && fast->next!=NULL)
    {
        fast=fast->next->next;
        slow=slow->next;
    }
    return slow;
    
}
```



## String

### 3. Longest Substring Without Repeating Characters

題目: 無重複字串的最長子串

想法:
1. 設定空字串substr
2. 如果字元x不存在於空字串，把x加入到substr
3. 反之x如果存在，substr=substr[substr.index[x]+1:] ，從該重複字元位置的後一位開始為substr
4. End


==Sliding Window Algorithm==

```
#2021.5.4
class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        subs=''
        maxlen=0
        for i in range(len(s)):
            if s[i] not in subs:
                subs+=s[i]
                maxlen=max(maxlen,len(subs))
            else:
                loc=subs.index(s[i])
                subs=subs[loc+1:]
                subs+=s[i]
        return maxlen  
```



---

```
#2020.4.18
def sol(s):
    len_max = 0
    start = 0
    sub_string = ''
    print(type(sub_string))
    for end in range(len(s)):
        if s[end] not in sub_string:
            sub_string += s[end]
        else:
            len_max = max(len_max, len(sub_string))
            while s[start] != s[end]:
                start += 1   #first repeat word location
            start += 1   #second repeat word location
            sub_string = s[start : end + 1] 
            print("sub_string: ",sub_string)
    return max(len_max, len(sub_string))

```
想法:
1. while s[start]!=s[end]:  start+=1 這行
設定好後的start意思是 first repeat word location 

    start += 1 意思是second repeat word location

2. sub_string = s[start : end + 1] 這行
reset the substring(include second repeat word, it become first word )

    重新設置子序列，並把重複的字當成第一個word




### 5. Longest Palindromic Substring

2020.3.22
[ Manacher’s Algorithm 厲害解法](http://gitlinux.net/2019-08-26-(5)-longest-palindromic-substring/)


想法1: 暴力法直接判斷有無Palindrome，結果Time Limit Exceeded
Time: O(n^3)
```
class Solution:
    def longestPalindrome(self, s: str) -> str:
        def isp(strr):
            ls=len(strr)
            for i in range(int(ls/2)):
                if strr[i]!=strr[ls-i-1]:
                    return False
            else:
                return True
            
        sub=''
        opt=''
        sl=len(s)
        maxlen=0
        for i in range(sl):
            for j in range(i,sl+1):
                sub=s[i:j]
                #print(sub)
                if isp(sub):
                    if len(sub)>maxlen:
                        maxlen=len(sub)
                        opt=sub
                    #maxlen=max(maxlen,len(sub))
        
        return opt
```

想法2: Greedy
Time: O(n^2)
1. 跑一次loop，判斷每個字元左右兩邊是否相等。
2. 如果有一樣字元，繼續往左右看，直到 left<0 or right>total_len 停止。
3. 找最大的substring

```
class Solution:
    def longestPalindrome(self, s: str) -> str:
        def getmax(st,l,r):
            res=''
            n=len(st)
            while l>=0 and r<n and st[l]==st[r]:
                res=st[l:r+1]  
                l-=1
                r+=1
            if res=='':
                return st[0]
            return res
        
        ir=''
        dr=''
        opt=''
        bigger=''
        if len(s)==1:
            return s[0]
        for i in range(0,len(s)):
            ir=getmax(s,i-1,i+1)
            dr=getmax(s,i,i+1)
            #print("ir",ir,i)
            if len(ir)>len(dr):
                bigger=ir
            else:
                bigger=dr
            if len(bigger)>len(opt):
                opt=bigger
            
        return opt  
```


想法3: DP
Time: O(n^2)

[[LeetCode]5. Longest Palindromic Substring](https://www.youtube.com/watch?v=ZnzvU03HtYk&ab_channel=%E5%B1%B1%E6%99%AF%E5%9F%8E%E4%B8%80%E5%A7%90)

1. 對角線設成 True
2. loop 由左上往右下掃
3. 假設s[i]=s[j]，有兩種情況
    a. 一種i+1==j，代表有相連的字串 (ex:bb)
    b. 另一種檢查 dp[i+1][j-1] 是否是回文，如果是那整串皆是。
![](https://i.imgur.com/QCdhYGh.png)


```
class Solution:
    def longestPalindrome(self, s: str) -> str:
        dp=[[False for i in range(len(s))] for i in range(len(s))]
        sub=''
        tem=''
        opt=''
        maxlen=0
        if len(set(s))==1:
            return s
        for j in range(0,len(s)):
            for i in range(0,j+1):
                if i==j:
                    dp[i][j]=True
                elif s[i]==s[j] and i+1==j: #bbc
                    dp[i][j]=True
                    sub=s[i:j+1]
                    if maxlen<len(sub):
                        maxlen=len(sub)
                        opt=sub
                elif s[i]==s[j] and dp[i+1][j-1]==True: #aba  
                    dp[i][j]=True   
                    sub=s[i:j+1]
                    if maxlen<len(sub):
                        maxlen=len(sub)
                        opt=sub
        if opt=='':
            return s[0]         
        return opt
```


想法4: Manacher’s Algorithm  
Time: O(n)





### 9. Palindrome Number

1. 負號也算string長度 (-121 長度為4)
2. 比對項目為 [ i : length-i-1 ] (需要減1是因為index start=0)

```python
class Solution:
    def isPalindrome(self, x: int) -> bool:
        sx=str(x)
        le=len(sx)
        for i in range(0,int(le/2)):
            if (sx[i]!=sx[le-i-1]):
                return False
        return True
```


```c
bool isPalindrome(int x){
    char *str=malloc(sizeof(char)*10); //int 值最大不超過10位數
    //char *str;
    int i=0,tmp,len;
    if (x<0)
        return 0;
    while(x!=0)
    {
        tmp=x%10;
        str[i]=tmp;
        x/=10;
        i+=1;
    }
    len=i;
    for(int j=0;j<len/2;j++)
    {
        if (str[j]!=str[len-j-1])
            return 0;
    }
    return 1;
    
}
```



### 13. Roman to Integer
![](https://i.imgur.com/l1OlCWL.png)


主要是觀察規律
```
dic={'I':1,'V':5,'X':10,'L':50,'C':100,'D':500,'M':1000}
```

1. 左邊小於右邊，要減掉左邊的數字
2. 左邊大於等於右邊，加上左邊的數字
3. 最後加上尾端羅馬數字 


```
# 2021.4.11
class Solution:
    def romanToInt(self, s: str) -> int:
        dic={'I':1,'V':5,'X':10,'L':50,'C':100,'D':500,'M':1000}
        count=0
        le=len(s)
        for i in range(le-1): #0~2
            if dic[s[i]]<dic[s[i+1]]:
                count-=dic[s[i]]
            else:    
                count+=dic[s[i]]
            #print(dic[i])
        count+=dic[s[le-1]]
        
        return count
```

### 20. Valid Parentheses

同 codility lession_7 Brackets
```
# 2021.4.12
class Solution:
    def isValid(self, s: str) -> bool:
        a=[]
        for i in s:
            if i=='(' or i=='[' or i=='{':
                a.append(i)
            else:
                if len(a)==0:
                    return False
                if i==')' and a[-1]=='(':
                    a.pop()
                elif i==']' and a[-1]=='[':
                    a.pop()
                elif i=='}' and a[-1]=='{':
                    a.pop()
                else:
                    return False
        print(a)
        if len(a)==0:
            return True
        else:
            return False
```



### 28. Implement strStr()

```python
# 2021.4.11
class Solution:
    def strStr(self, haystack: str, needle: str) -> int:
        nl=len(needle)
        sl=len(haystack)

        for i in range(sl-nl+1):
            #print(haystack[i:nl+i])
            if needle==haystack[i:nl+i]:
                return i        
        
        return -1
```


[別人解答](https://blog.csdn.net/fuxuemingzhu/article/details/79254558)
```python
class Solution:
    def strStr(self, haystack, needle):
        """
        :type haystack: str
        :type needle: str
        :rtype: int
        """
        M, N = len(haystack), len(needle)
        for i in range(M - N + 1):
            if haystack[i : i + N] == needle:
                return i
        return -1
```

* C

```c
int strStr(char * haystack, char * needle){
    if (needle==NULL) 
        return 0;
    int m = strlen(haystack), n = strlen(needle);
    int i=0,j=0;
    for (i=0;i<=m;i++)
    {
        for (j=0;j<n;j++)
        {
            if (haystack[i+j]!=needle[j])
                break;
        }
        if (j==n)
            return i;
    }
    return -1;
}
```



### 58. Length of Last Word

for loop 從後面往前跑
```python
#2021.4.12
class Solution:
    def lengthOfLastWord(self, s: str) -> int:
        le=len(s)
        temp,count=0,0
        for i in range(le-1,-1,-1):
            if s[i]==" ":
                if count==0:
                    continue
                else:
                    break
            else:
                count+=1

        return count
```

* c
```c
#2021.5.19
int lengthOfLastWord(char * s){
    int len=strlen(s),count=0;
    for(int i=len-1;i>=0;i--)
    {
        if (s[i]==' ')
        {
            if(count==0)
                continue;
            else
                break;
        }
        else
            count+=1;
    }
    return count;
}
```


### 67. Add Binary


bin(): 會返回0bxxx 形式
```
binary = bin(16)
print(binary)
=> 0b10000
```

```
class Solution:
    def addBinary(self, a: str, b: str) -> str:
        result=''
        inta=int(a,2)
        intb=int(b,2)
        su=inta+intb
        result=bin(su)
        
        return result[2:] 
```


### 125. Valid Palindrome

```
# 2021.4.12
class Solution:
    def isPalindrome(self, s: str) -> bool:
        temp=''
        s=s.lower()
        #print(s)
        for i in s:
            if ord(i)<=122 and ord(i)>=97 : #a~z
                temp+=i
            elif ord(i)<=57 and ord(i)>=48 :  #0-9
                temp+=i
        #print(temp)     
        le=len(temp)
        for i in range(int(le/2)):
            if temp[i]!=temp[le-i-1]:
                return False
        return True
```



### 151. Reverse Words in a String

```python
class Solution:
    def reverseWords(self, s: str) -> str:
        rr=s.split()
        rr.reverse()
        st= " ".join(rr)
        return st
```

[Python 字串(str)和列表(list)的互相轉換](https://www.itread01.com/content/1541809472.html)
[Python List reverse()方法](https://www.runoob.com/python/att-list-reverse.html)

* C
[REF:參考這個](https://leetcode-cn.com/problems/reverse-words-in-a-string/solution/fan-zhuan-zi-fu-chuan-li-de-dan-ci-by-leetcode-sol/)


還是ERROR，最後空格尚未解決。

```c

void swapc(char *a,char *b)
{
    char tmp=*a;
    *a=*b;
    *b=tmp;
}


char * reverseWords(char * s){
    int i,len;
    len=strlen(s);
    for(i=0;i<len/2;i++)
        swapc(&s[i],&s[len-i-1]);
    
    
    int n=len,start,idx=0;
    for(start=0;start<n;++start) {
        if (s[start] != ' ') {
            if (idx != 0) //not first substring, need to add space
                s[idx++] = ' ';
            int end = start;
            while(end<n && s[end] != ' ')
                s[idx++] = s[end++];
            /**
            for(i=start;i<=end;i++)
                printf("%c ",s[i]);
            printf("\n");
            printf("start: %d end:%d \n ",start,end);
            **/
            for(i=0;i<(end-start)/2;i++) //substr len is (end-start)
            {
                //printf("s[start]:%c ,s[end-i-1]:%c \n",s[start],s[end-i-1]);
                swapc(&s[start+i],&s[end-i-1]);
            }
            start = end; //改變下標，找下一個substring
        }
    }
    
    return s;
}
```

### 171. Excel Sheet Column Number

一個26進位的算數

```python
class Solution:
    def titleToNumber(self, columnTitle: str) -> int:
        ln=len(columnTitle)
        k,res=0,0
        for i in range(ln-1,-1,-1):
            #print(ord(i)-64,i)
            res+=(ord(columnTitle[i])-64)*(pow(26,k))
            k+=1
        #print(res)
        return res
```


### 190. Reverse Bits


```python
class Solution:
    def reverseBits(self, n: int) -> int:
        st= (bin(n)[2:])
        new=st[::-1]
        a=int(new + '0'*(32-len(new)), 2)  #補足夠的0(32位元)
        return a
```


### 214. Shortest Palindrome

求可以拼成迴文的字串，只能加字母在前面。

時間複雜度: O(n)

```
class Solution:
    def shortestPalindrome(self, s: str) -> str:
        reverse_string=s[::-1]
        ln=len(s)
        if ln==0: return ""
        for i in range(ln,0,-1):
            #print(i,s[:i],reverse_string[ln-i:])
            # s[:i] s中的前i個字串， reverse_string[ln-i:] 反轉s中的後i個字串
            if s[:i]==reverse_string[ln-i:]:
                break
        return reverse_string[:ln-i]+s
```


### 242. Valid Anagram

```python
class Solution:
    def isAnagram(self, s: str, t: str) -> bool:
        dic1=collections.Counter(s)
        dic2=collections.Counter(t)
        if dic1==dic2:
            return True
        else:
            return False
```





### 344. Reverse String
![](https://i.imgur.com/aLH1xXa.png)

* C code
```
void reverseString(char* s, int sSize){
    int i;
    int tmp;
    if(*s!='\0')
    {
        for(i=0;i<(sSize/2);i++)
        {
            tmp=s[i];
            s[i]=s[sSize-1-i];
            s[sSize-1-i]=tmp;
        }
    }
}
```

* Python code

```
class Solution:
    def reverseString(self, s: List[str]) -> None:
        """
        Do not return anything, modify s in-place instead.
        """
        if(s):
            Size=len(s)
            sSize=int(len(s)/2)
            for i in range(sSize):
                s[i],s[Size-1-i]=s[Size-1-i],s[i]
```
### 241. Different Ways to Add Parentheses

使用遞迴去解

```
class Solution:
    def diffWaysToCompute(self, expression: str) -> List[int]:
        result=[]
        if expression.isdigit():
            return [int(expression)] #need string type
        for i,item in enumerate(expression):
            if item in '+-*':
                left=self.diffWaysToCompute(expression[:i])
                right=self.diffWaysToCompute(expression[i+1:])
                #print(item,left)
                for l in left:
                    for r in right:
                        result.append(eval(str(l) + item + str(r)))
        return result
```

### 345. Reverse Vowels of a String

```
class Solution:
    def reverseVowels(self, s: str) -> str:              
        li=['a', 'e', 'i', 'o','u','A', 'E', 'I', 'O','U']
        len_s=len(s)
        left=0
        right=len_s-1
        ls=list(s)
        while left<right:
            if s[left] in li and s[right] in li:
                ls[left],ls[right]=ls[right],ls[left]
                left+=1
                right-=1
            elif s[left] in li:
                right-=1
            else:
                left+=1
            
        return ''.join(ls)
```

### 383. Ransom Note

檢查 ransomNote 是否可以由 magazine 組成
想法:
1. magazine 建立字典
2. ransomNote 去字典查詢是否有

speed: 92 ms
```
class Solution:
    def canConstruct(self, ransomNote: str, magazine: str) -> bool:
        dic={}
        for i in range(len(magazine)):
            if magazine[i] in dic:
                dic[magazine[i]]+=1
            else:
                dic[magazine[i]]=1
        #print(dic)
        for i in range(len(ransomNote)):
            if ransomNote[i] in dic and dic[ransomNote[i]]>0:
                dic[ransomNote[i]]-=1
            else:
                return False
        return True
```

[另解: 參考其他做法](https://blog.csdn.net/fuxuemingzhu/article/details/54178342)
1. 各自用Counter建立字典
2. 若ransomNote字典內的 val 大於 magazine，就代表無法組成。

speed: 44 ms
```
class Solution:
    def canConstruct(self, ransomNote: str, magazine: str) -> bool:
        rdic=collections.Counter(ransomNote)
        mdic=collections.Counter(magazine)
        #print(rdic,mdic)
        for a,b in rdic.items():
            if b>mdic[a]:
                return False
        return True
```

### 387. First Unique Character in a String

collections.Counter(s) 是按照頻率次數由高到低排序
但是dic.items() 順序是按照 s的字母出現順序去做遍歷

[佐證:dict.keys()、dict.values()、dict.items() 的顺序是一致的，且按照添加顺序排列。](https://zhuanlan.zhihu.com/p/188847468)


```
class Solution:
    def firstUniqChar(self, s: str) -> int:
        sc=collections.Counter(s)
        for a,b in sc.items():
            #print(a,b)
            if b==1:
                uni=a
                return s.index(uni)
        return -1
```

### 389. Find the Difference

法1: 使用counter 計算字符數目，再去比較兩個dict差異

```
class Solution:
    def findTheDifference(self, s: str, t: str) -> str:
        c1=Counter(s)
        c2=Counter(t)
        for k in c1|c2:
            if c1[k]!=c2[k]:
                return k
```

法2: list 型態直接相減取第一個字符
```
class Solution:
    def findTheDifference(self, s: str, t: str) -> str:
        c1=Counter(s)
        c2=Counter(t)
        a=list((c2-c1))
        return a[0]
```


### 409. Longest Palindrome

[Ref:字典統計次數](https://blog.csdn.net/fuxuemingzhu/article/details/54236594)

一開始想不到怎麼解決基數個字符問題

方法:
1. 如果出現 n次(odd)的字符，一定可以形成回文的字串中 n-1 個的字串
2. 承1，設定+1 (給出現1次的放中間使用)
3. n(even)，一定可以形成回文中n個的字串



```
class Solution:
    def longestPalindrome(self, s: str) -> int:
        d={}
        val=0
        count=0
        prime=0
        sl=len(s)
        for i in range(0,sl):
            a=s[i]
            if a in d:
                d[a]+=1
            else:
                d[a]=1
        print(d)
        for val in d.values():
            if val%2==1:
                count+=val-1
                prime=1
            else:
                count+=val
        return count+prime
```



### 647. Palindromic Substrings

類似 leetcode5 的解法，但是是去算有多少True的值

```
# 2021.3.28
class Solution:
    def countSubstrings(self, s: str) -> int:
        dp=[[False for _ in range(len(s))] for _ in range(len(s))]
        count=0
        for j in range(0,len(s)):
            for i in range(0,j+1):
                if i==j:
                    dp[i][j]=True
                    count+=1
                elif s[i]==s[j] and i+1==j:    
                    dp[i][j]=True
                    count+=1
                elif s[i]==s[j] and dp[i+1][j-1]==True:
                    dp[i][j]=True
                    count+=1
                
                    
        return count
```


### 680. Valid Palindrome II

leetcode 125. Valid Palindrome 的延伸


![](https://i.imgur.com/kwHqCbp.png)


```python
class Solution:
    def validPalindrome(self, s: str) -> bool:
        left,right=0,len(s)-1
        while(left<right):
            if s[left]!=s[right]:
                one=s[left+1:right+1]  #delete left element
                two=s[left:right]  #delete right element
                
                return one==one[::-1] or two==two[::-1] #reverse
            left+=1
            right-=1
        return True
```

### 709. To Lower Case

tolower 函式用法
```
int tolower(int c);
```


```c
char * toLowerCase(char * str){
    int len=strlen(str);
    for (int i=0;i<len;i++)
    {
        str[i]=tolower(str[i]);
    }
    //printf("%s",str);
    return str;
}
```

### 767. Reorganize String

输入是一個字串，如果這個字串 重新排列之後能组成一個新符串 使得這個字符相鄰的字符都不相同，那返回這個新字串；如果做不到，就返回空字串。


```python!
class Solution(object):
    def reorganizeString(self, S):
        counter = collections.Counter(S)
        ans = "#"
        while counter:
            stop = True
            for item, times in counter.most_common():
                if ans[-1] != item:
                    ans += item
                    counter[item] -= 1
                    if not counter[item]:
                        del counter[item]
                    stop = False
                    break
            if stop:
                break
        return ans[1:] if len(ans) == len(S) + 1 else ""
```

* 解法2
先把出現最多次的找出來, 先從 0 開始放偶數位
在交錯放入奇數位

Time Complexity: O(N)
```python!
class Solution(object):
    def reorganizeString(self, s):
        counter = collections.Counter(s)
        max_key=counter.most_common(1)[0][0]
        if counter[max_key] > (len(s)+1) //2:
            return ""
        out = [''] * len(s)
        ind=0
        for _ in range(counter[max_key]):
            out[ind] = max_key
            ind += 2
        del counter[max_key]
        for key, val in counter.most_common():
            for _ in range(val):
                if ind >= len(s):
                    ind = 1
                out[ind] = key
                ind += 2
        return "".join(out)
```

### 804. Unique Morse Code Words

找出一組字符串進行莫爾斯電碼的編碼有多少種不同情況。
```
zip可以將多個迭代器相對應位置打包成元組，返回一個迭代器。
```

```python
# 2021.5.19
class Solution:
    def uniqueMorseRepresentations(self, words: List[str]) -> int:
        sign=[".-","-...","-.-.","-..",".","..-.","--.","....","..",".---","-.-",".-..","--","-.","---",".--.","--.-",".-.","...","-","..-","...-",".--","-..-","-.--","--.."]
        eng=['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']
        dic=dict(zip(eng,sign))
        res=set()
        for item in words:
            combine=""
            for w in item: #gin -> g i n
                combine+=dic[w]
            res.add(combine)
        return len(res)
```

### 819. Most Common Word

有兩種解法:
1.一般而言會使用HashMap或dict來解題
2.字典樹(Trie)


order=sorted(dic.items(), key = lambda vv:(vv[1]),reverse=True)
lambda 是按照vv[1] 也就是dic 中 index=2的順序去排列 (reverse=True 代表大到小) 


```python
class Solution:
    def mostCommonWord(self, paragraph: str, banned: List[str]) -> str:
        punct=string.punctuation
        paragraph=paragraph.lower()
        for c in punct:
            paragraph=paragraph.replace(c," ")
        #print(paragraph)
        res=[]
        res=paragraph.split()
        dic={}
        for i in res:
            if i not in dic:
                dic[i]=1
            else:
                dic[i]+=1
        
        order=sorted(dic.items(), key = lambda vv:(vv[1]),reverse=True)
        
        for i,item in enumerate(order):
            if item[0] not in banned:
                return item[0]
```

Ans2: [Trie](https://desolve.medium.com/%E5%BE%9Eleetcode%E5%AD%B8%E6%BC%94%E7%AE%97%E6%B3%95-48-trie-1-533ffdfdc6ac)

```python=
class Solution:
    def __init__(self):
        self.res = ""
        self.maxcnt = 0

    class TrieNode:
        def __init__(self):
            self.word = ""
            self.cnt = 0
            self.links = [None] * 26

    def insert(self, r: TrieNode, s: str):
        curr = r
        for i in s:
            index = ord(i) - ord('a')
            if not curr.links[index]:
                curr.links[index] = self.TrieNode()
                curr.links[index].word = curr.word + i
            curr = curr.links[index]
        curr.cnt += 1

    def ban(self, r: TrieNode, s: str):
        curr = r
        for i in s:
            index = ord(i) - ord('a')
            if not curr.links[index]: return
            curr = curr.links[index]
        curr.cnt = 0

    def findMax(self, curr: TrieNode):
        if not curr: return
        if curr.cnt > self.maxcnt:
            self.res = curr.word
            self.maxcnt = curr.cnt
        for i in range(len(curr.links)):
            self.findMax(curr.links[i])

    def mostCommonWord(self, paragraph: str, banned: List[str]) -> str:
        root = self.TrieNode()
        words = re.findall(r'\w+', paragraph.lower())
        for word in words:
            self.insert(root, word)
        for s in set(banned):
            self.ban(root, s)
        self.findMax(root)
        
        return self.res
```



### 884. Uncommon Words from Two Sentences


```python
class Solution:
    def uncommonFromSentences(self, s1: str, s2: str) -> List[str]:
        total=s1+" "+s2
        lis=[]
        lis=total.split(" ")
        dic={}
        res=[]
        for i in lis:
            if i not in dic:
                dic[i]=1
            else:
                dic[i]+=1
        #print(dic)
        for key,val in dic.items():
            if val==1:
                res.append(key)
        return res
```


### 1221. Split a String in Balanced Strings

![](https://i.imgur.com/BRxAOBm.png)

R跟L 個數一樣時輸出

* Python code
```
class Solution:
    def balancedStringSplit(self, s: str) -> int:
        slen=len(s)
        r=0
        l=0
        count=0
        for i in s:
            if i=='R':
                r+=1
            elif i=='L':
                l+=1
            
            if l==r:
                count+=1
                l=0
                r=0
        return count
```

### 1328. Break a Palindrome

兩種情況
1.全是a，換最後一個字元為b
2.換第一個不為a的字元為b


```python
class Solution:
    def breakPalindrome(self, palindrome: str) -> str:
        ln=len(palindrome)
        if(ln==1):
            return ""
        for i in range(int(ln/2)):
            #print(palindrome[i],i)
            if palindrome[i]!='a':
                palindrome=palindrome.replace(palindrome[i],'a',1)
                return palindrome
            
        palindrome=palindrome[:-1]+"b"
        return palindrome
```


### 1332. Remove Palindromic Subsequences

這題要注意是subsequence (不需要連續之字串)。
最差情況為把a全部移掉在移掉b，所以最多2次。
三種情況:
1. 若s="" ，return 0
2. 若是回文，return 1
3. 不是回文，return 2
```
class Solution:
    def removePalindromeSub(self, s: str) -> int:
        # subsequence not need continuous
        if not s:
            return 0   
        if s[::-1]==s:
            return 1
        else:
            return 2
```


### 1370. Increasing Decreasing String

[上升下降字符串](https://blog.csdn.net/Wonz5130/article/details/104731560)
```
# 2021.4.11
class Solution:
    def sortString(self, s: str) -> str:
        res=''
        s=list(s)
        while s:
            temp=list(set(s))
            temp=sorted(temp,reverse = False)
            #print(temp)
            for i in temp:
                res+=i
                s.remove(i)
            temp=list(set(s))
            temp=sorted(temp,reverse = True) #large to small
            #print(temp)
            for i in temp:
                res+=i
                s.remove(i)
        return res
```

### 1832. Check if the Sentence Is Pangram

判斷26個字母是否均出現過一次

```python=
class Solution:
    def checkIfPangram(self, sentence: str) -> bool:
        dic={}
        for i in sentence:
            if i not in dic:
                dic[i]=1
            else:
                dic[i]+=1
        if len(dic)==26: #dict key length
            return True
        else:
            return False
```







## BST

### 94. Binary Tree Inorder Traversal

就是寫一個 Inorder  中序

法一
```python=
class Solution:
    def inorderTraversal(self, root: TreeNode) -> List[int]:
            res=[]
            if root:  ## 下面加self原因是要在class中用到inorder這個函式
                res.extend(self.inorderTraversal(root.left))    
                res.append(root.val) 
                res.extend(self.inorderTraversal(root.right)) 
            return res
```


法二
```python=
class Solution:
    def inorderTraversal(self, root: TreeNode) -> List[int]:
        self.list=list()
        self.travel(root)
        return self.list
        
    def travel(self,root):
        if not root:
            return
        else:
            self.travel(root.left)
            self.list.append(root.val)
            self.travel(root.right)
```

### 100. Same Tree


p [1,2]的資料結構為
```
TreeNode{val: 1, left: TreeNode{val: 2, left: None, right: None}, right: None}
```
比較兩顆樹是否一樣

```
class Solution:
    def isSameTree(self, p: TreeNode, q: TreeNode) -> bool:
        print(p)
        if not p and not q: #empty
            return True
        elif p and q:
            if p.val==q.val:
                if self.isSameTree(p.left,q.left):
                    return self.isSameTree(p.right,q.right)
```

![](https://i.imgur.com/AOS5zUD.png)


### 101. Symmetric Tree

題目: 判斷使否是對稱樹

寫兩個函式: 
a) 交換，先把自己的左子樹左右交換
b) 相等，再判斷自己的左右子樹相不相等

```python
# 2021.7.17
class Solution:
    def isSymmetric(self, root: TreeNode) -> bool:
        def rev(root):
            if not root:
                return
            tmp=root.left
            root.left=rev(root.right)
            root.right=rev(tmp)
            return root
        
        def same(p,q):
            if not p and not q:
                return True
            if p and q:
                if p.val==q.val:
                    if same(p.left,q.left):
                        return same(p.right,q.right)
            
        rev(root.left)
        return(same(root.left,root.right))
```



### 104. Maximum Depth of Binary Tree

```python=
class Solution:
    def maxDepth(self, root: TreeNode) -> int:
        def get_height(root):
            if not root:
                return 0
            left_h = get_height(root.left)
            right_h = get_height(root.right)
            
            return max(left_h,right_h)+1
        
        return get_height(root)
```

### 108. Convert Sorted Array to Binary Search Tree

Given an integer array nums where the elements are sorted in ascending order, convert it to a **height-balanced** binary search tree.

左右子樹高度不能相差超過1
陣列已經排好，所以每次取中間值當作root即可。

```python=
class Solution:
    def sortedArrayToBST(self, nums: List[int]) -> TreeNode:
        if not nums:
            return None
        mid=len(nums)//2
        #print(mid)
        root=TreeNode(nums[mid])
        root.left=self.sortedArrayToBST(nums[0:mid])
        root.right=self.sortedArrayToBST(nums[mid+1:])

        return root
```

### 109. Convert Sorted List to Binary Search Tree

跟108題一模一樣，把list type轉成 array即可

```python=
class Solution:
    def sortedListToBST(self, head: ListNode) -> TreeNode:
        arr=[]
        cur=head
        while(cur):
            arr.append(cur.val)
            cur=cur.next
        return self.convert(arr)
        
    def convert(self,arr):
        if not arr:
            return None
        mid=len(arr)//2
        root=TreeNode(arr[mid])
        root.left=self.convert(arr[0:mid])
        root.right=self.convert(arr[mid+1:])
        return root
```





### 110. Balanced Binary Tree

是否是平衡的二元樹(左右子樹不相差超過1)

想法:
如果只要有子樹不是平衡，則return -1 回去 (不直接return false 是因為)

```python=
class Solution:
    def isBalanced(self, root: TreeNode) -> bool:
        def get_height(root):
            if not root:
                return 0
            left_height,right_height = get_height(root.left),get_height(root.right)
            if left_height<0 or right_height<0 or abs(left_height-right_height)>1:
                return -1
            return max(left_height,right_height)+1
        
        if get_height(root)>=0:
            return True
        else:
            return False            
```
DFS 解法
```python!
class Solution:
    def dfs(self,root):
        if not root:
            return True
        return max(self.dfs(root.left),self.dfs(root.right))+1

    def isBalanced(self, root: Optional[TreeNode]) -> bool:
        if not root:
            return True
        left=self.dfs(root.left)
        right=self.dfs(root.right)

        if left-right not in [-1,0,1]:
            return False
        
        return self.isBalanced(root.left) and self.isBalanced(root.right)
```


### 111. Minimum Depth of Binary Tree

題目給定一個二元樹，要求找到最小的深度，也就是從根節點到最近的葉節點的路徑長。
(葉節點是指它底下沒有其他小孩了)

```
class Solution:
    def minDepth(self, root: TreeNode) -> int:
        #print(root)
        if not root:
            return 0
        if not root.left:
            return 1+self.minDepth(root.right)
        elif not root.right:
            return 1+self.minDepth(root.left)
        else:
            return 1+min(self.minDepth(root.right),self.minDepth(root.left))
```

### 112. Path Sum

1. 利用遞歸算法一層一層往下，每往下一層就減去前一層的值，最後一層與最後的值相等，而最後的值是一個leaf節點，就return True。
2. 一上來就是判斷root是否存在。
3. 看什麼樣的情況下是對的: 最後一個值等於剩下的sum，最後一個位置的node是一個葉子節點(沒有左右節點)。
4. 接著同時迭代左跟右邊，從左或右是否存在中取一個or的關係，這種情況下就return True。


```python
# 2021/7/18
class Solution:
    def hasPathSum(self, root: TreeNode, targetSum: int) -> bool:
        if not root:
            return False
        if not root.left and not root.right and root.val==targetSum:
            return True
        return self.hasPathSum(root.left,targetSum-root.val) or self.hasPathSum(root.right,targetSum-root.val )
```


### 124. Binary Tree Maximum Path Sum

[用遞迴的方式解題](https://ithelp.ithome.com.tw/articles/10213281)，從root開始往下走到每個節點，計算該節點往下走的最大路徑和，並且每經過一個節點就考慮看看這個節點是中繼點的狀況，一旦發現最大值比現有記錄的大，就更新成新的最大值。

psudo code
```
define res
maxPathSum(root){
    res = root.val
    dfs(root)
    return res
}
dfs(n){
    l = (n.left != NIL) ? dfs(n.left) : 0
    r = (n.right != NIL) ? dfs(n.right) : 0
    m = n.val
    m = max(m, l + m, r + m) // 單做為經過點的最大路徑和
    res = max(res, m, l + r + n.val) // m已比過3種可能，故只需再考慮中繼點
    return m
}
```

```
class Solution:
    def maxPathSum(self, root: TreeNode) -> int:
        self.res=root.val
        self.dfs(root)
        return self.res
    
    def dfs(self,n:TreeNode) -> int:
        l=self.dfs(n.left) if n.left else 0
        r=self.dfs(n.right) if n.right else 0
        m=max(n.val,l+n.val,r+n.val)
        self.res=max(self.res,m,l+r+n.val)
        #print(m)
        return m
```




### 144. Binary Tree Preorder Traversal
Preorder前序

```
class Solution:
    def preorderTraversal(self, root: TreeNode) -> List[int]:
            res=[]
            if root:
                res.append(root.val)
                res.extend(self.preorderTraversal(root.left))
                res.extend(self.preorderTraversal(root.right))
            return res
```

### 226. Invert Binary Tree

反轉一棵二元樹
![](https://i.imgur.com/hnhURu0.png)


```
class Solution:
    def invertTree(self, root: TreeNode) -> TreeNode:
        if not root:
            return
        tmp=root.left
        root.left=self.invertTree(root.right) #right tree invert
        root.right=self.invertTree(tmp) #left tree invert 
        
        return root
```




### 538. Convert BST to Greater Tree

![](https://i.imgur.com/F0I67Jt.png)

* 右子樹開始trans
```
class Solution:
    def convertBST(self, root: TreeNode) -> TreeNode:
        self.sum = 0
        
        def trans(node):
            if not node: 
                return
            #print(node)
            trans(node.right)
            self.sum += node.val
            node.val = self.sum
            trans(node.left)
        trans(root)
        return root
```

### 617. Merge Two Binary Trees


```python=
class Solution:
    def mergeTrees(self, root1: TreeNode, root2: TreeNode) -> TreeNode:
        
        if not root1:
            return root2
        if not root2:
            return root1
        if not root1 and root2:
            return
        new=TreeNode(root1.val+root2.val)
        new.left=self.mergeTrees(root1.left,root2.left)
        new.right=self.mergeTrees(root1.right,root2.right)
            
        return new
```



### 653. Two Sum IV - Input is a BST

找尋二元樹中是否具有合為k的兩點
先用中序把值存進陣列中，再搭配two-sum的解法。

```python=
class Solution:
    def findTarget(self, root: TreeNode, k: int) -> bool:
        res=[]
        def dfs(root):
            if not root:
                return
            dfs(root.left)
            res.append(root.val)
            dfs(root.right)
        dfs(root)
        
        for i in range(len(res)):
            sub=k-res[i]
            if sub in res:
                if res.index(sub)<i:
                    #print(res.index(sub),i)
                    return True
        return False
```



### 700. Search in a Binary Search Tree

在BST中找尋以val為root的子樹

```
class Solution:
    def searchBST(self, root: TreeNode, val: int) -> TreeNode:
        if not root:
            return None
        if root.val==val:
            return root
        elif root.val<val:
            return self.searchBST(root.right,val)
        else:
            return self.searchBST(root.left,val)
```


### 965. Univalued Binary Tree

判斷二元樹中是否具有單一值

想法:
1.把數值加進array
2.判斷set值是否大於1
```
class Solution:
    def isUnivalTree(self, root: TreeNode) -> bool:
        res=[]
        def dfs(root):
            if not root:
                return 
            else:
                dfs(root.left)
                res.append(root.val)
                dfs(root.right)
        dfs(root)
        if len(set(res))>1:
            return False
        else:
            return True
```


* 別人解法
利用值and去判斷

```
class Solution:
    def isUnivalTree(self, root):
        if not root: 
            return True
        if root.left and root.left.val != root.val: 
            return False
        if root.right and root.right.val != root.val: 
            return False
        a=self.isUnivalTree(root.left)
        b=self.isUnivalTree(root.right)
        return a and b
```


## Greedy

### 45. Jump Game II

55題的衍生
要求最少次數抵達最尾端，(保證一定可以抵達)。


![](https://i.imgur.com/mz2S0BH.jpg)

* max: 所能抵達的最大位址
* cur: 目前能抵達的最大位址，假設cur=i，代表一定要跳了。
* jump: 必須跳的次數 


```python=
class Solution:
    def jump(self, nums: List[int]) -> int:
        maxpos,cur,count=0,0,0
        #for i,item in enumerate(nums):
        for i in range(len(nums)-1):
            maxpos=max(maxpos,i+nums[i])
            if cur==i: #must jump 
                count+=1
                cur=maxpos
        return count
```

### 55. Jump Game

題目要判斷是否能夠抵達最尾端的陣列。
貪心算法，每次取最大能到的位置。

```python=
class Solution:
    def canJump(self, nums: List[int]) -> bool:
        maxpos=0
        for i,num in enumerate(nums):
            if i>maxpos: #can't reach this nums[i] position
                return False
            else:
                maxpos=max(maxpos,i+num)
            #print(i,num,maxpos)
        return True
```

### 561. Array Partition I

由於我們要最大化每對中的較小值之和，那麼肯定是每對中兩個數字大小越接近越好，因為如果差距過大，而我們只取較小的數字，那麼大數字就浪費掉了

Greedy 方法
先把陣列排序，兩兩比較


```python
class Solution:
    def arrayPairSum(self, nums: List[int]) -> int:
        new=sorted(nums)
        res=0
        for i in range(0,len(nums),2):
            res+=new[i]
        return res
```

----

<style>
.blue {
  color: blue;
}
</style>
