# Go


[Udemy - Class](https://www.udemy.com/course/go-the-complete-developers-guide/)
account: oldelette@gmail.com
[Udemy - Diagram GitHub](https://github.com/StephenGrider/GoCasts/tree/master)

[Ref: Go Playground](https://play.golang.org)

---
Step
Go to https://github.com/StephenGrider/GoCasts/tree/master/diagrams
Open the folder containing the set of diagrams you want to edit
Click on the ‘.xml’ file
Click the ‘raw’ button
Copy the URL
Go to https://www.draw.io/
On the ‘Save Diagrams To…’ window click ‘Decide later’ at the bottom
Click ‘File’ -> ‘Import From’ -> ‘URL’
Paste the link to the XML file

---


## Simple Start




### Boring Hello World

![](https://i.imgur.com/kzotC9P.png)
```go=
package main

import "fmt"

func main() {
	fmt.Println("Hi there!")
}
```



Questions
:::danger

1. How do we run the code in our project?
2. What does 'package main' mean?
3. What does 'import "fmt"' mean?
4. What's that 'func' thing?
5. How is the main.go file organized?
:::


### Go CLI

go run、go build、go install的[差別](https://medium.com/%E4%BC%81%E9%B5%9D%E4%B9%9F%E6%87%82%E7%A8%8B%E5%BC%8F%E8%A8%AD%E8%A8%88/golang-goroot-gopath-go-modules-%E4%B8%89%E8%80%85%E7%9A%84%E9%97%9C%E4%BF%82%E4%BB%8B%E7%B4%B9-d17481d7a655)

#### go run
go run指令簡單來說就像是直譯的方式來執行程式碼，而在細節上它其實會將程式碼進行編譯，最後產生可執行檔案，然後運行該可執行檔案，產生程式執行後的結果，而可執行檔會放在特定的暫存檔裡面，執行完就會自動刪除了


#### go build

go build指令就是將程式碼編譯為執行檔，例如：
```
go build main.go
```
這樣會在main.go當前目錄下產生一個main 名稱的可執行檔，如果是 Windows的話就是 main.exe.

但是go build有個缺點就是每次編譯程式碼，比較沒有效率，當專案架構越來越大，build的速度也就越來越慢.

#### go install

因應go build的缺點，因此有go install指令，go install可以做兩件事情：

1. 將套件編譯成.a file
2. 如果是main套件則編譯成可執行檔


![](https://i.imgur.com/DCpB5Xj.png)


### Package

Package Name and Folder Structure - [link](https://pjchender.dev/golang/modules-and-packages/#package-name-and-folder-structure)

1. Go 的程式碼都是以 package 的方式在組織，一個 package 就是在「同一資料夾」中的許多 GO 檔案。
2. 同一個 package 的所有 Go 檔案會放在同一個資料夾內；而這個資料夾內的所有檔案也都會屬於同一個 package，有相同的 package 名稱。
3. 通常 package 的名稱會跟著 folder 的名稱，舉例來說，若檔案放在 math/rand 資料夾中，則該套件會稱作 rand。




![](https://i.imgur.com/Z082iq3.png)

#### Executable / Reusable package

有兩種不同類型的 package

* executable package：是用來產生我們可以執行的檔案，一定會包含 package main 並且帶有名為 main 的函式，只有這個檔案可以被執行（run）和編譯（build），並且不能被其他檔案給匯入。
* reusable package(library package)：類似 "helpers" 或常稱作 library / dependency，目的是可以放入可重複使用的程式邏輯，它是不能被直接執行的，可以使用任何的名稱。

![](https://i.imgur.com/LWymWB8.png)

在 Go 裡面要區別這兩種 Package 的主要方式就是利用「package 名稱」
當使用 main 當做 package 名稱時，就會被當作 executable package
因此接著執行 go build \<fileName\> 時，會產生一支執行檔
但是當使用 main 以外的名稱是，都會被當作是 reusable package，因此當使用 go build 指令時，不會產生任何檔案。

![](https://i.imgur.com/XBxdcZp.png)

![](https://i.imgur.com/dsdCBQj.png)



### Import statement

[golang.org/pkg](https://pkg.go.dev/std)


![](https://hackmd.io/_uploads/B1BQLNc6h.png)



#### Import fmt

可以直接參考文檔 - [Package fmt](https://pkg.go.dev/fmt@go1.21.0) 



### func

[函式宣告](https://pjchender.dev/golang/function-and-method/#%E5%87%BD%E5%BC%8F%E5%AE%A3%E5%91%8A) -  最常見的宣告方式

* 使用 func 來定義函式

```go=
// 完整寫法：func add(x int, y iny) int {...}
func add(x, y int) int {
  return x + y
}
```

![](https://i.imgur.com/ax8hQUo.png)



### main.go organized

![](https://i.imgur.com/NXXm9nO.png)




## Deeper Into Go



### Variable Declarations

[Basic Go Type](https://pjchender.dev/golang/variables/#basic-go-type)




```go=
package main

import "fmt"

func main() {
	// var card string = "Ace of spades"
	card := "Ace of Spades"
	fmt.Println(card)
}
```

上面的 6,7 行是完全等價的
要特別注意一點, 只有在定義新變數的時候, 才能用 :=


![](https://i.imgur.com/jDYF2KB.png)


Golang 是一個靜態型別 (statically-typed)


:::success
靜態語言 / 動態語言 (Static vs. Dynamic Typing)
變數帶有資料型態是靜態語言，且使用前一定要先宣告；反之則是動態語言.

* Static：
優點是可避免執行時期型態錯誤、提供重構輔助與更多的文件形式.
缺點是程式語法繁瑣、彈性不足，只能檢查出執行時期前的簡單錯誤.
ex: C++、Java、Go
* Dynamic：
優點是語法簡潔、具有較高的彈性.
缺點是型態錯誤在執行時期才會呈現出來，效能表現較不理想，編輯輔助工具較為缺乏，依賴慣例或實體文件來得知API使用方式.
ex: Javascript、Ruby、Python
:::


### Basic Go Types


![](https://i.imgur.com/6fR6u1A.png)


#### 第一種宣告方式: short declaration


使用 := 宣告，表示之前沒有進行宣告過. 這是在 go 中最常使用的變數宣告的方式，因為它很簡潔.
但因為在 package scope 的變數都是以 keyword 作為開頭，因此不能使用縮寫的方式定義變數（foo := bar），只能在 function 中使用，具有區域性（local variable）：

```go=
// 第一種宣告方式
function main() {
    foo := "Hello"
    bar := 100

  // 也可以簡寫成
  foo, bar := "Hello", 100
}

// 等同於
function main() {
    var foo string
    foo = "Hello"
}
```

:::info
在 package scope 宣告的變數會一直保存在記憶體中，直到程式結束才被釋放，因此應該減少在 package scopes 宣告變數
:::

#### 第二種宣告方式: variable declaration

使用時機主要是：
* 當你不知道變數的起始值
* 需要在 package scope 宣告變數
* 當為了程式的閱讀性，將變數組織在一起時

```go=
// 第二種宣告方式，在 main 外面宣告（全域變數），並在 main 內賦值
var foo string
var bar int

// 可以簡寫成
var (
    foo string
    bar int
)

function main() {
    foo = "Hello"
    bar = 100
}
```


#### 第三種宣告方式

直接宣告並賦值：
```go=
// 第三種宣告方式，直接賦值
var (
    foo string = "Hello"
    bar int = 100
)
```

---


#### Quiz

* Not valid

```go=
// example 1 - syntax error: non-declaration statement outside function body
package main
 
import "fmt"
 
deckSize := 20
 
func main() {
  fmt.Println(deckSize)
}

// example 2 - deckSize is assigned before it's initialized
package main
 
import "fmt"
 
func main() {
  deckSize = 52
  fmt.Println(deckSize)
}
```

* Valid
```go=
// example 1
package main
 
import "fmt"
 
func main() {
  var deckSize int
  deckSize = 52
  fmt.Println(deckSize)
}

// example 2 - 可以在 main 函式外宣告變數
package main
 
import "fmt"
 
var deckSize int
 
func main() {
  deckSize = 50
  fmt.Println(deckSize)
}
```




### Functions and Return Types


```go=
package main

import "fmt"

func main() {
	// card := "Ace of Spades"
	card := newCard()
	fmt.Println(card)
}

func newCard() string {
	return "Five of Diamonds"
}
```

Go 清楚定義無論何時執行 newCard 這個 function, 它的回傳值都要是 string type

![](https://i.imgur.com/xye6G72.png)




---

### Slice

[Golang](https://pjchender.dev/golang/slice-and-array/) Slice and Array

在 Go 裡面有兩種資料結構可以處理包含許多元素的清單資料，分別是 Array 和 Slice：

1. Array：清單的長度是固定的（fixed length），屬於原生型別（primitive type），較少在程式中使用。
2. Slice：可以增加或減少清單的長度，使用 [] 定義，例如，[]byte 是 byte slice，指元素為 byte 的 slice；[]string 是 string slice，指元素為 string 的 slice。


![](https://i.imgur.com/YzeemBi.png)


#### Byte Slice


[]byte 是 byte slice，指元素為 byte 的 slice

![Imgur](https://i.imgur.com/yAHTeuy.png)





#### Slice For Loop


在疊代陣列時會使用到 for i, card := range cards{…}，其中如果 i 在疊代時沒有被使用到，但有宣告它時，程式會噴錯；因此如果不需要用到這個變數時，需把它命名為 _。


![Imgur](https://i.imgur.com/kQcvYK6.png)


```go=
package main

import (
	"fmt"
)

func main() {
	cards := []string{"Ace of Diamonds", newCard(), newCard()}
	cards = append(cards, "Six of Spages") // 既有的 slice 新增
	for i, cards := range cards {
		fmt.Println(i, cards)
	}
}

func newCard() string {
	return "Five of Diamonds"
}
```

#### Split

切割須要留意的是，在 golang 中使用 : 來切割 slice 時，並不會複製原本的 slice 中的資料，
而是建立一個新的 slice
但實際上還是指稱到相同位址的底層 array（並沒有複製新的），因此還是會改到原本的元素

:::danger
有時我們只需要使用原本 slice 中的一小部分元素，但由於透過 : 的方式重新分割 slice 時
該 slice 仍然會指稱到底層相同的 array，這導致雖然我們只用了原本 slice 中的一小部分元素
但其他多餘的部分因為仍然被保留在底層的 array 中,
進而使得該 array 無法被 garbage collection.

因此若只需要使用原本 slice 中一小部分的元素內容時,
建議可以使用 copy 或 append 建立新的 slice 與其對應的新的 array.
:::



```go=
func main() {
    // 定義內部元素型別為字串的陣列
    fruits := []string{"apple", "banana", "grape", "orange"}

    // 取得 slice 的 length 和 capacity
    fmt.Println(len(fruits)) // length, 4
    fmt.Println(cap(fruits)) // capacity, 4

    // append 為陣列添加元素（不會改變原陣列）
    fruits = append(fruits, "strawberry")

    // range syntax: fruits[start:end] 可以截取陣列，從 "start" 開始到 "end-1" 的元素
    fmt.Println(fruits[0:2]) //  [apple, banana]
    fmt.Println(fruits[:3])  //  [apple, banana, grape]
    fmt.Println(fruits[2:])  //  [grape, orange, strawberry]

    // 透過 range cards 可以疊代陣列元素
    for _, fruit := range fruits {
        fmt.Println(fruit)
    }
}
```

另外，使用 : 時，不能超過它本身的 capacity，否則會導致 runtime panic



```go=
func main() {
    s := []int{1, 2, 3, 4, 5}

    // over the slice capacity will cause runtime panic
    overSliceCap := s[:6]
    fmt.Println(overSliceCap)
}
```



#### Copy

1. cop 的元素數量如果「多於」被複製進去的元素時，會用 zero value 去補.
ex: 當 cop 的長度是 4，但只複製 3 個元素進去時，最後位置多出來的元素會補 zero value.
2. cop 的元素數量如果「少於」被複製進去的元素時，超過的元素不會被複製進去.
ex: 當 cop 的長度是 1，但卻複製了 3 個元素進去時，只會有 1 個元素被複製進去.


```go=
func main() {
	scores := []int{1, 2, 3, 4, 5}
	cop := make([]int, 4) // 建立空 slice 且長度為 4
	copy(cop, scores[:3]) // 使用 copy 將前 scores 中的前三個元素複製到 cop 中
	fmt.Println("cop: ", cop) // cop:  [1 2 3 0]
}
```

#### make &　new

golang [筆記](https://medium.com/d-d-mag/golang-%E7%AD%86%E8%A8%98-make-%E8%88%87-new-%E7%9A%84%E5%B7%AE%E5%88%A5-68b05c7ce016)：make 與 new 的差別


**new** 
new 可以用來初始化泛型，並且返回儲存位址.
所以通常我們會用指標變數來接 new 過後的型別.
特別要注意的是，new 會自動用 zeroed value 來初始化型別，也就是字串會是""，number 會是 0，channel, func, map, slice 等等則會是 nil


**make**
make 與 new 不同，是用來初始化一些特別的型別，像是 channel, map, slice 等等.
另外特別要注意的是 make 並不會回傳指標，如果要拿到指標，就要考慮用像是 new 的方式來初始化型別


使用 make 建立 slice 時，可以指定該 slice 的 length 和 capacity

```go=
func main() {
    // make(T, length, capacity)
    a := make([]int, 5) // len(a)=5, cap(a)=5, [0 0 0 0 0]

    // 建立特定 capacity 的 slice
    b := make([]int, 0, 5) // len=0 cap=5, []
    b = b[:cap(b)]         // len=5 cap=5, [0 0 0 0 0]
    b = b[1:]              // len=4 cap=4, [0 0 0 0]
}
```

#### capacity and length

[link](https://pjchender.dev/golang/slice-and-array/#capacity-and-length)

在 Slice 中會包含
* Pointer to Array：這個 pointer 會指向實際上在底層的 array。
* Capacity：從 slice 的第一個元素開始算起，它底層 array 的元素數目
* Length：該 slice 中的元素數目

![Imgur](https://i.imgur.com/7yWoeqQ.png)

:::info
len()取得 slice 元素的個數
cap()取得 slice 第一個元素於代表的 array 中的索引起算到最後的長度
:::



它們會如下圖存放在記憶體中，因此當我們使用該 slice 時，它會先指向該 slice，而該 slice 中的 pointer to head 會在指到底層的 array：
![Imgur](https://i.imgur.com/OhE4ng3.png)





## Organizing Data With Structs




### Struct in Go

[Golang](https://pjchender.dev/golang/structs/) Struct

structs 是在 GO 中的一種資料型態，它就類似 python dictionary, JavaScript 中的物件（Object）或是 Ruby 中的 Hash.



* 三種宣告 Person struct 的方式：
```go=
var user1 *Person       // nil
user2 := &Person{}      // {}，user2.firstName 會是 ""
user3 := new(Person)    // {}，user3.firstName 會是 ""
```


![Imgur](https://i.imgur.com/CPSh19P.png)


```go=
type person struct {
	firstName string
	lastName  string
}

func main() {
	alex := person{firstName: "Alex", lastName: "Anderson"}
	fmt.Println(alex) // {Alex Anderson}
}
```

#### Golang zero value

[Golang](https://blog.kalan.dev/posts/golang-default-value) 當中的預設值以及 zero value

在 golang 當中，如果在初始化時沒有賦值，就會使用 zero value.

![Imgur](https://i.imgur.com/KvFKoXw.png)


這時因為 firstName, lastName 都沒有輸入，所以預設都會是 ""
```go=
type person struct {
	firstName string
	lastName  string
}

func main() {
	var alex person
	fmt.Println(alex) // { }
	fmt.Printf("%+v", alex) //{firstName: lastName:}
}
```

### Embedding Structs

在 struct 內關聯另一個 struct（[nested struct](https://pjchender.dev/golang/structs/#%E5%9C%A8-struct-%E5%85%A7%E9%97%9C%E8%81%AF%E5%8F%A6%E4%B8%80%E5%80%8B-structnested-struct)）


![Imgur](https://i.imgur.com/4X1C8rV.png)

```go=
type contactInfo struct {
	email   string
	zipCode int
}

type person struct {
	firstName string
	lastName  string
	contactInfo
}

func main() {
	jim := person{
		firstName: "Alex",
		lastName:  "Anderson",
		contactInfo: contactInfo{
			email:   "alex@gmail.com",
			zipCode: 94000,
		},
	}
	jim.print()
}

func (p person) print() {
	fmt.Printf("%+v", p)
}
```

### Pass by Value

Go 是一個 pass by value 的程式語言，也就是每當我們把值放入函式中時，Go 會把這個值完整的複製一份，並放到新的記憶體位址
以下面的程式碼為例：

```go=
package main

import "fmt"

type person struct { // 建立 person type
    firstName string
    lastName  string
}

// 建立 person 的 function receiver
func (p person) updateName(newFirstName string) {
    fmt.Printf("Before update: %+v\n", p)Ｇ
    p.firstName = newFirstName
    fmt.Printf("After update: %+v\n", p)
}

func (p person) print() {
    fmt.Printf("Current person is: %+v\n", p)
}

func main() {
    jim := person{
        firstName: "Jim",
        lastName:  "Party",
    }

  // Before update: {firstName:Jim lastName:Party}
    jim.updateName("Aaron")
  // After update: {firstName:Aaron lastName:Party}

    jim.print() // Current person is: {firstName:Jim lastName:Party}
}
```
會發現到雖然呼叫了 jim.updateName() 這個方法，但 jim 的 firstName 並沒有改變.
這是因為當我們呼叫 jim.updateName() 時，Go 會把呼叫此方法的 jim 複製一份到新的記憶體位置，修改的 jim 其實是存在另一個記憶體位置，
這就是為什麼當我們在 updateName() 這個函式中呼叫 p 時，
會看到 firstName 是有改變的，
但最後呼叫 jim.print() 時，卻還是得到舊的 jim

![Imgur](https://i.imgur.com/1deNSJb.png)



### Struct with Pointers



:::info
使用 struct pointer 時才可以修改到原本的物件，否則會複製一個新的
:::


當 pointer 指稱到的是 struct 時，可以直接使用這個 pointer 來對該 struct 進行設值和取值.
在 golang 中可以直接使用 pointer 來修改 struct 中的欄位.



![Imgur](https://i.imgur.com/bplD75C.png)

```go=
func main() {
	jim := person{
		firstName: "Alex",
		lastName:  "Anderson",
		contactInfo: contactInfo{
			email:   "alex@gmail.com",
			zipCode: 94000,
		},
	}
	jimPointer := &jim
	jimPointer.updateName("jimmy")
	jim.print()
    // {firstName:jimmy lastName:Anderson contactInfo:{email:alex@gmail.com zipCode:94000}}
}

func (pointerToPerson *person) updateName(newFirstName string) {
	(*pointerToPerson).firstName = newFirstName
}
```


![Imgur](https://i.imgur.com/nE2ghNy.png)


### Pointer Shortcut

一般來說，若想要透過 struct pointer（&v）來修改該 struct 中的屬性
需要先解出其值（*p）後使用 (*p).X = 10，但這樣做太麻煩了
因此在 golang 中允許開發者直接使用 p.X 的方式來修改

```go=
jimPointer := &jim
jimPointer.updateName("jimmy")
// same
jim.updateName("jimmy")
```


![Imgur](https://i.imgur.com/9XpH3De.png)


### Data type


Value vs Reference Types - [資料型別 Data Types](https://pjchender.dev/golang/data-types/)

在 Golang 中，雖然所有資料結構都是 Pass by value
但當碰到 slices、maps、functions 時，雖然它還是會 copy 一份新的
只是實際上，它 copy 的是一個 pointer，所以即使它依然是 copy by value，但對這個變數進行修改時，會修改到 pointer 背後實際所指稱到的記憶體位址
進而使得在操作這些資料結構時，會感覺像是 Pass by Reference


![Imgur](https://i.imgur.com/WURtQey.png)


#### Custom Type

自定義資料型別

```go=
// 定義 cards 這個型別，它的本質是 slice of strings
type cards []string

// 定義 person 這個型別，它的本質是 struct
type person struct {
    firstName string
    lastName string
}
```

#### Type Conversion

型別轉換（Type Conversion）
T(v) 這種方式，可以把 v 的值轉換成 T 這個型別.
舉例來說:

![Imgur](https://i.imgur.com/rsmOKON.png)

```go=
func main() {
    i := 42              // int
    f := float64(i)      // int 轉成 float64
    u := uint(f)         // float 64 轉成 uint

    fmt.Println(i, f, u)
}
```






----

## Others

### Go modules

Go modules 是 Go 從 1.11 版開始提供的套件管理工具，就像 Nodejs 的 npm 或是 python 的 pip，用起來也很簡單

### Start

#### go mod init

這邊要使用 go mod init \<project-name\> 進行初始化（類似 npm init），完成後會多一個檔案 go.mod
    
    
```
go mod init test

go: creating new go.mod: module test
go: to add module requirements and sums:
        go mod tidy
```


#### go mod tidy

:::success
go mod tidy
:::

這個 command 有兩個作用

1. 引用項目需要的依賴增加到 go.mod 文件
使用cat go.mod 查看go.mod文件會發現多了一行
![](https://hackmd.io/_uploads/SJ44klran.png)


2. 去掉 go.mod 文件中項目不需要的依賴
執行 rm uuid.go 删除 uuid.go文件，然後執行 go mod tidy -v,
其中-v 可以顯示整個命令的執行過程
![](https://hackmd.io/_uploads/H1o-exSpn.png)


#### go run

在 Nodejs 中一般會使用 npm install \<package\> 安裝 package，Python 則是使用 pip install \<package\>，但在 Go 裡面不需要特地下指令，直接用就可以了

譬如說這邊要使用一個 package 叫 logrus，那就直接到 main.go 裡面 import \"github.com\/sirupsen/logrus\"，意思是「我要使用 github 上 sirupsen/logrus 這個 package」，然後就可以開始用了

```go=
package main

import (
    "github.com/sirupsen/logrus"
)

func main() {
    // 在 main 裡面使用 logrus
    l := logrus.New()
    
    l.Info("This is an info")
    l.Warn("This is a warning")
    l.Error("This is an error")
}
```

接著在編譯的時候 Go 就會自動下載那個 package 再進行編譯

![](https://hackmd.io/_uploads/rkSHbgBp2.png)



#### go.sum

編譯完之後除了 go.mod 之外還會多出一個檔案 go.sum
簡單說 go.sum 是Go用來驗證下載的 module 是否為真，內容是否有被竄改過的 module 校驗雜湊碼檔









