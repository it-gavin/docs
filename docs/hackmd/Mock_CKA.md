###### tags: `Work`

# Mock_CKA

### 1. RBAC

![](https://i.imgur.com/ZqO8UkH.png)

:::danger
role / rolebinding  有 ns
clusterrole / clusterrolebinding  無 ns
serviceaccount  有 ns

pv 無 ns
pvc 有 ns
:::

```
kubectl create clusterrole deployment-clusterrole --verb=create \
--resource=deployments,daemonsets,statefulsets

kubectl create serviceaccount cicd-token -n app-team1

kubectl create rolebinding cicd-token-rolebinding --serviceaccount=app-team1:cicd-token \
--clusterrole=deployment-clusterrole -n app-team1
```

![](https://i.imgur.com/l1rp3x6.png)


### 2. CPU

![](https://i.imgur.com/CVJw3y4.png)

```
k top pod -l name=cpu-utilizer -A --sort-by cpu > /opt/KUTR00401/KUTR00401.txt
```

### 3. Networkpolicy

![](https://i.imgur.com/hYYkdDT.png)

```
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: allow-port-from-namespace
  namespace: my-app
spec:
  podSelector: {}
  policyTypes:
    - Ingress
  ingress:
    - from:
        - namespaceSelector:
            matchLabels:
              name: big-corp # 訪問者的 ns labels
      ports:
        - protocol: TCP
          port: 8080
```

#### 檢查: 
allow-port-from-namespace

![](https://i.imgur.com/bHH7uSo.png)

### 4. Service

![](https://i.imgur.com/yg3M6Kk.png)

deployment 修改 pod container port

```
k edit deployment front-end

...
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx
        ports:                #　增加
        - containerPort: 80　　#　增加
          name: http　　　　　　#　增加
          
kubectl expose deployment front-end --port=80 --target-port=80 ＼
--type=NodePort --name=front-end-svc
```

#### 檢查

```
1. curl <node>:<Nodeport>
--> curl cluster-node2:32566
2. curl <svc>:80
--> curl 10.108.194.230:80
```
![](https://i.imgur.com/5iku3CA.png)


### 5. Ingress

![](https://i.imgur.com/TmKsgR8.png)

* -k：跳過 SSL 證書檢測 
* -L：跟随跳轉，比如網站做了重定向，不加這個選項的话只會看到一個 302 的訪問 code 就结束了，加上的話會看到完整的跳轉情况

```
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: pong
  namespace: ing-internal
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
spec:
  ingressClassName: nginx-example
  rules:
  - http:
      paths:
      - path: /hello
        pathType: Prefix
        backend:
          service:
            name: hello
            port:
              number: 5678
```

#### 檢查

```
kubectl get ingress -ning-internal
curl -kL internal_IP/hello
```


### 6. Deployment scale

![](https://i.imgur.com/WoI8ARW.png)

```
k scale deployment loadbalancer --replicas 2
```

### 7. nodeSelector

![](https://i.imgur.com/GGYzQGp.png)

[k8s-io Using Pods](https://kubernetes.io/docs/concepts/workloads/pods/#using-pods) 再手動新增 **nodeSelector 欄位**

```
apiVersion: v1
kind: Pod
metadata:
  name: nginx-kusc00401
spec:
  containers:
  - name: nginx-kusc00401
    image: nginx
  nodeSelector: #這個是和 containers 同一级别的。
    disk: ssd
```

### 8. Node Scheduled

![](https://i.imgur.com/5IpxBFc.png)

```
k describe node | grep Taint | grep -vc NoSchedule > /opt/KUSC00402/kusc00402.txt
```
* -c 代表統計個數
* -v 代表排除

#### 檢查
```
cat /opt/KUSC00402/kusc00402.txt
--> 2
```

### 9. Multi-container

![](https://i.imgur.com/VutY8Tk.png)
[k8s-io Using Pods](https://kubernetes.io/docs/concepts/workloads/pods/#using-pods) 在手動增加多個 container
```
apiVersion: v1
kind: Pod
metadata:
  name: kucc4
spec:
  containers:
  - name: nginx
    image: nginx
  - name: redis
    image: redis
  - name: memcached
    image: memcached
```


#### 檢查
```
k get pods 確認有 3/3
```
![](https://i.imgur.com/sK54hYd.png)


### 10. PV

![](https://i.imgur.com/zIL4D3U.png)

[k8s-io Persistent Volumes](https://kubernetes.io/docs/concepts/storage/persistent-volumes/#persistent-volumes) 再去修正成題目要的

```
apiVersion: v1
kind: PersistentVolume
metadata:
  name: app-data
spec:
  capacity:
    storage: 2Gi
  volumeMode: Filesystem
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: /srv/app-data
```

### 11. PVC

![](https://i.imgur.com/CJx0FbT.png)

[k8s-io PersistentVolumeClaims](https://kubernetes.io/docs/concepts/storage/persistent-volumes/#persistentvolumeclaims) 再去修正成題目要的

```
# PVC
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: pv-volume
spec:
  accessModes:
    - ReadWriteOnce
  volumeMode: Filesystem
  resources:
    requests:
      storage: 10Mi
  storageClassName: ci-hostpath-sc
```
[k8s-io Claims As Volumes ](https://kubernetes.io/docs/concepts/storage/persistent-volumes/#claims-as-volumes) 再去修正成題目要的
```
# Pod
      volumeMounts:
      - mountPath: "/usr/share/nginx/html"
        name: mypd
  volumes:
    - name: mypd
      persistentVolumeClaim:
        claimName: pv-volume
```
更改 pvc 容量
```
kubectl edit pvc pv-volume
```

![](https://i.imgur.com/Ey8ttem.png)


### 12. Pod log

![](https://i.imgur.com/sPb2EZe.png)

```
k logs bar | grep "file-not-found" > /opt/KUTR00101/bar
```


### 13. Sidecar

![](https://i.imgur.com/EeVt4ek.png)


sidecar 邊車容器不是作為主容器，而是輔助主容器做一些功能

```
# 先把運行中的 legacy-app pod 倒出來成 yaml file, 然後備份
kubectl get pods leagcy-app -o yaml > sidecar.yaml
cp sidecar.yaml sidecar.yaml.bkg

# 修改好後
kubectl delet -f sidecar.yaml
kubectl create -f sidecar.yaml
```

[k8s-io Using a sidecar container with the logging agent](https://kubernetes.io/docs/concepts/cluster-administration/logging/#sidecar-container-with-logging-agent) 再去修正成題目要的

* 增加之內容
```
... 
    volumeMounts:
    - name: varlog
      mountPath: /var/log
  - name: count-log-1
    image: busybox:1.28
    args: [/bin/sh, -c, 'tail -n+1 -F /var/log/1.log']
    volumeMounts:
    - name: varlog
      mountPath: /var/log
  volumes:
  - name: varlog
    emptyDir: {}
```



### 14. Upgrade

[CKA真题14](https://www.youtube.com/watch?v=LTvaD8aWTiM&ab_channel=%E5%B0%8F%E8%8E%AB)

![](https://i.imgur.com/kqNGyHf.png)

**题目要求不升级 etcd，這裡手動補上**

```
1、切換環境
kubectl config use-context mk8s

2、配置
#升级kueadm
kubectl drain mk8s-master-0 --ignore=daemonsets

ssh mk8s-master-0
sudo -i 

 apt-mark unhold kubeadm && \
 apt-get update && apt-get install -y kubeadm=1.26.0-00 && \
 apt-mark hold kubeadm

kubeadm upgrade apply v1.26.0 --etcd-upgrade=false

#升级 kubelt
apt-mark unhold kubelet kubectl && \
apt-get update && apt-get install -y kubelet=1.26.0-00 kubectl=1.26.0-00 && \
apt-mark hold kubelet kubectl

sudo systemctl daemon-reload
sudo systemctl restart kubelet #這裡要重啟 kubelt 的

exit
exit

kubectl uncordon mk8s-master-0

3、驗證
kubectl get node -o wide
kubectl --version
kubelet --version
```


### 15. ETCD backup & Restore

[CKA真题15](https://www.youtube.com/watch?v=ZNOMtTUU7XM&ab_channel=%E5%B0%8F%E8%8E%AB)

![](https://i.imgur.com/yGnWxub.png)

```
1、確定當前環境
kubectl get node
2. 配置
export ETCDCTL_API=3
etcdctl --endpoints=https://127.0.0.1:2379 \
  --cacert=<trusted-ca-file> --cert=<cert-file> --key=<key-file> \
  snapshot save /var/lib/backup/etcd-snapshot.db
  
# Restoring an etcd cluster
ll /data/backup/etcd-snapshot-previous.db
# (不確定對不對) 直接還原, 不要加 --data-dir
etcdctl snapshot restore /var/lib/backup/etcd-snapshot-previous.db
```
```
# 正常流程, 還要去修正 /etc/kubernetes/manifests/etcd.yaml 裡面的 hostPath: 路徑
etcdctl snapshot restore --data-dir <data-dir-location> \
/var/lib/backup/etcd-snapshot-previous.db
vim /etc/kubernetes/manifests/etcd.yaml
```

* –data-dir: 自己定義要將 ETCD 還原的位置 \<data-dir-location\> ，會自動幫創建資料夾


以下應該正確 restore
```
mv /etc/kubernetes/manifests /etc/kubernetes/manifests_bkg
ETCDCTL_API=3 etcdctl snapshot restore /tmp/etcd-backup.db
cp -r /etc/kubernetes/manifests_bkg /etc/kubernetes/manifests
# 裡面的 hostPath 路徑修正
vim /etc/kubernetes/manifests/etcd.yaml 
```
![](https://i.imgur.com/NX46tpe.png)


### 16. Troubleshoot
[CKA真题16](https://www.youtube.com/watch?v=H5Qtns6Y3UU&ab_channel=%E5%B0%8F%E8%8E%AB)

![](https://i.imgur.com/G0Mwkqo.png)


通過 get nodes 查看異常節點，登入節點查看 kubelet 等组件的 status 並判斷原因.
真實考试时，這個異常節點的 kubelet 服務没有啟動導致的.
journalctl -u kubelet


```
k config use-context wk8s
k get node
ssh wk8s-node-0
sudo -i
systemctl status kubelet
systemctl start kubelet
systemctl enable kubelet
```

### 17. Drain & Cordon

[CKA真题17](https://www.youtube.com/watch?v=jx1R0EzMWEA&ab_channel=%E5%B0%8F%E8%8E%AB)

![](https://i.imgur.com/hHSa1AZ.png)

```
k config use-context wk8s
kubectl cordon ek8s-node-1
kubectl drain ek8s-node-1 --ignore-daemonsets

# 如果 --ignore-daemonsets 報錯, 使用
kubectl drain ek8s-node-1 --ignore-daemonsets --delete-local-data --force
kubectl drain ek8s-node-1 --ignore-daemonsets --delete-emptydir-data --force

# 驗證
kubectl get node
```

正常來說已經沒有 pod 會在 ek8s-node-1 上了, 但是 daemonsets 模式的 pod 還是會顯示在 ek8s-node-1 上 (因為上面加了 --ignore-daemonsets)