###### tags: `Work`
# Pipenv


[pipenv](https://packaging.python.org/tutorials/managing-dependencies/#managing-dependencies) 是目前 Python 官方所推薦的套件管理工具

[pipenv - Hackmd](https://hackmd.io/@yencheng/r1yzqakKv)

[Vedio: Python Tutorial: Pipenv - Easily Manage Packages and Virtual Environments](https://www.youtube.com/watch?v=zDYL22QNiWk)

* 自動產生與更新Pipfile and Pipfile.lock 解決了維護 requirements.txt 的問題
* 透過套件的 Hash 安全性檢查確認 （當安裝套件 hash 值不同時，跳出錯誤，防止惡意套件侵入）
* 產生 dependency 的樹狀圖 (e.g. $ pipenv graph)



## Run pipenv

在專案目錄下，建立 Py3 or Py2 虛擬環境：
```
pipenv --three  // 產生 Python 3 虛擬環境
pipenv --two // 產生 Python 2 虛擬環境
```

這邊的版本會根據 OS 中 Python3 的預設運作版本 (3.8.10: /usr/bin/python)。

![](https://i.imgur.com/DNZdAo9.png)



```
pipenv install
```

第一次執行 pipenv 後會在目錄下產生 **Pipfile 裡面會記載安裝過的套件**，就像以前用的 requirements.txt 一樣，同時也會在安裝好後新增 Pipfile.lock 作為 Hash 的安全檢查用。

* ++Pipfile file++
```yaml
[[source]]
url = "https://pypi.python.org/simple"
verify_ssl = true
name = "pypi"

[packages]
pylint = "==2.4.4"
mypy = "==0.910"

[dev-packages]

[requires]
python_version = "3.8"
```

* ++安裝套件++
安裝後 加進 Pipfile

```
pipenv install pylint==2.4.4
pipenv install mypy==0.910
```


* ++進入虛擬環境++
```
pipenv shell
```

* ++列出套件相依圖++
```
pipenv graph
```

![](https://i.imgur.com/w1DhBIq.png)

* ++顯示套件++
```
pipenv lock -r
```

![](https://i.imgur.com/8cX0zy1.png)


* ++產生 requirements++
```
pipenv lock --requirements > requirements.txt
```


* ++Import from requirements++
只要有 requirements.txt 執行 pipenv install 就會幫你建環境囉!
```
pipenv install -r path/to/requirements.txt
```

* ++刪除環境++
```
pipenv --rm
```

![](https://i.imgur.com/F6qjZSr.png)




## Other


### 查看虛擬環境位置

:::success
pipenv --venv
:::

![](https://i.imgur.com/eve5ZLf.png)


* ++利用 python sys 也可以查看執行位置++
```
>>> import sys
>>> sys.executable
'/home/gavin/.local/share/virtualenvs/pipenv_test-QjQ43fuv/bin/python'
```

# virtualenv

[搞懂Python的virtualenv](https://ithelp.ithome.com.tw/articles/10199980)

```
sudo apt install python3.8-venv
```

## 建立虛擬環境

最後的myvenv則是虛擬環境的名稱。
當執行指令後virtualenv會將所需資料複製到myvenv這資料夾中，接下來就是如何使用了

```
python3 -m venv myvenv
```


會在執行此命令下的資料夾下建立一個存放虛擬環境資訊的資料夾。
像下面這張圖， 就是建立兩個虛擬環境，分別是 myenv 以及 test。

![](https://i.imgur.com/6Yzppbt.png)


## 使用虛擬環境

當成功建立一個虛擬環境後會在你專案的資料夾內產生一個虛擬環境資料夾，例如用venv產生的就會擁有一個venv的資料夾，然後依照以下方式可以啟動虛擬環境模式：

```
source ./myvenv/bin/activate.fish
```

![](https://i.imgur.com/iSI6OMb.png)


## 關閉環境 venv 包

```
deactivate
```



# Pyenv


Pyenv 解決了 Python 版本的問題，有著下列的好處

* 可以讓使用者依照自己需求切換全域的 Python 版本
* 可以基於不同專案決定該專案需要的 Python 版本
* 並且不會相依於 Python，是由 shell script 撰寫而成
* 可以透過修改環境變數來覆寫 Python 的版本


