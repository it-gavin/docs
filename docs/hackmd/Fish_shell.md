###### tags: `Work`
# Fish shell

:::info
sudo apt-add-repository ppa:fish-shell/release-3
sudo apt-get update
sudo apt-get install fish
:::



安裝完 Fish shell 後，預設啟用還是原本的 Bash，
可以嘗試使用這個指令，來把預設的 shell 切換成 Fish：
```
chsh -s /usr/bin/fish gavin
```

## Oh-my-fish

suggested: lambda、 eclm

要讓你的 Fish shell 更強大的話，通常會建議安裝 Oh-my-fish
```
curl -L https://get.oh-my.fish | fish
```

多種主題可以到官網查看效果 - [Available themes](https://github.com/oh-my-fish/oh-my-fish/blob/master/docs/Themes.md)


### 列出所有的安装包

[參考文章: Oh My Fish! 讓你的 Shell 漂亮起來](https://zhuanlan.zhihu.com/p/35448750)

:::success
omf list
:::

![](https://i.imgur.com/MZREMYK.png)


### 改變主題

:::success
omf theme < theme-name>  #ex: omf theme agnoster
:::



### 下載/刪除 主題


:::success
omf install < theme-name>  #ex: omf install agnoster
omf remove < theme-name>  #ex: omf remove agnoster
:::



![](https://i.imgur.com/NdDSxv4.png)


## fish Alias

修改這個檔案
```
vim ~/.config/fish/config.fish
````

* example
```
if status is-interactive                                                                                                                                      
    # Commands to run in interactive sessions can go here
end

# Alias
alias python="python3"
```



## PPA

[Ubuntu Linux 新增與移除 PPA 個人套件庫指令教學](https://blog.gtwang.org/linux/ubuntu-linux-add-and-remove-ppa-command-tutorial/)

PPA 是 Personal Package Archive 的縮寫
軟體開發者將原始碼上傳至 [Launchpad](https://launchpad.net/ubuntu) 後，透過其線上編譯服務，產生預先編譯好的套件，提供使用者使用。

如果你（使用者) 自己打包了一些有用的套件
你可以去 Launchpad 註冊一個專屬帳號，把你打包的套件 上傳到 Launchpad 你的 專屬帳號內。
Launchpad 會幫你轉為 二進位檔 放在 你的專屬套件庫內





### Add 

新增套件庫就是執行 add-apt-repository 這個指令，後面接著 PPA 個人套件庫的名稱即可

:::success
ppa: 使用者名稱/PPA名稱  #套件庫名稱的格式    
:::

新增的 PPA 會存放在系統的 /etc/apt/sources.list.d/ 目錄下，也可以直接用文字編輯器去修改。

