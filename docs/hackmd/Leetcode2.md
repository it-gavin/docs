###### tags: `Language`

# Leetcode2



## DP

[leetcode 经典动态规划DP算法题目(思路、方法、code)](https://blog.csdn.net/dingdingdodo/article/details/106083169#198_httpsleetcodecncomproblemshouserobber_33)

### 62. Unique Paths

![](https://i.imgur.com/gc0oKng.png)
m=3 n=7 ans=28
![](https://i.imgur.com/DDLzhOb.jpg)


找出 DP 公式

```
dp[i][j]=dp[i][j-1]+dp[i-1][j] # 自己這格的路徑 = 上面那格的路徑 + 左邊那格的路徑
```

方法1:
初始值全是1，再去改。
```python=
class Solution:
    def uniquePaths(self, m: int, n: int) -> int:
        dp=[[1 for _ in range(n)] for _ in range(m)] # m*n matrix
        #print(dp)
        #dp[0][0]=1
        for i in range(1,m):
            for j in range(1,n):
                #print(i,j)
                dp[i][j]=dp[i][j-1]+dp[i-1][j]
        return dp[m-1][n-1]
```


方法2:

初始值全是0 ,dp[0][0]=1
[上面那格的路徑] 與 [左邊那格的路徑] 分開加。
```python=
class Solution:
    def uniquePaths(self, m: int, n: int) -> int:
        dp=[[0 for _ in range(n)] for _ in range(m)] #m*n matrix
        #print(dp)
        dp[0][0]=1
        for i in range(m):
            for j in range(n):
                if i!=0:
                    dp[i][j]+=dp[i-1][j]
                if j!=0:
                    dp[i][j]+=dp[i][j-1]
        return dp[m-1][n-1]
```


### 63. Unique Paths II

方法1: error
卡在無法處理 i=0 or j=0 時的路徑設置

```python=
class Solution:
    def uniquePathsWithObstacles(self, obstacleGrid: List[List[int]]) -> int:
        m=len(obstacleGrid)
        n=len(obstacleGrid[0])
        print(m,n)
        dp=[[1 for _ in range(n)] for _ in range(m)] #start and end has obstacles
        if obstacleGrid[0][0]==1 or obstacleGrid[m-1][n-1]==1:
            return 0
        for i in range(m):
            for j in range(n):
                if obstacleGrid[i][j]==1:
                    dp[i][j]=0
                    
        for i in range(1,m):
            for j in range(1,n):
                if dp[i][j]!=0:
                    dp[i][j]=dp[i][j-1]+dp[i-1][j]
        return dp[m-1][n-1]
```

正確做法:

把 i 與 j 的值分開加，也就是[上面那格的路徑] 與 [左邊那格的路徑] 分開加。

```python=
class Solution:
    def uniquePathsWithObstacles(self, obstacleGrid: List[List[int]]) -> int:
        m=len(obstacleGrid)
        n=len(obstacleGrid[0])
        #print(m,n)
        dp=[[0 for _ in range(n)] for _ in range(m)] #start and end has obstacles
        if obstacleGrid[0][0]==1 or obstacleGrid[m-1][n-1]==1:
            return 0
        dp[0][0]=1
        for i in range(m):
            for j in range(n):
                if obstacleGrid[i][j]==1:
                    dp[i][j]=0
                else:
                    if i!=0:
                        dp[i][j]+=dp[i-1][j]
                    if j!=0:
                        dp[i][j]+=dp[i][j-1]
        #print(dp)
        return dp[m-1][n-1]
```


### 70. Climbing Stairs

![](https://i.imgur.com/nYs8R5p.png)


* Time Limit Exceeded 
**遞迴方法太慢**
```
class Solution:
    def climbStairs(self, n: int) -> int:
        if n<3:
            return n
        else:
            return self.climbStairs(n-1)+self.climbStairs(n-2)
```



```python=
#DP
class Solution:
    def climbStairs(self, n: int) -> int:
        arr=[0,1,2]
        if n<3:
            return n
        else:
            for i in range(3,n+1):
                arr.append(arr[i-1] + arr[i-2])
            return arr[n]
```


### 198. House Robber

經典小偷DP 問題
不能連續兩天偷，求最大偷走金額。

第k 天能偷的最大值
a. k-2 的最大值加上今天偷的
b. k-1 的最大值


```python=
class Solution:
    def rob(self, nums: List[int]) -> int:
        ln=len(nums)
        if ln<2:
            return nums[0]
        dp=[0 for _ in range(ln)]
        dp[0]=nums[0]
        dp[1]=max(nums[0],nums[1])
        for i in range(2,ln):
            dp[i]=max(dp[i-2]+nums[i],dp[i-1])
        
        return max(dp)
```

### 213. House Robber II

跟 leetcode 198 差在整個是一個環狀的家，所以要找不能同時包含頭尾的最大值
總共有 k 戶人家
a. 位置: 0 ~ k-1 
b. 位置: 1 ~ k


```python=
class Solution:
    def rob(self, nums: List[int]) -> int:
        ln=len(nums)
        if ln<2:
            return nums[0]
        dp=[0 for _ in range(ln)]
        dp[0]=nums[0]
        dp[1]=max(nums[0],nums[1])
        for i in range(2,ln-1): # 0 ~ n-1
            dp[i]=max(dp[i-2]+nums[i],dp[i-1])
        tmp1,tmp2=0,0
        tmp1=max(dp)
        
        dp=[0 for _ in range(ln)]
        dp[0]=0
        dp[1]=nums[1]
        for i in range(2,ln): # 1 ~ n
            dp[i]=max(dp[i-2]+nums[i],dp[i-1])
        tmp2=max(dp)
        
        return max(tmp1,tmp2)
```



### 300. Longest Increasing Subsequence

[最長遞增子序列，類似 leetcode 334](https://www.youtube.com/watch?v=5rfZ4WnNKBk&ab_channel=happygirlzt)

* subswquence: 子字串不用連續
* subarray: 需要連續

經典DP:
用1D dp array紀錄每個位子最長的subsequence

![](https://i.imgur.com/tnTlwyv.png)

```python
#2021/6/30
class Solution:
    def lengthOfLIS(self, nums: List[int]) -> int:
        for i in range(len(nums)):
            for j in range(i):
                if nums[j]<nums[i]:
                    dp[i]=max(dp[i],dp[j]+1) #由前面的dp[j]在加1
        return max(dp)
```



```python
class Solution:
    def lengthOfLIS(self, nums: List[int]) -> int:
        n=len(nums)
        dp=[-float('inf')]*(n)
        maxlen=0
        
        for i in range(n):
            ln=1 #unit num base len=1
            for j in range(i): #check previous subsequence
                if nums[j]<nums[i]:
                    ln=max(ln,dp[j]+1)
                    
            dp[i]=ln
            if dp[i]>maxlen:
                maxlen=dp[i]
        #print(dp)
        return maxlen
```


### 322. Coin Change
![](https://i.imgur.com/mX5RynN.png)

#### C code
```c=
int coinChange(int* coins, int coinsSize, int amount){
    int arr[amount+1];
    int Max=999999999;
    int n=coinsSize;
    arr[0]=0; //intialization
    int i,j; //arr[1],arr[2],....,f[27]
    for(i=1;i<=amount;i++)
    {
        arr[i]=Max;
        //last coin coins[j]
        for(j=0;j<n;j++)
        {
            if(i>=coins[j]&& arr[i-coins[j]]!=Max)
            {
                int tmp;
                int c=arr[i-coins[j]]+1;
                if(arr[i]>c)
                {
                    tmp=arr[i];
                    arr[i]=c;
                    c=tmp;
                }
                
            }
        }
    }
    
    if(arr[amount]==Max)
        return -1;
    else
        return arr[amount];
}
```


[花花酱 LeetCode 322. Coin Change - 刷题找工作 EP148](https://www.youtube.com/watch?v=uUETHdijzkA&ab_channel=HuaHua)

![](https://i.imgur.com/tDunj09.png)



### 334. Increasing Triplet Subsequence


類似 300 題，確保max 值>=3 即可。

```c
class Solution:
    def increasingTriplet(self, nums: List[int]) -> bool:
        n=len(nums)
        dp=[1 for _ in range(n)]
        for i in range(n):
            ln=1
            for j in range(i):
                if nums[j]<nums[i]:
                    ln=max(dp[j]+1,ln)
                dp[i]=ln
        if max(dp)>=3:
            return True
        else:
            return False
```


### 337. House Robber III

rob: The max amount of money if the root is robbed.
not_rob: The max amount of money if the root is not robbed

兩種狀況討論:
node不偷: 左child 最大 + 右child 最大
node偷: 左child不偷的最大 + 右child 不偷的最大 + 此node.val

```python=
class Solution:
    def rob(self, root: TreeNode) -> int:
        rob,notrob=self.help(root)
        
        return max(rob,notrob)
        
    def help(self,root):
        if root:
            l_rob,l_notrob=self.help(root.left)
            r_rob,r_notrob=self.help(root.right)
            
            c_rob=root.val+l_notrob+r_notrob
            c_notrob=max(l_rob,l_notrob)+max(r_rob,r_notrob)
            
            return c_rob,c_notrob
        
        return 0,0
```

#### Python3

BFS 算法

以當前值amount為起點，相鄰的每一個節點為amount-coin(coin in coins, amount - coin),找出能找零的最小層即可，找不到返回-1.（運行超時）

```python=
class Solution:
    def coinChange(self, coins: List[int], amount: int) -> int:
        count = 0
        if not coins:
            return -1
        if not amount:
            return 0
        if amount < coins[0]:
            return -1
        q_store = [amount]
        #print(q_store)
        while q_store:
            count += 1
            q_list = q_store
            #print(q_list)
            q_store = []
            for q in q_list:
                for coin in coins:
                    if q > coin:
                        q_store.append(q - coin)
                    elif q < coin:
                        break
                    else:
                        return count
        return -1
```


==DP算法==

![](https://i.imgur.com/CSsQvhs.png)



```python=
class Solution(object):
    def coinChange(self, coins, amount):
        """
        :type coins: List[int]
        :type amount: int
        :rtype: int
        """
        dp = [float('inf')]*(amount+1)
        dp[0] = 0
        for coin in coins:
            for i in range(coin, amount+1):
                if dp[i-coin] != float('inf'):
                    dp[i] = min(dp[i], dp[i-coin] + 1)
        
        if dp[-1] != float('inf'):
            return dp[-1]
        else:
            return -1
```

### 516. Longest Palindromic Subsequence

[Leetcode 516. Longest Palindromic Subsequence (Python)](https://zhenyu0519.github.io/2020/03/01/lc516/#leetcode-516-longest-palindromic-subsequence-python)


題目: 找到最長的迴文字串

case1: s[i]==s[j]  字元一樣時
a<span class="blue">*****</span>a
```
dp[i][j]=dp[i+1][j-1]+2
```


case2: s[i]!=s[j] 字元不一樣時，以下兩種狀況
(2.1): a <span class="blue">b***** b </span>
(2.2): <span class="blue">a*****a</span> b

```
dp[i][j]=max(dp[i+1][j],dp[i][j-1])
```


迴圈跑法: 由右下角開始，往右及上面跑。 (下圖藍色框框內藍筆順續)

ex: bbbab
依序找到以下之最長迴文字串: b -> ab -> bab -> bbab -> bbbab

![](https://i.imgur.com/uV17ThJ.jpg)




```c
class Solution:
    def longestPalindromeSubseq(self, s: str) -> int:
        ln=len(s)
        dp= [[0 for _ in range(ln)] for _ in range(ln)]
        #print(dp)
        for i in range(ln-1,-1,-1):
            dp[i][i]=1
            for j in range(i+1,ln):
                if s[i]==s[j]:
                    dp[i][j]=dp[i+1][j-1]+2
                else:
                    dp[i][j]=max(dp[i+1][j],dp[i][j-1])
        return dp[0][ln-1]
```








### 583. Delete Operation for Two Strings

題目:
一次扣掉一個字元，找出讓兩個字串相等的次數。

類似 leetcode1143
[使用DP解決此問題](https://www.cnblogs.com/grandyang/p/7144045.html)
找出兩個字串中最長的 相同子字串 substring ， 再用 word1+word2的長度扣掉相同長度即可。

會遇到兩種狀況:
1. 同樣字元的話，就是dp[i-1][j-1]+1，最長相同子序列又多了一個相同的字符，所以長度加1。

```python
dp[i-1][j-1]+1
```
2. 如果這兩個字符不相等，就要比較前一個 i-1 與 j-1 中的最大值。
題目中的例子來說，"sea" 和"eat"，當比較index[1][1]字符時，發現's' 和'e' 不相等，下一步就要錯位比較，比較sea 中index[0][1]第一個's' 和index[1][0] eat 中的'a'，sea 中的'e' 跟eat 中的第一個'e' 相比

```python
dp[i][j] = max(dp[i-1][j],dp[i][j-1])
```

![](https://i.imgur.com/LtvtXLZ.jpg)



```python=
class Solution:
    def minDistance(self, word1: str, word2: str) -> int:
        if word1==word2:
            return 0
        lw1=len(word1)
        lw2=len(word2)
        dp=[[0 for _ in range(lw1)]for _ in range(lw2)]
        maxcom=0
        for j in range(lw1):
            for i in range(lw2):
                if word2[i]==word1[j]:
                    if i>0 and j>0:
                        dp[i][j]=dp[i-1][j-1]+1
                        maxcom=max(maxcom,dp[i][j])
                    else:
                        dp[i][j]=1
                else:
                    dp[i][j]=max(dp[i-1][j],dp[i][j-1])
                    maxcom=max(maxcom,dp[i][j])
        #print(dp,maxcom)
        #maxcom=max(map(max,dp))
        return lw1+lw2-(2*maxcom)
```


### 673. Number of Longest Increasing Subsequence

為 leetcode:300 最長遞增子序列的變種題


dp[x]: 儲存x結尾之 最長子序列的長度
time[x]: 儲存x結尾之 最長子序列的個數

```python=
class Solution:
    def findNumberOfLIS(self, nums: List[int]) -> int:
        maxLIS,ans=0,0
        ln=len(nums)
        dp=[1 for _ in range(ln)]
        time=[1 for _ in range(ln)]
        for i in range(ln):
            for j in range(i):
                if nums[j]<nums[i]:
                    if dp[j]+1>dp[i]:
                        dp[i]=dp[j]+1
                        time[i]=time[j]
                    elif dp[j]+1==dp[i]:
                        time[i]+=time[j]
        
        #print(dp)
        #print(time)
        maxLIS=max(dp) 
        print(maxLIS)
        for i in range(ln):
            if dp[i]==maxLIS:
                ans+=time[i]
        
        return ans
```


### 1143. Longest Common Subsequence

題目: 找出最長相同sequence，不用連續。
可搭配leetcode583

![](https://i.imgur.com/1R918Rw.jpg)

分兩種情況:
1. 同樣字元的話，就是dp[i-1][j-1]+1，最長相同子序列又多了一個相同的字符，所以長度加1。

```python
dp[i-1][j-1]+1
```
2. 如果這兩個字符不相等，這邊有3種情況。

a. i=0 但 j!=0 代表要參考左邊的格子
```
dp[i][j]=dp[i][j-1]
```
b. i!=0 但 j=0 代表要參考上面的格子
```
dp[i][j]=dp[i-1][j]
```
c. i 跟 j 都不等於0 代表要比較上面的格子 與 左邊的格子。
```
dp[i][j]=max(dp[i-1][j],dp[i][j-1])
```




```python
class Solution:
    def longestCommonSubsequence(self, text1: str, text2: str) -> int:
        t1=len(text1)
        t2=len(text2)
        maxcom=0
        dp=[[0 for _ in range(t1)] for _ in range(t2)]
        #print(dp)
        for j in range(t1):
            for i in range(t2):
                if text1[j]==text2[i]:
                    if i==0 or j==0:
                        dp[i][j]=1
                    else:
                        dp[i][j]=dp[i-1][j-1]+1
                else:
                    if i!=0 and j!=0:
                        dp[i][j]=max(dp[i-1][j],dp[i][j-1])
                    elif i==0 and j!=0:
                        dp[i][j]=dp[i][j-1]
                    elif j==0 and i!=0:
                        dp[i][j]=dp[i-1][j]
                maxcom=max(maxcom,dp[i][j])
        #print(dp)
        return maxcom
```


可以不分三種狀況，直接寫成下面這樣
```c
2021.5.15
class Solution:
    def longestCommonSubsequence(self, text1: str, text2: str) -> int:
        ln1=len(text1)
        ln2=len(text2)
        maxlen=0
        dp=[[0 for _ in range(ln1)]for _ in range(ln2)]
        
        for j in range(ln1):
            for i in range(ln2):
                if text2[i]==text1[j]:
                    if i==0 or j==0:
                        dp[i][j]=1
                    else:
                        dp[i][j]=dp[i-1][j-1]+1
                    maxlen=max(maxlen,dp[i][j])
                else:
                    dp[i][j]=max(dp[i-1][j],dp[i][j-1])
                    maxlen=max(maxlen,dp[i][j])
        #print(dp)
        return maxlen
```


### 1155. Number of Dice Rolls With Target Sum

You have d dice, and each die has f faces numbered 1, 2, ..., f.

Return the number of possible ways (out of fd total ways) modulo 10^9 + 7 to roll the dice so the sum of the face up numbers equals target.

有d個骰子，每個骰子的面有f種，目標是組合成target的可能性

用dp

[花花酱 LeetCode 1155. Number of Dice Rolls With Target Sum - 刷题找工作 EP263](https://www.youtube.com/watch?v=J9s7402s5FA)


![](https://i.imgur.com/ZC6hpTI.png)


[Ref](https://medium.com/rayonchang/leetcode-medium-1155-number-of-dice-rolls-with-target-sum-8d8df678cc3)


error code
```
# 2021.3.29
class Solution:
    def numRollsToTarget(self, d: int, f: int, target: int) -> int:
        #dp[i][t]  1,6,3
        mod= 10**9+7
        dp=[[-1 for i in range(d+1)] for i in range(target+1)]
        print(dp)
        dp[0][0]=1
        for i in range(1,d+1):
            for j in range(1,f+1):
                for k in range(j,target+1):
                    print(i,j,k)
                    dp[i][k]=(dp[i][k]+dp[i-1][k-j])%mod
        return dp[d][target]
```



* 其他方法
```
from functools import lru_cache
class Solution:
    def numRollsToTarget(self, d: int, f: int, target: int) -> int:
        mod=10**9+7
        @lru_cache(maxsize=None)
        def dp(i,t):
            if i==0: 
                if t==0:
                    return 1
                else:
                    return 0
            
            if t>f*i or t<i: return 0
            ans=0
            for k in range(1,f+1): # 1-f
                ans=(ans+dp(i-1,t-k))%mod
            return ans
        
        return dp(d,target)
```

## Sliding Window


[滑動窗口（Sliding Window）技巧總結](https://codingnote.cc/zh-tw/p/188376/)

滑動窗口演算法可以用以解決數組/字元串的子元素問題，它可以將嵌套的循環問題，轉換為單循環問題，降低時間複雜度。


如果題目中求的結果有以下情況時可使用滑動窗口演算法：
* 最小值 Minimum value
* 最大值 Maximum value
* 最長值 Longest value
* 最短值 Shortest value
* K值 K-sized value


### 438. Find All Anagrams in a String

```
Anagram
將組成一個詞或短句的字母重新排列順序
```


找到所有anagram出現在 string 中的 Index 位置


![](https://i.imgur.com/kK8O7bl.png)


```python=
class Solution:
    def findAnagrams(self, s: str, p: str) -> List[int]:
        dic1=Counter(p)
        sub=''
        res=[]
        for i in range(len(s)):
            sub+=s[i]
            if len(sub)==len(p):
                dic2=Counter(sub)
                if dic1==dic2:
                    res.append(i-len(p)+1)
                sub=sub[1:] # remove first char
        return res
```


### 567. Permutation in String


要求 s2中有沒有 s1 的 permutation，有的話返回 True，沒有返回 False。

index 從 0 開始到 len(s2)，
假設sub 長度等於len(s1)，就可以開始比較兩個字典一不一樣。


![](https://i.imgur.com/qZW5R6X.png)



Runtime: 1504 ms

```python=
# 2021/6/30
class Solution:
    def checkInclusion(self, s1: str, s2: str) -> bool:
        m=len(s1)
        n=len(s2)
        sub=''
        window_start,match=0,0
        dic1=Counter(s1)
        for i in range(n):
            sub+=s2[i]
            if len(sub)==m:
                dic2=Counter(sub)
                if dic1==dic2:
                    return True
                sub=sub[1:] # remove first char
        return False
```



### 904. Fruit Into Baskets

重點: 
用 hashmap 去記下水果的種類跟數量
每次窗戶向右移動, 把水果加到 busket 裡面, 順便記下目前裝下的水果
當 busket 裡面的水果種類比2大,開始縮小窗戶
1. 水果種數減少
2. 把水果從 busket 裡面拿出來
3. 如果 busket 裡面沒水果了，那 clean hashmap
4. 縮小窗戶
5. 比較最大水果數量


**Time complexity: O(N)**
雖然有一個for 跟一個 while, 但不代表他的時間複雜度是 O(N\*\*2)
因為 for loop 的右邊窗戶擴大跟 while 裡面左邊窗戶縮減最多最多都只會走一遍 
N: 多少水果, 題目給的 input array
所以是 O(N+N) 也就是 O(N)

**Space complexity: O(1)**
因為我們用的 hashmap 的長度, 最多最多不會超過3
(每當到3的時候, 我們就會開始刪除水果, 直到0個就把這個水果種類從 hashmap 裡面拿掉)


* method 1 - sliding windows
```python!
class Solution:
    def totalFruit(self, fruits: List[int]) -> int:
        maxfruit = 0
        left = 0
        bucket = {}
        for i in range(len(fruits)):
            if fruits[i] in bucket:
                bucket[fruits[i]] += 1
            else:
                bucket[fruits[i]] = 1
            while len(bucket) > 2:
                bucket[fruits[left]] -= 1
                if bucket[fruits[left]] == 0:
                    del bucket[fruits[left]]
                left += 1
            maxfruit = max(maxfruit, i-left+1)

        return maxfruit
```


![](https://i.imgur.com/56JBpng.png)


* method - two pointer
```py!
class Solution:
    def totalFruit(self, fruits: List[int]) -> int:
        start = end = max_fruits = 0
        seen = {}
        for end in range (len(fruits)):
            seen[fruits[end]] = end
            if len(seen) > 2:
                min_ind = min(seen.values())
                start = min_ind + 1
                del seen[fruits[min_ind]]
            max_fruits = max(max_fruits,end - start +1)
            
        return max_fruits
```

![](https://i.imgur.com/sAPx0ke.png)



## Backtracking

回溯法 - 枚舉多維度數值的方法
運用遞迴依序窮舉各個維度的數值，製作所有可能的多維度數值，並且在遞迴途中避免枚舉不正確的多維度數值。


### 37. Sudoku Solver

```python!
class Solution:
    def solveSudoku(self, board: List[List[str]]) -> None:
        """
        Do not return anything, modify board in-place instead.
        """
        self.backtrack(board)
        
    def backtrack(self, board):
        for i in range(9):
            for j in range(9):
                if board[i][j] == '.':
                    for num in '123456789':
                        if self.isPointValid(board, i, j, num):
                            board[i][j] = num
                            if self.backtrack(board): 
                                return True
                            else:
                                board[i][j] = '.' # backtrack
                    return False
        return True
    
    def isPointValid(self, board, x, y, num):
        for i in range(9):
            if board[i][y] == num: # check col
                return False
            if board[x][i] == num: # check row
                return False
            if board[3*(x//3)+i//3][3*(y//3)+i%3] == num: # check 3 * 3 block
                return False
        return True
```

![](https://i.imgur.com/UKHuTgA.png)
