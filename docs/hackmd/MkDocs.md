# MkDocs

[MkDocs](https://www.mkdocs.org/) is a fast, simple and downright gorgeous static site generator that's geared towards building project documentation. 
Documentation source files are written in Markdown, and configured with a single YAML configuration file. Start by reading the introductory tutorial, then check the User Guide for more information.


## Gitlab Install

### Method 1


```
pip install mkdocs mkdocs-material
```

直接初始化項目

```
mkdocs new my-project
```

直接會在當前目錄底下創建一個 資料夾: my-project 再加上一些 mkdocs 需要的基本檔案

![](https://hackmd.io/_uploads/SkfzqaX23.png)




### Method 2

參考: [mkdocs](https://gitlab.com/pages/mkdocs)

Preview your project: mkdocs serve,
your site can be accessed under localhost:8000

```
mkdocs serve
```

![](https://hackmd.io/_uploads/H1uip2mn3.png)



### mkdocs.yml

重點在 site_url 要換成你的 namespace

```
site_name: My Docs by GitLab Pages
site_url: https://it-gavin.gitlab.io/docs
repo_url: "https://gitlab.com/it-gavin/docs"
site_dir: public
```

* https://it-gavin.gitlab.io/docs

![](https://hackmd.io/_uploads/rJUpV6Q32.png)




## Material



### Theme

[mkdocs-material](https://github.com/squidfunk/mkdocs-material)

```
pip install mkdocs-material
```


Add the following lines to mkdocs.yml:
```
theme:
  name: material
```

* 可以看到主題變了

![](https://hackmd.io/_uploads/BkVn5pmh2.png)


### Image

If you want to add image zoom functionality to your documentation, the glightbox plugin is an excellent choice, as it integrates perfectly with Material for MkDocs

```
mkdocs-glightbox
```

### Diagrams

想要 include mermaid, 需要 markdown_extensions

```
markdown_extensions:
  - pymdownx.superfences:
      custom_fences:
        - name: mermaid
          class: mermaid
          format: !!python/name:pymdownx.superfences.fence_code_format
```


### emojis

This configuration enables the use of icons and emojis by using simple shortcodes which can be discovered through the icon search. 
Add the following lines to mkdocs.yml:
```
markdown_extensions:
  - attr_list
  - pymdownx.emoji:
      emoji_index: !!python/name:materialx.emoji.twemoji
      emoji_generator: !!python/name:materialx.emoji.to_svg
```

* md file 顯示
```
:smile: 
```

![](https://hackmd.io/_uploads/B1lIiR73h.png)



### Logo


logo 會在網頁的左上角顯示出来，可以是一個文字或者是圖片
如果是圖片可以用這樣的形式：
```
theme:
  logo: 'images/logo.svg'
 ```

上面例子 mkdocs 會在 **docs 目錄下搜尋 images 的目錄**，所以前提是你要有這個文件



### nav


屬性 nav 是設定文件中最重要的屬性之一，聽過它實現章節目錄的設定


```
nav:
  - "index.md"
  - Work:
      - Tool:
        - MKDocs: "hackmd/MkDocs.md"
        - Pipenv: "hackmd/Pipenv.md"
      - Kubernetes:
        - Kubectl: "hackmd/Kubectl.md"
        - Kubernetes: "hackmd/Kubernetes.md"
        - CKA: "hackmd/CKA.md"
        - Mock_CKA: "hackmd/Mock_CKA.md"
      - Nginx: "hackmd/Ngnix.md"
```

![](https://hackmd.io/_uploads/rJ_75R7n3.png)







