###### tags: `Work`

# CKA

Certified Kubernetes Administrator (CKA)

[官網報名連結](https://www.cncf.io/certification/cka/)

You will be evaluated on 5 topics around Kubernetes administration:
:::success
[Cluster Architecture, Installation & Configuration 25%](https://github.com/alijahnas/CKA-practice-exercises/blob/CKA-v1.23/cluster-architecture-installation-configuration.md)
[Workloads & Scheduling 15%](https://github.com/alijahnas/CKA-practice-exercises/blob/CKA-v1.23/workloads-scheduling.md)
[Services & Networking 20%](https://github.com/alijahnas/CKA-practice-exercises/blob/CKA-v1.23/services-networking.md)
[Storage 10%](https://github.com/alijahnas/CKA-practice-exercises/blob/CKA-v1.23/storage.md)
[Troubleshooting 30%](https://github.com/alijahnas/CKA-practice-exercises/blob/CKA-v1.23/troubleshooting.md)
:::


CNCF總共開放了三種K8S的認證
* CKA (Certified Kubernetes Admin)
* CKAD (Certified Kubernetes Application Developer)
* CKS (Certified KUbernetes Security Specialist)

## 考試準備

在Udemy平台上有一系列的K8S認證的課程，下面的連結是準備CKAD 的兩堂課程。

[CKA:](https://www.udemy.com/course/certified-kubernetes-administrator-with-practice-tests/) Certified Kubernetes Administrator (CKA) with Practice Tests
[CKAD](https://www.udemy.com/course/certified-kubernetes-application-developer/)


### Udemy 課程

* Kubernetes for the Absolute Beginners - Hands-on [買](https://www.udemy.com/course/learn-kubernetes/)
* Certified Kubernetes Administrator (CKA) with Practice Tests [買](https://www.udemy.com/course/certified-kubernetes-administrator-with-practice-tests/)
* Kubernetes Certified Application Developer (CKAD) with Tests [link](https://www.udemy.com/course/certified-kubernetes-application-developer/)


### 免費入門課程

* [LFS151x] 雲基礎設施技術入門課程
[Introduction to Cloud Infrastructure Technologies](https://training.linuxfoundation.org/training/introduction-to-cloud-infrastructure-technologies/#)
* [LFS158x] Kubernetes 入門課程
[Introduction to Kubernetes](https://www.edx.org/course/introduction-to-kubernetes?utm_medium=partner-marketing&utm_source=affiliate&utm_campaign=linuxfoundation&utm_content=landingpage-lfs158)

### Course Resources
```
ID: LFtraining
PWD: Penguin2014
```

![](https://i.imgur.com/A27MWNS.png)



### Class Forum

[LFS258 Class Forum](https://forum.linuxfoundation.org/categories/lfs258-class-forum)



## Udemy-Beginners

Kubernetes for the Absolute Beginners - [Hands-on](https://www.udemy.com/course/learn-kubernetes/)

![](https://i.imgur.com/fAkZcDo.png)

### Objectives

課程大綱

![](https://i.imgur.com/KMqp3TD.png)


### Accessing the Labs

提供一個線上練習的平台 [link](https://kodekloud.com/courses/labs-kubernetes-for-the-absolute-beginners-hands-on/)

因為跟原本 udemy 是不同平台，所以要另外註冊一次。
注意是以 Labs 開頭
[Labs](https://kodekloud.com/lessons/kubernetes-for-beginners-labs/) – Online Kubernetes Lab for Beginners – Hands On

![](https://i.imgur.com/ByxcOks.png)

Apply the coupon code kk-labs-k8b-lakjg328321095305

輸入Udemy-Beginners課程提供給我們免費的代碼:

![](https://i.imgur.com/MC4ic1l.png)


接下來可以成功登陸並開始練習
![](https://i.imgur.com/N0SfBHE.png)


### Section-2 

Quiz:1 Architecture

![](https://i.imgur.com/twPkrqg.png)




## Udemy-CKA

Certified Kubernetes Administrator [(CKA)](https://www.udemy.com/course/certified-kubernetes-administrator-with-practice-tests/) with Practice Tests

![](https://i.imgur.com/iWDn8Vt.png)




## LFS258

This course will give you a strong operating knowledge of Kubernetes, including how to deploy a containerized application and manipulating resources via the API.

**Course Outline**
* Chapter 1. Course Introduction
* Chapter 2. Basics of Kubernetes
* Chapter 3. Installation and Configuration
* Chapter 4. Kubernetes Architecture
* Chapter 5. APIs and Access
* Chapter 6. API Objects
* Chapter 7. Managing State with Deployments
* Chapter 8. Volumes and Data
* Chapter 9. Services
* Chapter 10. Helm
* Chapter 11. Ingress
* Chapter 12. Scheduling
* Chapter 13. Logging and Troubleshooting
* Chapter 14. Custom Resource Definitions
* Chapter 15. Security
* Chapter 16. High Availability
* Chapter 17. Exam Domain Review


### Chapter 2. Basics of Kubernetes

[CKA - Chapter 2 淺談 Kubernetes](https://www.gaia.net/tc/news_detail/2/132/CKA-Kubernetes)
[Chapter 2. Basics of Kubernetes](https://github.com/johandry/CKA/blob/master/Ch02.md)

本章總共有7個小節, 首先我們要知道Kubernetes的定義, 接著透過叢集架構了解K8s的設計, 使用容器化技術是否存在著某些妥協, 需要考量的點有哪些？ 

:::success
Kubernetes is all about decoupled and transient services. 
Decoupling (模組解耦) means that everything has been designed to not require anything else in particular. 
Transient(瞬態) means that the whole system expects various components to be terminated and replaced. 
A flexible and scalable environment means to have a framework that does not tie itself from one aspect to the next, and expect objects to die and to reconnect to their replacements.
:::

#### 2.1 What Is Kubernetes?
```
an open-source system for automating deployment, 
scaling, and management of containerized applications
```
一個自動化部署、可拓展和管理容器應用的開源系統

多年來，Google 一直使用 Borg 來管理叢集中的容器，積累了大量的叢集管理經驗和運維軟體開發能力，Google 參考 Borg ，開發出了 Kubernetes，即 Borg 是 Kubernetes 的前身。（但是 Google 目前還是主要使用 Borg）

除了kubernetes之外, 還有其他的選項嗎？ 其實是有的, 下面介紹幾個.
Built on open source and easily extensible, Kubernetes is definitely a solution to manage containerized applications.  There are other solutions as well. 

![](https://i.imgur.com/Wc8c70F.png)


* Docker Swarm 是 Docker Inc. 的解決方案。它最近經過重新架構，基於 SwarmKit, 嵌入 Docker 引擎。 https://github.com/docker/swarmkit
* Apache Mesos 是一個數據中心調度器，它可以通過使用框架來運行容器。Marathon是可讓您編排容器的框架。 https://mesosphere.github.io/marathon/
* Rancher 是一個與容器編排器無關的系統，它為管理應用程序提供了一個單一的玻璃界面。它支持 Mesos、Swarm、Kubernetes，以及它的原生系統 Cattle。從 2.0 開始，牛已被 Kubernetes 取代。 http://rancher.com








#### 2.2 Components of Kubernetes

Kubernetes 的元件分為兩種，分別是 Control Plane Components(控制平面元件)、Node Components(節點元件)。

![](https://i.imgur.com/rlh0PZF.png)

當我們創建一個 Kubernetes (K8S) 意味著我們創建了一個叢集(cluster), 而叢集(cluster) 的意思是有一組控制平台(control plane) 及 至少一個的工作機節點(worker node), 在這些工作節點上可以運行容器化的應用(application) 並且由 控制平台(control plane)進行管理.
當我們自行建置 Kubernetes 叢集, 每個叢集至少要有一個工作機節點.

工作機節點(Worker)上運行容器化應用的組件稱之為 Pod, 一個Pod上可以跑一個或多個容器(container)
透過控制台(control plane)的管理, 我們可以把同一個容器化的服務透過部署(deployment)機制跑在不同的Pod及工作節點之上, 提供高可用性(High Availability) 及故障移轉 (fault-tolerance)




#### 2.3 Kubernetes Architecture

:::warning
Kubernetes is not just API-driven, but is API-centric.
:::

![](https://i.imgur.com/YRotEBO.png)

* kube-api-server - 控制平台(control plane)的中心就是 api-server，它實現了系統所有 API 的通用功能。用戶客戶端和實現 Kubernetes 業務邏輯的組件（稱為控制器）與相同的 API 交互。這些 API 類似於 REST，主要支持（主要是）持久資源上的 CRUD 操作。所有持久的集群狀態都存儲在 etcd 鍵值存儲的一個或多個實例中。
* kubelet - 每個運行容器的節點都會有kubelet, 它通常是systemd的process, 而不是一個容器. kubelet 接收運行容器的請求，管理任何必要的資源，並與容器引擎一起在本地節點上管理它們。本地容器引擎可以是 Docker、cri-o、containerd 或其他的CRI (container Runtime Interface)
* kube-proxy - 創建和管理網絡規則，以將網絡上的容器暴露給其他容器或外部世界. 它其實就是一個跑在各個工作節點(node) 的網路代理(network proxy), 整體的運行在Pod之上, 並且用來實現一部分的Kubernetes Service 的概念. kube-proxy 維護各個節點上的網路規則, 這些規則允許叢集內部或外部的網路與Pod間進行網路通信.
* kube-scheduler - 將運行容器的pod規範轉發給API，並找到合適的節點來運行這些容器。 又或者說, scheduler 負責監控新創建的Pods 但仍未指定運行的工作節點(Node), 安排工作節點讓Pod可以運行在其之上. 調度決策的考慮因素包含單個Pod 或 多個Pods (Pod 的集合)所需要的資源, 如: 硬體, 軟體, 部署策略/限制/規範, 工作負載間的干擾 (inter-workload interference)及最終期限 (deadlines).
* etcd - 是具有高可用性及一致性的鍵值(key-value) 資料庫, 用來保存K8S叢集數據的後台資料庫. 管理etcd 可能會因為環境建置情況, 我們會需要操作 etcd的備份及還原資料
* control manager - 是Kubernetes 的大腦由 kube-controller-manager 和 cloud-controller-manager 组成. 它的運作機制是通過API-server監控整個叢集的狀態, 並且確保集群上的運作符合預期
    * kube-controller-manager - 每個控制器(controller)其實都是一個單獨的程序(process), 但是為了降低複雜性, 它們都被編譯到同一個可執行的文件, 並在一個程序中運行. 這些控制器(controller)包含下面幾種：
        * 節點控制器 node controller - node 發生故障時, 需要發出通報
        * 任務控制器 job controller - 監測一次性任務的Job 對象, 然後創建Pods來運行任務直到完成任務.
        * 端點控制器 end-point controller - 加入端點的對象也就是串接 Service 與 Pods
        * 服務帳戶與令牌控制器 service account & token controller - 為新建立的命名空間(namespace)創建對應的默認帳戶及訪問令牌
    * cloud-controller-manager - 僅運行在特定的雲平台, 若是本地運行Kubernetes, 所部署的環境不需要考慮之.




---


## Others


###  Using GCE to Set Up the Lab Environment



[GCP VPC](https://medium.com/learn-or-die/gcp-%E5%A4%9A%E5%80%8B%E8%99%9B%E6%93%AC%E7%A7%81%E4%BA%BA%E7%B6%B2%E8%B7%AF-5c574170611b)

```
Virtual Private Cloud  虛擬私有雲
```

default 已經幫你把每個地區對應到的網段都切好了
![](https://i.imgur.com/PMxGj2w.png)



# KodeKloud

利用課程 25.Accessing the Labs 去登入, 並輸入優惠碼變成免費 (密碼會用信件寄給你)
https://uklabs.kodekloud.com/courses/labs-certified-kubernetes-administrator-with-practice-tests/


![](https://i.imgur.com/qlkxtYY.png)

然後回到首頁，看到 Udemy Labs – Certified Kubernetes Administrator with Practice Tests 已經可以 Start Course 了

![](https://i.imgur.com/gXCXRnP.png)



# Killer

[Killer-sh](https://killer.sh/) 線上模擬題

[CKA](https://github.com/oldelette/oldelette.github.io/blob/master/Udemy/CKA1.pdf)



---

# Others 


網路上 Program Exam

[No.1-2021年CKA考试真题](https://blog.csdn.net/shuoshuo132/article/details/115432804)

## 1.1
![](https://i.imgur.com/qoae68P.png)

題目意思是建立一個 service account, 該 service account 具有在命名空間 dev 下創建 Deployment, StatefulSet, DaemonSet 的權限


```
kubectl create
```

[RBAC](https://ithelp.ithome.com.tw/articles/10195944) (Role-Base Access Control，基於角色的訪問控制)


## 1.2

![](https://i.imgur.com/AW1uo99.png)

創建兩個 container pod, 其中一個用來寫 log 到 /log/data/output.log , 另一個要把寫入的 log 輸出.
題目有說到 log 文件允許在當前 pod 中使用，因此我們用 emptyDir 去解決這個問題.
題目沒有說到 namespece, 所以默認選擇 default.

* 先透過 kubectl run 創建 yaml 檔後再去做修改
```
sudo kubectl run log --image=busybox --dry-run=client -o yaml > 0205_logpod.yaml
```

* yaml 
```
apiVersion: v1                                                                                                                                                                                             
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: log 
  name: log 
spec:
  containers:
  - image: busybox
    name: log-pod
    command: ["sh","-c","echo important information >> /log/data/output.log; sleep 1d"]
    volumeMounts:
    - name: date-log
      mountPath: /log/data
    resources: {}
  - image: busybox
    name: log-cus
    command: ["sh","-c","cat /log/data/output.log; sleep 1d"]
    volumeMounts:
    - name: date-log
      mountPath: /log/data
  dnsPolicy: ClusterFirst
  restartPolicy: Always
  volumes:
  - name: date-log
    emptyDir: {}
status: {}
```


:::info
查看 log 方式：kubectl log my-pod
如果一個 pod 中有多個 container，可以使用 -c 另行指定 container。
:::

通過 kubectl log 去查看 log message
```
sudo kubectl logs log -c log-cus
```



![](https://i.imgur.com/OyEDcJj.png)
