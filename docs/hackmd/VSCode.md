###### tags: `Language`

2021.5.6

# VSCode 
[Visual Studio Code](https://code.visualstudio.com/)

是一個支援多平台的開放原始碼免費程式碼編輯器，由微軟開發，並支持Windows，Mac OS，Linux，只要裝上對應語言的延伸外掛模組(Extensions)，即可輕鬆變成IDE的開發環境。



## 使用python 範例

VS Code編輯器上主要分成三個區塊，左邊是快速功能鈕，藍色框是快速功能鈕的顯示區，右邊綠色框是Python程式分頁的顯示區。




## 連結 Wimdow 下的 cmder

[同場加映 : 在終端機上使用cmder](https://medium.com/@jackaly9527/%E7%9A%AE%E6%AF%9B%E7%AD%86%E8%A8%98-%E5%A6%82%E4%BD%95%E5%9C%A8vs-code%E4%BD%BF%E7%94%A8%E5%A4%9A%E7%A8%AEshell%E6%96%BC%E7%B5%82%E7%AB%AF%E6%A9%9F%E4%B8%8A-f89ce1e59fea)


### 修改 setting.json

1. 左下角齒輪先開啟setting版面
2. 在setttings頁面，我們可以透過點擊右上角右邊數來第三個的icon，進入當前的settings.json，並透過修改json進行設定。


* 改成這樣，位址要對。
```json=
{
    "workbench.editorAssociations": [
        {
            "viewType": "jupyter.notebook.ipynb",
            "filenamePattern": "*.ipynb"
        }
    ]
    "terminal.integrated.shell.windows": "Cmd.exe",
    "terminal.integrated.env.windows": {
    "CMDER_ROOT": "D:\\gavin\\cmder"
    },
    "terminal.integrated.shellArgs.windows": [
        "/k D:\\gavin\\cmder\\vendor\\init.bat"
    ],
}
```

* 成功結合
![](https://i.imgur.com/mqjoaeU.png)


2021 cmder 改版後如下

```json=
{
    "terminal.integrated.profiles.windows": {
        "cmder": {
          "path": "C:\\WINDOWS\\System32\\cmd.exe",
          "args": ["/k D:\\gavin\\cmder\\vendor\\init.bat"]
        }
      },
      "terminal.integrated.defaultProfile.windows": "cmder"
}
```


## 加入 pep8

[參考文獻: PEP 8寫作風格補充以及Visual Studio Code的autopep8延伸模組的相關設定](https://swf.com.tw/?p=1229)

<span class="red">**存檔時透過 autopep8自動執行格式化**</span>
上述的import敘述整理、函式和類別之間的空行、程式敘述的空格…等格式化設定，可交給VS code的autopep8延伸模組自動完成。設定步驟如下：

![](https://i.imgur.com/JfW3L13.png)

![](https://i.imgur.com/wpz5cwk.png)
如此，每次存檔時，VS Code就會自動執行autopep8自動調整Python原始碼的編排格式。

![](https://i.imgur.com/EUEi2tp.png)



## 連接 Github

[參考這篇:使用Visual Studio Code( VS Code)](https://www.rs-online.com/designspark/github-microsoft-visual-studio-code-cn) 又需要使用 Github來做版本控制或紀錄，現在兩家已經整合得很好了，VS Code甚至已經附帶著Git的插件。

### Git 

Window下 要先安裝並設置好在環境變數，不能用Cmder 內建的git。
[到官網下載](https://git-scm.com/downloads)，然後一直下一步就好，他會自己幫你設定好環境變數。

![](https://i.imgur.com/N9J7cuF.png)


### 下載並修改已存在的repository

step1. 我們可以直接複製存放庫 (repository)，首先要先去Github取得網址。

![](https://i.imgur.com/auZV2Ab.png)


step2. 接著點選「複製存放庫」可以於上方輸入網址進行複製：

![](https://i.imgur.com/zN46eid.png)

step3. 完成


* 一旦你在這個專案中對任何程式進行修改，VS Code就會自動偵測到並且將其標示為M ( Modified )：
* 如果是新增檔案的話則會以Ｕ來標記，是Untracked 的意思：


注意:
在剛剛的操作中可以注意到是「暫存」的變更，代表還沒完全認可，如果要認可的話需要點選提交 ( Commit ) 而提交的時候都需要附上版本資訊。
沒有做這步驟的話無法上傳

* 上傳github
1.點擊狀態即可直接進行上傳。
![](https://i.imgur.com/0B9FI5W.png)



## 快捷鍵

[官方快速鍵一覽表](https://www.itread01.com/content/1548344894.html)

檢視多個檔案
* 新建檔案 Ctrl+N
* 檔案之間切換 Ctrl+Tab
* 切出一個新的編輯器（最多3個）Ctrl+\，也可以按住Ctrl滑鼠點選Explorer裡的檔名
* 左中右3個編輯器的快捷鍵Ctrl+1 Ctrl+2 Ctrl+3
* 3個編輯器之間迴圈切換 Ctrl+`
* 編輯器換位置，Ctrl+k然後按Left或Right

側邊欄4大功能顯示：
* Show Explorer Ctrl+Shift+E
* Show SearchCtrl+Shift+F
* Show GitCtrl+Shift+G
* Show DebugCtrl+Shift+D
* Show OutputCtrl+Shift+U









<style>
.blue {
  color: blue;
}
.green {
  color: green;
}
.red {
  color: red;
}
</style>



