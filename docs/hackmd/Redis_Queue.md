###### tags: `Work`


# Redis Queue

[RQ (Redis Queue)](https://python-rq.org/) is a simple Python library for queueing jobs and processing them in the background with workers. 
It is backed by Redis and it is designed to have a low barrier to entry. It can be integrated in your web stack easily.

RQ requires Redis >= 3.0.0.

Redis Queue 是一個輕量級的 python 庫，用於將任務放入到隊列將，並在後臺異步執行。
Redis Queue 依賴 Redis ，且Redis 的版本要求>=3.0.0

* Install on Linux
```
sudo apt install python3-rq
```


## worker

rq worker 就是 python 進程，主要任務是從執行長時間、或是阻塞任務.

* 啓動 worker
```
rq worker
rq worker high normal low # 啓動 rq worker,並監聽 high,normal,low 三個任務隊列
```

worker 會不斷的從給定的隊列中讀取任務，並執行任務。當所有任務都完成時，會阻塞等待新任務的到來。
:::warning
注意：
每個 worker 同一時刻只會處理一個任務
在一個 worker 中，不存在同步問題。如果你想同步執行多個任務，則需要啓動多個 worker.
:::


###  worker 的生命週期

worker 進程的生命週期如下：
* ++啓動:++
主要是加載 python 環境
* ++註冊++
worker 將自己註冊到RQ 系統中
* ++開始監聽++
從Redis 隊列中取任務。如果 worker 工作在burst 模式下，且所有監聽的隊列是空的，則 worker 進程退出，進程結束。否則，等待任務到來；
* ++準備執行任務++
worker 將自己的狀態設置爲busy,表明自己準備要執行任務。並將要執行的任務在StartedJobRegistry中進行註冊.
* ++啓動子進程++
利用子進程執行任務.
* ++任務執行後的清理工作++
worker設置自己的狀態爲idle，並設置job的執行結果。另外， 在StartedJobRegistry中取消註冊，在FinishedJobRegistry中對任務進行註冊。
* ++循環++
*繼續監聽任務隊列.


## Job

RQ 任務（job) 是Python對象，表示將要在worker中執行的函數（任何可執行的函數都可以，沒有特別的要求.
將函數即函數參數壓入Redis 隊列中，即可返回一個Job 對象.

```python!
# main.py
import requests
import time
from rq import Queue
from redis import Redis
from add import print_result

# Tell RQ what Redis connection to use
redis_conn = Redis()
# q = Queue('low', connection=redis_conn)
q = Queue(connection=redis_conn)  

#將可執行函數壓入隊列中
job = q.enqueue(print_result, 'hello world')
print(f"job result: {job.result}")   # => None

# Now, wait a while, until the worker is finished
time.sleep(2)
print(f"job result: {job.result}")
```


```python!
# utils.py
def print_result(url):
    return url
```



![](https://hackmd.io/_uploads/B1lvecOv3.png)

```
python main.py
```


# Redis

Remote Dictionary Server，是快速的開源記憶體內鍵值資料存放區
Redis是完全開源免費的，遵守BSD協議，是一個高性能的key-value數據庫。
Redis 與其他 key - value 緩存產品有以下三個特點：
* Redis支持數據的持久化，可以將內存中的數據保持在磁盤中，重啟的時候可以再次加載進行使用。
* Redis不僅僅支持簡單的key-value類型的數據，同時還提供list，set，zset，hash等數據結構的存儲。
* Redis支持數據的備份，即master-slave模式的數據備份。


## Redis Advantage

:::success
1. 異常快速：Redis 的速度非常快，每秒能執行約11萬集合，每秒約81000+條記錄
2. 支持豐富的數據類型：Redis支持最大多數開發人員已經知道像列表，集合，有序集合，散列數據類型。這使得它非常容易解決各種各樣的問題，因為我們知道哪些問題是可以處理通過它的數據類型更好。
3. 操作都是原子性：所有Redis操作是原子的，這保證了如果兩個客戶端同時訪問的Redis服務器將獲得更新後的值。
4. 多功能實用工具：Redis是一個多實用的工具，可以在多個用例如緩存，消息，隊列使用(Redis原生支持發布/訂閱)，任何短暫的數據，應用程序，如Web應用程序會話，網頁命中計數等
:::




##  Redis Base Command
Redis 基本操作方法 常用指令


```
sudo docker run --name redis -d redis
```

Check Redis [Ref](https://blog.techbridge.cc/2016/06/18/redis-introduction/)

```
redis-cli
set test "hello world"
-> OK
get test
```
下面的提示 127.0.0.1 是本機的IP地址，6379 為 Redis 服務器運行的 port

![](https://hackmd.io/_uploads/HJ-MA_dvn.png)


## Redis with database

Redis 與資料庫資料同步解決方案

資料庫同步到Redis
我們大多傾向於使用這種方式，也就是將資料庫中的變化同步到Redis，這種更加可靠。Redis在這裡只是做緩存。

### 方案1

做緩存，就要遵循緩存的語義規定：
讀：讀緩存redis，沒有，讀mysql，並將mysql的值寫入到redis。
寫：寫mysql，成功後，更新或者失效掉緩存redis中的值。

![](https://hackmd.io/_uploads/HJOPJFdwh.png)

對於一致性要求高的，從資料庫中讀，比如金融，交易等資料。其他的從Redis讀
這種方案的好處是由 mysql，常規的關係型資料庫來保證持久化，一致性等，不容易出錯


### 方案2

這裡還可以基於binlog使用mysql_udf_redis，將資料庫中的資料同步到Redis.
但是很明顯的，這將整體的複雜性提高了，而且本來我們在系統代碼中能很輕易完成的功能，現在需要依賴第三方工具，而且系統的整個邊界擴大了，變得更加不穩定也不好管理了.

![](https://hackmd.io/_uploads/ByFgxYdw2.png)






